<?php
use setasign\Fpdi\Fpdi;

defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);

class Bup extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    // START
	function __construct()

        {
            parent::__construct();
            not_login();
            $this->load->helper(array('form', 'url', 'download'));
            $this->load->model('M_bup');
            $this->load->library('form_validation');
            $this->dbdefault = $this->load->database('default', TRUE);
            $this->dbeps = $this->load->database('eps', TRUE);
            // $this->load->library('cetak_pdf');
            $this->load->library('modify_pdf');
            $this->load->library('upload');
            $this->load->helper('apiclient');
            $this->load->helper('namafile');
            $this->load->helper('help');
            $this->load->helper(array('form', 'url'));
             $this->load->library('zip');
                $this->load->helper('download');


            // Load zip library
            $this->load->library('zip');
        }
// END

//----------------------------------------------ADMIN BKD------------------------------------------------------------------------//

   

	// INDEX BKD
        public function index()
            {
                // $skpd = $this->input->post('A_01');
                //  $row = $this->M_bup->get($id);

                //  $data = array(
                // 	'row' => $row,

                //  );
                // $this->template->load('template', 'admin/bup/index',$data); //dari tabel EPS

                $skpd = $this->input->get('A_01');
                $thn = $this->input->get('thpensiun');
                $tmt = $this->input->get('tmtpensiun');
                // $status=$this->input->get('status');
                $data['skpd'] = $this->M_bup->get_filter_skpd();
                //  echo date_indo('2017-09-5');
                // $data['tahun'] = $this->M_bup->get_filter_thn();
                // $data['tmt'] = $this->M_bup->get_filter_tmt();
                // $data['waktu_skpd'] = $this->M_bup->get_list_waktu_skpd();
                $data['row'] = $this->M_bup->get($skpd,$thn,$tmt)->result_array();
                    // echo json_encode($data);
                $this->template->load('template', 'admin/bup/bkd/index', $data); //dari tabel EPS
            } 

	// END 

     
	// BUP DIUSULKAN BKD
        public function bup_bkd()
            {
                $row = $this->M_bup->get_usul();

                $data = array(
                    'row' => $row,
                );
                $this->template->load('template', 'admin/bup/bkd/view_bup', $data); //dari tabel pensiun pngajuan
            }
	//END

	// BLAST BUP BKD KE SKPD
        public function blast_bkd()
            {
                $this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

                $id = $this->input->post('B_02B');
                $ids = $this->input->post('B_02B');
                $blast_bkd = $this->input->post('diusulkan_bkd');
                // $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
                // $B_03 = $this->input->post('B_03');
                $kunci = $this->input->post('kunci_nominatif');
                $A_01 = $this->input->post('A_01');
                $wskpd = $this->input->post('id_waktu_skpd');
                $wskpd_get = $this->input->post('id_waktu_skpd2');
                $tmt = $this->input->post('tmt');
                
                $start_date = date('Y:m:d H:i:s');  
                $dates = strtotime($start_date);
                $date = strtotime("+$wskpd_get day", $dates);
            
                
                // $AK_TMT = $this->input->post('AK_TMT');
                if ($this->form_validation->run() == FALSE) {

                    //  $row = $this->M_bup->get();
                    //  $dinas = $this->M_bup->get_skpd();
                    //  $data = array(
                    // 	'row' => $row,
                    // 	'dinas' => $dinas,
                    //  );
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Gagal mengirim data.');
                    //  $this->template->load('template', 'admin/bup/index',$data);
                    // redirect('bup');
                    redirect_back();
                } else {

                    // $this->M_bup->insert_history_acc();
                    for ($i = 0; $i < sizeof($id); $i++) {
                        $data = array(
                            'B_02B' => $id[$i],
                            'diusulkan_bkd' => $blast_bkd[$i],
                            'id_waktu_skpd' => $wskpd[$i],
                            'due_date_skpd' => date('Y:m:d H:i:s', $date),
                            'created_at' => date('Y:m:d H:i:s'),
                            'A_01' => $A_01[$i],
                            'last_position' => 1,
                            'jenis_ajuan' => 1,
                            'status_ajuan' => 0,
                             'tmt' => $tmt[$i],
                            // 'AK_TMT' => $AK_TMT[$i],
                        );
                        $this->db->insert('pensiun_pengajuan', $data);
                    }



                    // echo json_encode($data);


                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil mengirim data.');

                    redirect_back();
                    //  return redirect('bup');
                }
            }
	//END

    // REVISI USULAN DARI BKD KE SKPD
	    public function revisi_bkd()
        {
            $this->form_validation->set_rules('B_02B', 'B_02B', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal merubah data.');
                redirect_back();
            } else {
                $data = array(
                    "last_position" => 1,
                    "status_ajuan" => 0,

                );
                $this->db->where('B_02B', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan', $data);

                $data = array(
                    "status_acc" => 2,
                    "nip_acc" => $this->session->userdata('B_02B'),

                );
                $this->db->where('id_pengajuan', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan_acc', $data);
                // echo json_encode($data);
                // $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil menambahkan data');

                redirect_back();
            }
        }
	// END

	// ACC USULAN DARI BKD KE SKPD
	    public function acc_bkd()
        {
            $this->form_validation->set_rules('B_02B', 'B_02B', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal merubah data.');
                redirect_back();
            } else {
                $data = array(
                    "last_position" => 2,
                    "status_ajuan" => 5

                );
                $this->db->where('B_02B', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan', $data);

                $data = array(
                    "status_acc" => 1,
                    "nip_acc" => $this->session->userdata('B_02B'),

                );
                $this->db->where('id_pengajuan', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan_acc', $data);
                // echo json_encode($data);
                // $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil ACC BUP ');

                redirect_back();
            }
        }
	// END

	//EDIT INDEX BUP BKD
		// public function edit_index_bup()
		// {
		// 	$row = $this->M_bup->get_usul();
		// 	$data = array(

		// 		'row' => $row,
		// 	);
		// 	$this->template->load('template', 'admin/bup/edit_bup_bkd', $data);
	    // }
	//END

	//DELETE PENGAJUAN
	    public function del($id)
        {

            $this->db->where('B_02B', $id);
            $this->db->delete('pensiun_pengajuan');
            redirect_back();
        }
	//END

	//ACTION EDIT BUP BKD
	    public function edit_bup_bkd($id)
        {
            $this->form_validation->set_rules('B_02B', 'NIP', 'trim');
            $this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            // $idk = $this->input->post('b.B_02');
            if ($this->form_validation->run() == FALSE) {

                $query = $this->M_bup->getall($id);
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                    $provinsi = $this->M_bup->get_provinsi();
                    $kabkota = $this->M_bup->get_kab();
                    $keca = $this->M_bup->get_keca();
                    $desa = $this->M_bup->get_kelu();
                    $status_anak = $this->M_bup->get_status_anak();
                    $status_sutri = $this->M_bup->get_status_sutri();
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'provinsi' => $provinsi,
                        'kabkota' => $kabkota,
                        'keca' => $keca,
                        'desa' => $desa,
                        'status_anak' => $status_anak,
                        'status_sutri' => $status_sutri,
                        'status_acc' => $status_acc,

                    );
                    // $nip = $id;
                    // $kodes = $this->input->post('kodes'); //sample => 10,11,13,14
                    // $kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                    // $kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8,28_9,25,27';
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';
                    
                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id,
                        
                        
                    );
                    // $nama = namafile($kodes);
                    $res = ApiClientPost($params);
                    $result = json_decode($res, TRUE);
                    $data['efilenya'] = $result['data'];
                    // echo json_encode($data);
                    $this->template->load('template', 'admin/bup/bkd/edit_bup_bkd', $data); //dari tabel pensiun pngajuan
                } else {
                    echo "<script>alert ('Data tidak ditemukan');";
                    redirect_back();
                }

                // redirect('user/add_index');

                # code...
            } else {

                // $post = $this->input->post(null, TRUE);
                $this->M_bup->edit();
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                    redirect_back();
                }
                echo "<script>window.location ='" . site_url('Bup/bup_bkd') . "' ; </script>";
            }
        }
	//END

    // ACC AJUAN DARI SKPD 
        public function acc_ajuan_skpd()
        {
            $this->form_validation->set_rules('B_02B', 'B_02B', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal merubah data.');
                redirect_back();
            } else {
                $data = array(
                    "last_position" => 2,
                    "status_ajuan" => 5,
                );
                $this->db->where('B_02B', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan', $data);

                $data = array(
                    "status_acc" => 1,
                    "nip_acc" => $this->session->userdata('B_02B'),

                );
                $this->db->where('id_pengajuan', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan_acc', $data);
                // echo json_encode($data);
                // $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil menambahkan data');

                redirect_back();
            }
        }

    // END

    // INDEX BUP ACC

        public function bup_by_sk()
        {
             
          
                    $data['row'] = $this->M_bup->getby_sk()->result_array();
                    // echo json_encode($data);
                    $this->template->load('template', 'admin/bup/bkd/view_sk', $data);

           
        }

    //END
    // INDEX BUP ACC

        public function bup_acc()
        {
            
          
                    $data['row'] = $this->M_bup->get_bup_acc()->result_array();
                    // echo json_encode($data);
                    $this->template->load('template', 'admin/bup/bkd/view_bup_acc', $data);

           
        }

    //END

    // DOWNLOADBULK ALL

    function download_bulk_bkn_yk()
        {
             
                 $id = $_POST['B_02B'];
                $this->load->library('zip');
                $this->load->helper('download');
                  
                    // $nama = namafile($kodes);
                    // $res = ApiClientPost($params);
                    // $result = json_decode($res, TRUE);
                    // $data['efilenya'] = $result['data'];
                //  echo json_encode($data);
                    //   ob_start();

                   for ($i = 0; $i < sizeof($id); $i++) {
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';
                    
                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id[$i],
                    );
                    
                     $res = ApiClientPost($params);
                     $result = json_decode($res, TRUE);
                     $data['efilenya'] = $result['data'];
                    //  echo json_encode($data);
                       
                        // $this->zip->add_data($data['efilenya']);
                        // $this->zip->archive($data['efilenya'].'.zip');
                        // $this->zip->download( $data['efilenya'].'.zip');
                        // $this->load->library('zip');
                        // $this->load->helper('download');

                    foreach ($data['efilenya'] as $key => $val) {

                        if($val['id_jenis'][$i] != NULL){
                            $namel = kodefile_jkt($val['id_jenis'][$i]);
                                        // $name_f = $namel.'_'.$row->B_02B.'.pdf';
                            $name_f = $namel . '_' . $id[$i] . '.pdf';
                            $paths = $val['efile'];
                            $this->zip->add_data($name_f, file_get_contents($paths));
                            //  echo json_encode($paths);
                             
                            // $this->zip->archive('download'.'.zip');
                            //  $this->zip->download('download'. '.zip');
                        }
                        
                        // echo json_encode($paths);
                         $this->zip->archive('download'.'.zip');
                    }
                   
                    $this->zip->download('download'. '.zip'); 
                    
                } 
                 
              
                
        }
    // END
    // BLAST ACC BKD KE BKN
        public function ajuan_bkn()
        {

            $this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

            $id = $this->input->post('B_02B');
            $ids = $this->input->post('B_02B');
             $idy = $this->input->post('B_02B');
            $blast_bkd = $this->input->post('diusulkan_bkd');
            // $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
            // $B_03 = $this->input->post('B_03');
            $kunci = $this->input->post('kunci_nominatif');
            $A_01 = $this->input->post('A_01');
            $wskpd = $this->input->post('id_waktu_skpd');
            $tanggalSekarang = date('Y-m-d H:i:s');
            $tanggalDuedate = date("Y-m-d H:i:s", strtotime($tanggalSekarang . ' + ' . $wskpd . ' Days'));
            echo "tanggal sekarang :" . $tanggalSekarang . "<br>";
            echo "tanggal overdue :" . $tanggalDuedate;
            // echo json_encode($ids);

            $this->load->library('zip');
            $this->load->helper('download');
            // $AK_TMT = $this->input->post('AK_TMT');
            if ($this->form_validation->run() == FALSE) {
                // 	echo "d";
                // die();
                //  $row = $this->M_bup->get();
                //  $dinas = $this->M_bup->get_skpd();
                //  $data = array(
                // 	'row' => $row,
                // 	'dinas' => $dinas,
                //  );
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
                //  $this->template->load('template', 'admin/bup/index',$data);
                // redirect('bup');
                redirect_back();
            } else {

                $id_surat = $this->M_bup->get_id_surat();

                foreach ($id_surat as $data) {
                    $data->id; // Tambahkan tag option ke variabel $lists
                }

                $no_surat_pengantar = $data->id;

                for ($i = 0; $i < sizeof($idy); $i++) {
                    $data = array(
                        'B_02B' => $idy[$i],
                        'nomer_surat' =>$this->input->post('nomer_surat'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_by' => $this->session->userdata('B_02B'),
                        

                    );
                    // $this->db->where('B_02B', $idy[$i]);
                    // $this->db->update('pensiun_pengajuan', $data);
                    $this->db->insert('pensiun_no_surat_pengantar',$data);
                }

                // $this->M_bup->insert_history_acc();

                for ($i = 0; $i < sizeof($id); $i++) {
                    $data = array(
                        // 'B_02B' => $id[$i],
                        'last_position' => 3,
                        'status_ajuan' => 6,
                        'id_no_surat_pengantar_bkd' => $no_surat_pengantar,

                    );
                    $this->db->where('B_02B', $id[$i]);
                    $this->db->update('pensiun_pengajuan', $data);
                }
                for ($x = 0; $x < sizeof($ids); $x++) {
                    $data = array(
                        'id_pengajuan' => $ids[$x],
                        'nip_acc' => $this->session->userdata('B_02B'),
                        'status_acc' => 0,
                        // 'jabatan_acc'=>,
                        'created_at' => date('Y:m:d H:i:s'),

                    );
                    $this->db->where('id_pengajuan', $ids[$x]);
                    $this->db->update('pensiun_pengajuan_acc', $data);
                }

                for ($i = 0; $i < sizeof($id); $i++) {
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';
                    
                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id[$i],
                    );
                    
                     $res = ApiClientPost($params);
                     $result = json_decode($res, TRUE);
                     $data['efilenya'] = $result['data'];
                    //  echo json_encode($data);
                       
                        // $this->zip->add_data($data['efilenya']);
                        // $this->zip->archive($data['efilenya'].'.zip');
                        // $this->zip->download( $data['efilenya'].'.zip');
                        // $this->load->library('zip');
                        // $this->load->helper('download');

                    foreach ($data['efilenya'] as $key => $val) {

                        if($val['id_jenis'][$i] != NULL){
                            $namel = kodefile_jkt($val['id_jenis'][$i]);
                                        // $name_f = $namel.'_'.$row->B_02B.'.pdf';
                            $name_f = $namel . '_' . $id[$i] . '.pdf';
                            $paths = $val['efile'];
                            $this->zip->add_data($name_f, file_get_contents($paths));
                            //  echo json_encode($paths);
                             
                            // $this->zip->archive('download'.'.zip');
                            //  $this->zip->download('download'. '.zip');
                        }
                        
                        // echo json_encode($paths);
                         $this->zip->archive('download'.'.zip');
                         $this->zip->download('download'. '.zip'); 
                    }
                   
                     redirect_back();
                    
                } 

                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');

                redirect_back_alt();
                // $this->template->load('template', 'admin/bup/view_bup_acc'); 
                // redirect_back();
            }
        }
	//END

	// ACC USULAN DARI BKD KE BKN
        public function kirim_bkn()
            {
                $this->form_validation->set_rules('B_02B', 'B_02B', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Gagal merubah data.');
                    redirect_back();
                } else {
                    $data = array(
                        "last_position" => 3,
                        "status_ajuan" => 6

                    );
                    $this->db->where('B_02B', $_POST['B_02B']);
                    $this->db->update('pensiun_pengajuan', $data);

                    $data = array(
                        "status_acc" => 0,
                        "nip_acc" => $this->session->userdata('B_02B'),

                    );
                    $this->db->where('id_pengajuan', $_POST['B_02B']);
                    $this->db->update('pensiun_pengajuan_acc', $data);
                    // echo json_encode($data);
                    // $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
                    $this->session->set_flashdata('info', 'Berhasil');
                    $this->session->set_flashdata('pesan', 'Data BUP sudah dikirim ke BKN.');

                    redirect_back();
                }
            }
	// END

//----------------------------------------------END ADMIN BKD------------------------------------------------------------------//

//----------------------------------------------ADMIN SKPD/UMPEG--------------------------------------------------------------//
	
    // BLAST BUP SKPD KE USER
        public function blast_skpd()
            {
                $this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');
                $id = $this->input->post('B_02B');
                $blast_skpd = $this->input->post('diusulkan_skpd');
                $ids = $this->input->post('B_02B');
                $wskpd = $this->input->post('id_waktu_pns');
                //  $wskpd_get = $this->input->post('id_waktu_pns2');

                $start_date = date('Y:m:d H:i:s');  
                $dates = strtotime($start_date);
                $date = strtotime("+$wskpd day", $dates);
                if ($this->form_validation->run() == FALSE) {


                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
                    //  $this->template->load('template', 'admin/bup/index',$data);
                    redirect('bup/bup_skpd');
                } elseif ($wskpd > 0) {

                    for ($i = 0; $i < sizeof($id); $i++) {
                        $data = array(
                            'id_waktu_pns' => $this->input->post('id_waktu_pns'),
                            'diusulkan_skpd' => 1,
                            'last_position' => 0,
                            'due_date_pns' => date('Y:m:d H:i:s', $date),
                            'status_ajuan ' => 1

                        );
                        $this->db->where('B_02B', $id[$i]);
                        // $this->db->where('id_waktu_pns', $wskpd[$i]);
                        $this->db->update('pensiun_pengajuan', $data);
                    }
                    for ($x = 0; $x < sizeof($ids); $x++) {
                        $data = array(
                            'id_pengajuan' => $ids[$x],
                            'nip_acc' => $this->session->userdata('B_02B'),
                            'status_acc' => 0,
                            'jabatan_acc'=> $this->session->userdata('I_JB'),
                            'created_at' => date('Y:m:d H:i:s'),

                        );
                        $this->db->insert('pensiun_pengajuan_acc', $data);
                    }

                    // echo json_encode($data);


                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil mengirim data');

                    return redirect('bup/bup_skpd');
                } else {

                    for ($i = 0; $i < sizeof($id); $i++) {
                        $data = array(
                            // 'B_02B' => $id[$i],
                            'diusulkan_skpd' => 1,
                            'last_position' => 0,
                            'status_ajuan ' => 1

                        );
                        $this->db->where('B_02B', $id[$i]);
                        $this->db->update('pensiun_pengajuan', $data);
                    }
                    for ($x = 0; $x < sizeof($ids); $x++) {
                        $data = array(
                            'id_pengajuan' => $ids[$x],
                            'nip_acc' => $this->session->userdata('B_02B'),
                            'status_acc' => 0,
                            'jabatan_acc'=> $this->session->userdata('I_JB'),
                            'created_at' => date('Y:m:d H:i:s'),

                        );
                        $this->db->insert('pensiun_pengajuan_acc', $data);
                    }

                    // echo json_encode($data);


                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil mengirim data');

                    return redirect('bup/bup_skpd');
                }
            }
	//END

	// KIRIM USULAN DARI SKPD KE BKD
	    public function kirim_usulan_skpd()
        {
            $this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

            $idx = $this->input->post('B_02B');
            $ids = $this->input->post('B_02B');
            $idy = $this->input->post('B_02B');
            $kirim = $this->input->post('kirim_bkd');
            $btl = $this->input->post('kirim_pns');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal mengirim data.');
                //  $this->template->load('template', 'admin/bup/index',$data);
                redirect_back();
            
            } else {
                // $this->M_bup->insert_surat_pengantar();
                $id_surat = $this->M_bup->get_id_surat();

                foreach ($id_surat as $data) {
                    $data->id; // Tambahkan tag option ke variabel $lists
                }

                $no_surat_pengantar = $data->id;

                for ($i = 0; $i < sizeof($idy); $i++) {
                    $data = array(
                        'B_02B' => $idy[$i],
                        'nomer_surat' =>$this->input->post('nomer_surat'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_by' => $this->session->userdata('B_02B'),
                        

                    );
                    // $this->db->where('B_02B', $idy[$i]);
                    // $this->db->update('pensiun_pengajuan', $data);
                    $this->db->insert('pensiun_no_surat_pengantar',$data);
                }

                if ( $_POST['kirim']) {

                    for ($i = 0; $i < sizeof($idx); $i++) {
                        $data = array(
                            'B_02B' => $idx[$i],
                            'id_no_surat_pengantar_skpd' => $no_surat_pengantar,
                            'last_position' => 2,
                            'status_ajuan' => 3,
                            

                        );
                        $this->db->where('B_02B', $idx[$i]);
                        $this->db->update('pensiun_pengajuan', $data);
                    }

                    for ($x = 0; $x < sizeof($ids); $x++) {
                        $data = array(
                            'id_pengajuan' => $ids[$x],
                            'nip_acc' => $this->session->userdata('B_02B'),
                            'status_acc' => 0,
                            // 'jabatan_acc'=>,
                            'created_at' => date('Y:m:d H:i:s'),

                        );
                        $this->db->where('id_pengajuan', $ids[$x]);
                        $this->db->update('pensiun_pengajuan_acc', $data);
                    }

                // echo json_encode($_POST['kirim']);
                //  echo "KIRIM";
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil mengirim usulan ke BKD');

                redirect_back();

                }else{
                   for ($i = 0; $i < sizeof($idx); $i++) {
                    $keterangan = $this->input->post('keterangan');
                    $data = array(
                        "B_02B" => $idx[$i],
                        "last_position" => 4,
                        "status_ajuan" => 2,
                        "keterangan" => $keterangan,
                        

                    );
                    $this->db->where('B_02B',  $idx[$i]);
                    $this->db->update('pensiun_pengajuan', $data);
                }
                for ($x = 0; $x < sizeof($ids); $x++) {
                    $data = array(
                        "id_pengajuan" => $ids[$x],
                        "status_acc" => 2,
                        "nip_acc" => $this->session->userdata('B_02B'),

                    );
                     
                    //  echo "BTL";
                    $this->db->where('id_pengajuan',  $ids[$x]);
                    $this->db->update('pensiun_pengajuan_acc', $data);
                }
                    // echo json_encode($data);
                    $this->session->set_flashdata('succes',"Data Berhasil Diedit");
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Batal kirim ke BKD');

                    redirect_back();
                    // echo "BTL";
                    // echo json_encode($_POST['batal']);
                
                }

               
            }
            // echo "BTL";
            //     for ($i = 0; $i < sizeof($idx); $i++) {
            //         $keterangan = $this->input->post('keterangan');
            //         $data = array(
            //             "B_02B" => $idx[$i],
            //             "last_position" => 4,
            //             "status_ajuan" => 2,
            //             "keterangan" => $keterangan,
                        

            //         );
            //         $this->db->where('B_02B',  $idx[$i]);
            //         $this->db->update('pensiun_pengajuan', $data);
            //     }
            //     for ($x = 0; $x < sizeof($ids); $x++) {
            //         $data = array(
            //             "id_pengajuan" => $ids[$x],
            //             "status_acc" => 2,
            //             "nip_acc" => $this->session->userdata('B_02B'),

            //         );
                    
            //         //  echo json_encode($kirim);
            //         $this->db->where('id_pengajuan',  $ids[$x]);
            //         $this->db->update('pensiun_pengajuan_acc', $data);
            //     }
            //         // echo json_encode($data);
            //         $this->session->set_flashdata('succes',"Data Berhasil Diedit");
            //         $this->session->set_flashdata('info', 'success');
            //         $this->session->set_flashdata('pesan', 'Batal kirim ke BKD ??');

            //         redirect_back();
            
        }
	// END
 

    //BUP DIAJUKAN SKPD KE BKD
	    public function bup_diajukan_skpd()
        {

            $data['row'] = $this->M_bup->get_ajuan_skpd()->result_array();
            

            $this->template->load('template', 'admin/bup/bkd/view_bup_diajukan_skpd', $data);
        }
    //END

    // EDIT BUP DIAJUKAN SKPD KE BKD
        public function edit_bup_diajukan_skpd($id)
        {

            $this->form_validation->set_rules('B_02B', 'NIP', 'trim');
            $this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            // $idk = $this->input->post('b.B_02');
            if ($this->form_validation->run() == FALSE) {
                $query = $this->M_bup->getall($id);
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                    $provinsi = $this->M_bup->get_provinsi();
                    $kabkota = $this->M_bup->get_kab();
                    $keca = $this->M_bup->get_keca();
                    $desa = $this->M_bup->get_kelu();
                    $status_anak = $this->M_bup->get_status_anak();
                    $status_sutri = $this->M_bup->get_status_sutri();
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'provinsi' => $provinsi,
                        'kabkota' => $kabkota,
                        'keca' => $keca,
                        'desa' => $desa,
                        'status_anak' => $status_anak,
                        'status_sutri' => $status_sutri,
                        'status_acc' => $status_acc,

                    );
                    // $kodes = '28_7,04_12,02,44_2,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_1,28_8';
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';

                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id
                    );

                    $res = ApiClientPost($params);
                    $result = json_decode($res, TRUE);
                    $data['efilenya'] = $result['data'];
                    // echo json_encode($data);
                    $this->template->load('template', 'admin/bup/bkd/edit_bup_bkd', $data); //dari tabel pensiun pngajuan
                } else {
                    echo "<script>alert ('Data tidak ditemukan');";
                    redirect_back();
                }

                // redirect('user/add_index');

                # code...
            } else { 
                // $post = $this->input->post(null, TRUE);
                $this->M_bup->edit();
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                    redirect_back();
                }
                 $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Gagal merubah data Calon BUP.');
                    redirect_back();
            }
            //dari tabel pensiun pngajuan
        }
	//END

    // REVISI USULAN DARI SKPD KE PNS
        public function revisi_skpd()
            {
                $this->form_validation->set_rules('B_02B', 'B_02B', 'trim|required');

                if ($this->form_validation->run() == TRUE) {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Gagal dikirim ke PNS bersangkutan.');
                    redirect_back();
                } else {

                
                    $keterangan = $this->input->post('keterangan');
                    $data = array(
                        
                        "last_position" => 0,
                        "status_ajuan" => 4,
                        "keterangan" => $keterangan,

                    );
                    $this->db->where('B_02B', $_POST['B_02B']);
                    $this->db->update('pensiun_pengajuan', $data);
                    
               
                    $data = array(
                        "status_acc" => 0,
                        "nip_acc" => $this->session->userdata('B_02B'),

                    );
                     $this->db->where('id_pengajuan', $_POST['B_02B']);
                    $this->db->update('pensiun_pengajuan_acc', $data);
                
                    // echo json_encode($data);
                    // $this->session->set_flashdata('succes',"Data Berhasil Diedit");
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil dikirim ke PNS bersangkutan');

                    redirect_back();
                }
                
                
            }
	// END
 
    // INDEX BUP SKPD
	    public function bup_skpd()
        {
            $id = $this->input->post('A_01');
            $row = $this->M_bup->get_usul_by_bkd($id);
            $pns = $this->M_bup->get_waktu_pns_by_bkd($id);
            $data = array(
                'row' => $row,
                'pns' => $pns,
            );
            // echo json_encode($row);

            $this->template->load('template', 'admin/bup/skpd/view_bup_skpd', $data); //dari tabel pensiun pngajuan
        }

	    public function bup_diusulkan_skpd()
        {
            $id = $this->input->get('A_01');


            $data['row'] = $this->M_bup->get_usul_by_id($id)->result_array();

            $this->template->load('template', 'admin/bup/skpd/view_bup_usulan_skpd', $data); //dari tabel pensiun pngajuan
        }
	//END

    //EDIT BUP SKPD
	    public function edit_bup_skpd($id)
        {

            $this->form_validation->set_rules('B_02B', 'NIP', 'trim');
            $this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            // $idk = $this->input->post('b.B_02');
           
            $id_lengkap = $_POST['jenis_berkas'];
            if ($this->form_validation->run() == FALSE) {
                $query = $this->M_bup->getall($id);
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                    $provinsi = $this->M_bup->get_provinsi();
                    $kabkota = $this->M_bup->get_kab();
                    $keca = $this->M_bup->get_keca();
                    $desa = $this->M_bup->get_kelu();
                    $status_anak = $this->M_bup->get_status_anak();
                    $status_sutri = $this->M_bup->get_status_sutri();
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'provinsi' => $provinsi,
                        'kabkota' => $kabkota,
                        'keca' => $keca,
                        'desa' => $desa,
                        'status_anak' => $status_anak,
                        'status_sutri' => $status_sutri,
                        'status_acc' => $status_acc,

                    );
                    // $kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';

                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id 
                    );

                    $res = ApiClientPost($params);
                    $result = json_decode($res, TRUE);
                    $data['efilenya'] = $result['data'];
                    // echo json_encode($res);
                    $this->template->load('template', 'admin/bup/skpd/edit_bup_skpd', $data); //dari tabel pensiun pngajuan
                } else {
                    echo "<script>alert ('Data tidak ditemukan');";
                    redirect_back();
                }

                // redirect('user/add_index');

                # code...
            } else {
                $cek = $this->M_bup->cekberkas($id, $id_lengkap);
                        $x = $_POST['jenis_berkas'];
                        $y = $_POST['status_berkas'];

                        $m =  array_combine($x, $y);
                // $post = $this->input->post(null, TRUE);
                foreach($m as $d){
                    if($d == ''){
                        $this->M_bup->edit_belumlengkap();
                        $this->session->set_flashdata('info', 'Informasi');
                        $this->session->set_flashdata('pesan', 'Berkas persayaratan (*wajib) Belum lengkap.');
                        redirect_back();
                        // die();
                    }else{
                        $this->M_bup->edit();
                        if ($this->db->affected_rows() > 0) {
                        $this->session->set_flashdata('info', 'success');
                        $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                        redirect_back();
                    }
                    echo "<script>window.location ='" . site_url('Bup/bup_skpd') . "' ; </script>";
                    echo "berhasil"; 
                    };
                }
            }
            //dari tabel pensiun pngajuan
        }
	//END

//----------------------------------------------END ADMIN SKPD/UMPEG--------------------------------------------------------------//

//----------------------------------------------USER PNS YBS----------------------------------------------------------------------//

	// KIRIM USULAN DARI USER KE SKPD
	    public function kirim_usulan_user()
        {
            $this->form_validation->set_rules('B_02B', 'B_02B', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal merubah data.');
                redirect_back();
            } else {
                $data = array(
                    "last_position" => 4,
                    "status_ajuan" => 2

                );
                $this->db->where('B_02B', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan', $data);
                $data = array(
                    "status_acc" => 0,


                );
                $this->db->where('id_pengajuan', $_POST['B_02B']);
                $this->db->update('pensiun_pengajuan_acc', $data);
                // echo json_encode($data);
                // $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil mengirim data');

                redirect_back();
            }
        }
	// END

    //BUP USER 
	    public function bup_user($id)
        {
            $this->form_validation->set_rules('B_02B', 'NIP', 'trim');
            //  $this->form_validation->set_rules('kelengkapan_data', 'data', 'trim||required');
            $this->form_validation->set_message('required', '{field} berkas *wajib belum lengkap, silahkan diisi');
            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            // $idk = $this->input->post('b.B_02');
            $nip = $this->session->userdata('B_02B');
            $id_lengkap = $_POST['jenis_berkas'];
             
            $sql = $this->db->query("SELECT * FROM pensiun_pengajuan WHERE B_02B = '$nip' AND diusulkan_bkd = 1 AND diusulkan_skpd = 1 ");
            $query = $sql->num_rows();
            //  echo $query;
            if ($query > 0) {
                if ($this->form_validation->run() == FALSE) {
                    $query = $this->M_bup->getall($id);
                    if ($query->num_rows() > 0) {
                        $row = $query->row();
                        $sutri = $this->M_bup->get_suami_istri($id);
                        $anak = $this->M_bup->get_anak($id);
                        $provinsi = $this->M_bup->get_provinsi();
                        $kabkota = $this->M_bup->get_kab();
                        $keca = $this->M_bup->get_keca();
                        $desa = $this->M_bup->get_kelu();
                        $status_anak = $this->M_bup->get_status_anak();
                        $status_sutri = $this->M_bup->get_status_sutri();
                        $status_acc = $this->M_bup->get_status_pengajuan($id);
                        // $efile_wajib = $this->M_bup->check_berkas();
                        $data = array(
                            'row' => $row,
                            'sutri' => $sutri,
                            'anak' => $anak,
                            'provinsi' => $provinsi,
                            'kabkota' => $kabkota,
                            'keca' => $keca,
                            'desa' => $desa,
                            'status_anak' => $status_anak,
                            'status_sutri' => $status_sutri,
                            'status_acc' => $status_acc,
                            // 'efile_wajib' => $efile_wajib,

                        );
                        // $kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                       
                        $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2,19_3';
                        $token = '!BKDoyee123';

                        
                        $params = array(
                            'token' => $token,
                            'kodes' => $kodes,
                            'nip' => $id
                        );

                        $res = ApiClientPost($params);
                        $result = json_decode($res, TRUE);
                        $data['efilenya'] = $result['data'];
                        // echo json_encode($data);
                        $this->template->load('template', 'admin/bup/user/view_bup_user', $data); //dari tabel pensiun pngajuan
                    } else {
                        echo "<script>alert ('Data tidak ditemukan');";
                        echo "window.location ='" . site_url('Bup/bup_user') . "' ; </scrip>";
                    }

                    // redirect('user/add_index');
                } else {
                    // $post = $this->input->post(null, TRUE);
                        $cek = $this->M_bup->cekberkas($id, $id_lengkap);
                        $x = $_POST['jenis_berkas'];
                        $y = $_POST['status_berkas'];

                        $m =  array_combine($x, $y);

                        foreach($m as $d){
                            if($d == ''){
                                $this->M_bup->edit_belumlengkap();
                                $this->session->set_flashdata('info', 'Informasi');
                                $this->session->set_flashdata('pesan', 'Berkas persayaratan (*wajib) Belum lengkap.');
                                redirect_back();
                                // die();
                            }else{
                                $this->M_bup->edit();
                                if ($this->db->affected_rows() > 0) {
                                $this->session->set_flashdata('info', 'success');
                                $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                                redirect_back();
                            }
                            echo "<script>window.location ='" . site_url('Bup/bup_user') . "' ; </script>";
                            echo "berhasil"; 
                            };
                        }
                        // $m = array_merge($x, $y);
                        // echo json_encode($m);
                    // if ($x != null ) {
                        //     //  echo "Belum lengkap";
                        //     // $id = $this->input->post('id');

                            
                        //         //  for ($i = 0; $i < sizeof($id); $i++) {
                        //         //     $status_berkas = $_POST['status_berkas'];
                        //         //     $data = array(
                        //         //         "status_berkas" => $status_berkas,
                                        

                        //         //     );
                        //         //     $this->db->where('id_pns', $_POST['B_02B']);
                        //         //      $this->db->where('status_berkas', '0');
                        //         //     $this->db->update('pensiun_status_berkas', $data);
                        //         //     echo json_encode($status_berkas);
                        //         // }
                            
                                
                                
                        //             // echo json_encode($x);
                        //         $this->session->set_flashdata('info', 'Informasi');
                        //         $this->session->set_flashdata('pesan', 'Berkas persayaratan (*wajib) Belum lengkap.');
                        //         // redirect_back();
                        //         //  echo "gagal";
                        
                        //  } else {
                            
                            
                        
                        //     //    $result = array();
                        //     //         foreach ($_POST['id_pns'] as $key => $val) {
                        //     //             $result[] = array(             
                        //     //                 'id_pns' => $_POST['id_pns'][$key],
                        //     //                 'jenis_berkas' => $_POST['jenis_berkas'][$key],
                        //     //                 'status_berkas' => $_POST['status_berkas'][$key]         
                        //     //             );      
                        //     //         }      
                        //     //         $this->db->insert_batch('pensiun_status_berkas',$result);
                        //     //         echo json_encode($result);
                                
                        //     //         $this->db->where('id_pns', $id);
                        //     //         $this->db->where('jenis_berkas', "-");
                        //     //         $this->db->delete('pensiun_status_berkas');
                        //             $this->M_bup->edit();
                        //             if ($this->db->affected_rows() > 0) {
                        //             $this->session->set_flashdata('info', 'success');
                        //             $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                        //             redirect_back();
                        //         }
                        //         echo "<script>window.location ='" . site_url('Bup/bup_user') . "' ; </script>";
                        //         echo "berhasil";
                    // }
                    // echo "not";
                   
                }
                //dari tabel pensiun pngajuan
            } else {
                $this->template->load('template', 'admin/validate');
            }
           
        }

	
	//END

// END USER YBS PNS
//----------------------------------------------END USER PNS YBS------------------------------------------------------------------//

//----------------------------------------------MENU BKN-------------------------------------------------------------------------//
// MENU BKN 

    //EDIT BUP BKN
	    public function edit_bup_bkn($id)
        {

            $this->form_validation->set_rules('B_02B', 'NIP', 'trim');
            $this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            // $idk = $this->input->post('b.B_02');
            if ($this->form_validation->run() == FALSE) {
                $query = $this->M_bup->getall($id);
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                    $provinsi = $this->M_bup->get_provinsi();
                    $kabkota = $this->M_bup->get_kab();
                    $keca = $this->M_bup->get_keca();
                    $desa = $this->M_bup->get_kelu();
                    $status_anak = $this->M_bup->get_status_anak();
                    $status_sutri = $this->M_bup->get_status_sutri();
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'provinsi' => $provinsi,
                        'kabkota' => $kabkota,
                        'keca' => $keca,
                        'desa' => $desa,
                        'status_anak' => $status_anak,
                        'status_sutri' => $status_sutri,
                        'status_acc' => $status_acc,

                    );
                    // $kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                    $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                    $token = '!BKDoyee123';

                    $params = array(
                        'token' => $token,
                        'kodes' => $kodes,
                        'nip' => $id
                    );

                    $res = ApiClientPost($params);
                    $result = json_decode($res, TRUE);
                    $data['efilenya'] = $result['data'];
                    // echo json_encode($res);
                    $this->template->load('template', 'admin/bup/bkn/edit_bup_bkn', $data); //dari tabel pensiun pngajuan
                } else {
                    echo "<script>alert ('Data tidak ditemukan');";
                    redirect_back();
                }

                // redirect('user/add_index');

                # code...
            } else {
                // $post = $this->input->post(null, TRUE);
                $this->M_bup->edit();
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
                    redirect_back();
                }
                echo "<script>window.location ='" . site_url('Bup/bup_skpd') . "' ; </script>";
            }
            //dari tabel pensiun pngajuan
        }
	//END

    // BUP AJUAN BKN
     public function bup_ajuan_bkn($id)
        {
            $data = array(
                'page' => 'acc',
                'pages' => 'tolak',

            );
            // $id = $this->input->post('id_surat');
            $data['row'] = $this->M_bup->get_bup_bkn($id)->result_array();


            $this->template->load('template', 'admin/bup/bkn/view_bup_bkn', $data);
            // echo json_encode($data);
        }
    // END

	// BLAST ACC /TOLAK BKD KE BKN
        public function acc_tolak_bkn()
        {
            $this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

            $id = $this->input->post('B_02B');
            $ids = $this->input->post('B_02B');
            $blast_bkd = $this->input->post('diusulkan_bkd');
            // $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
            // $B_03 = $this->input->post('B_03');
            $kunci = $this->input->post('kunci_nominatif');
            $A_01 = $this->input->post('A_01');
            $wskpd = $this->input->post('id_waktu_skpd');
            $tanggalSekarang = date('Y-m-d H:i:s');
            $tanggalDuedate = date("Y-m-d H:i:s", strtotime($tanggalSekarang . ' + ' . $wskpd . ' Days'));
            echo "tanggal sekarang :" . $tanggalSekarang . "<br>";
            echo "tanggal overdue :" . $tanggalDuedate;
            // $AK_TMT = $this->input->post('AK_TMT');
            if ($this->form_validation->run() == FALSE) {

                //  $row = $this->M_bup->get();
                //  $dinas = $this->M_bup->get_skpd();
                //  $data = array(
                // 	'row' => $row,
                // 	'dinas' => $dinas,
                //  );
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
                //  $this->template->load('template', 'admin/bup/index',$data);
                // redirect('bup');
                redirect_back();
            } else {

                // $this->M_bup->insert_history_acc();
                if (isset($_POST['acc'])) {
                    for ($i = 0; $i < sizeof($id); $i++) {
                        $data = array(
                            // 'B_02B' => $id[$i],
                            'last_position' => 3,
                            'status_bkn' => 1

                        );
                        $this->db->where('B_02B', $id[$i]);
                        $this->db->update('pensiun_pengajuan', $data);

                        // echo json_encode($data);
                    }
                    for ($x = 0; $x < sizeof($ids); $x++) {
                        $data = array(
                            'id_pengajuan' => $ids[$x],
                            'nip_acc' => $this->session->userdata('B_02B'),
                            'status_acc' => 5, //AC BKN
                            // 'jabatan_acc'=>,
                            'created_at' => date('Y:m:d H:i:s'),

                        );
                        $this->db->where('id_pengajuan', $ids[$x]);
                        $this->db->update('pensiun_pengajuan_acc', $data);

                        // echo json_encode($data);
                    }
                } elseif (isset($_POST['tolak'])) {
                    for ($i = 0; $i < sizeof($id); $i++) {
                        $data = array(
                            // 'B_02B' => $id[$i],
                            'last_position' => 3,
                            'status_bkn' => 2
                        );
                        $this->db->where('B_02B', $id[$i]);
                        $this->db->update('pensiun_pengajuan', $data);

                        // echo json_encode($data);
                    }
                    for ($x = 0; $x < sizeof($ids); $x++) {
                        $data = array(
                            'id_pengajuan' => $ids[$x],
                            'nip_acc' => $this->session->userdata('B_02B'),
                            'status_acc' => 6, //TOLAK BKN
                            // 'jabatan_acc'=>,
                            'created_at' => date('Y:m:d H:i:s'),

                        );

                        $this->db->where('id_pengajuan', $ids[$x]);
                        $this->db->update('pensiun_pengajuan_acc', $data);
                        // echo json_encode($data);

                    }
                } else {
                    redirect_back();
                }


                // echo json_encode($sql);


                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');

                redirect_back();
                //  return redirect('bup');
            }
        }
	//END
    
    // BKN ACC
        public function bkn_acc()
        {
            $data['row'] = $this->M_bup->get_bkn_acc()->result_array();

            $this->template->load('template', 'admin/bup/bkn/view_bkn_acc', $data);
        }

    // END

    // BKN TOLAK
        public function bkn_tolak()
        {
            $data['row'] = $this->M_bup->get_bkn_tolak()->result_array();

            $this->template->load('template', 'admin/bup/bkn/view_bup_tms', $data);
        }
    // END
//----------------------------------------------END MENU BKN-----------------------------------------------------------------------//

//----------------------------------------------CRUD TABLE AHLIWARIS--------------------------------------------------------------//

    //UPDATE SORT SUTRI
        public function updatesort()
        {
            $sortable = $this->input->post('sortable');
            // echo json_encode($sortable);
            $this->M_bup->updatesort_sutri($sortable);
            $this->session->set_flashdata('message', '<strong>Success!</strong> Menu position update.');
        }
    // END UPDATE SORT

    //UPDATE SORT ANAK
	    public function updatesort_anak()
        {
            $anak = $this->input->post('anak');
            // echo json_encode($sortable);
            $this->M_bup->updatesort_anak($anak);
            $this->session->set_flashdata('message', '<strong>Success!</strong> Menu position update.');
        }
    // END UPDATE SORT  

	// ADD ANAK
	    public function insert_anak()
        {
            $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Ditambahkan");
                redirect_back();
            } else {
                $data = array(
                    "KF_01" => $_POST['KF_01'],
                    "KF_02" => $_POST['KF_02'],
                    "KF_03" => $_POST['KF_03'],
                    "KF_04" => $_POST['KF_04'],
                    "KF_05" => $_POST['KF_05'],
                    "KF_08" => $_POST['KF_08'],
                    "KF_09" => $_POST['KF_09'],
                    "KF_10" => $_POST['KF_10'],
                    "keterangan" => $_POST['keterangan'],
                    "status" => $_POST['status'],
                    'updated_at' => date('Y-m-d H:i:s')
                );

                // $this->dbeps->insert('MASTKEL1',$data);
                $this->db->insert('MASTKEL1', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");

                redirect_back();
            }
        }
	//END

	//EDIT ANAK
	    public function edit_anak()
        {
            $this->form_validation->set_rules('ID', 'ID', 'required');
            $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Di Edit");
                redirect_back();
            } else {
                $data = array(
                    "KF_03" => $_POST['KF_03'],
                    "KF_04" => $_POST['KF_04'],
                    "KF_05" => $_POST['KF_05'],
                    "KF_08" => $_POST['KF_08'],
                    "KF_09" => $_POST['KF_09'],
                    "KF_10" => $_POST['KF_10'],
                    "keterangan" => $_POST['keterangan'],
                    "status" => $_POST['status'],
                    'updated_at' => date('Y-m-d H:i:s')
                );
                // $this->dbeps->where('ID', $_POST['ID']);
                // $this->dbeps->update('MASTKEL1',$data);
                $this->db->where('ID', $_POST['ID']);
                $this->db->update('MASTKEL1', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");
                //      $id= $this->input->post('B_02B');
                // 	redirect ('Bup/edit_bup_bkd/',$id);
                redirect_back();
            }
        }
	//END

	// ADD Sutri
	    public function insert_sutri()
        {
            $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Ditambahkan");
                redirect_back();
            } else {
                $data = array(
                    "KF_01" => $_POST['KF_01'],
                    "KF_02" => $_POST['KF_02'],
                    "KF_03" => $_POST['KF_03'],
                    "KF_04" => $_POST['KF_04'],
                    "KF_05" => $_POST['KF_05'],
                    "KF_06" => $_POST['KF_06'],
                    "KF_09" => $_POST['KF_09'],
                    "KF_10" => $_POST['KF_10'],
                    "keterangan" => $_POST['keterangan'],
                    "status" => $_POST['status'],
                    'created_at' => date('Y-m-d H:i:s')
                );

                // $this->dbeps->insert('MASTKEL1',$data);
                $this->db->insert('MASTKEL1', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");

                redirect_back();
            }
        }
	//END

	//EDIT SUTRI
	    public function edit_sutri()
        {
            $this->form_validation->set_rules('ID', 'ID', 'required');
            $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Di Edit");
                redirect_back();
            } else {
                $data = array(
                    // "KF_01" => $_POST['KF_01'],
                    // "KF_02" => $_POST['KF_02'],
                    "KF_03" => $_POST['KF_03'],
                    "KF_04" => $_POST['KF_04'],
                    "KF_05" => $_POST['KF_05'],
                    "KF_06" => $_POST['KF_06'],
                    "KF_09" => $_POST['KF_09'],
                    "KF_10" => $_POST['KF_10'],
                    "keterangan" => $_POST['keterangan'],
                    "status" => $_POST['status'],
                    'updated_at' => date('Y-m-d H:i:s')
                );
                // $this->dbeps->where('ID', $_POST['ID']);
                // $this->dbeps->update('MASTKEL1',$data);
                $this->db->where('ID', $_POST['ID']);
                $this->db->update('MASTKEL1', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");
                //      $id= $this->input->post('B_02B');
                // 	redirect ('Bup/edit_bup_bkd/',$id);
                redirect_back();
            }
        }
	//END

	//DELETE AHLIWARIS
	    public function hapus_ahli_waris($id)
        {
            $data = array(
                'deleted_at' => date('Y-m-d H:i:s')
            );
            // $this->dbeps->where('ID', $id);
            // $this->dbeps->update('MASTKEL1',$data);
            $this->db->where('ID', $id);
            $this->db->update('MASTKEL1', $data);
            redirect_back();
        }
	//END

//-----------------------------------------------END OF CRUD TABLE AHLIWARIS-------------------------------------------------------------//

//-----------------------------------------------CHAINED DROPDOWN-------------------------------------------------------------------------//

	// GET KOTA
	    function list_kota()
        {
            $id_provinsi = $this->input->post('id');

            $sub = $this->M_bup->get_kota($id_provinsi);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='00'>Pilih</option>";

            foreach ($sub as $data) {
                $lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('list_kota' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }
	//END

	//GET KECAMATAN
	    function list_kecamatan()
        {
            $id_kota = $this->input->post('id');


            $sub = $this->M_bup->get_kecamatan($id_kota);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='00'>Pilih</option>";

            foreach ($sub as $data) {
                $lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('list_kecamatan' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }
	//END

	// GET KELURAHAN
	    function list_kelurahan()
        {
            $id_kecamatan = $this->input->post('id');


            $sub = $this->M_bup->get_kelurahan($id_kecamatan);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='00'>Pilih</option>";

            foreach ($sub as $data) {
                $lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('list_kelurahan' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }
	//END

//----------------------------------------------END CHAINED DROPDOWN---------------------------------------------------------------------//

//----------------------------------------------SETTING WAKTU---------------------------------------------------------------------------//
	//SETTING WAKTU
	//BKD
	    public function setting_bkd()
        {


            $row = $this->M_bup->get_setting();
            $data = array(
                'row' => $row,
            );
            $this->template->load('template', 'admin/setting/waktu_bkd', $data); //dari tabel EPS
        }
	    public function insert_setting_bkd()
        {

            $this->form_validation->set_rules('waktu_skpd', 'waktu_skpd', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Ditambahkan");
                redirect_back();
            } else {
                $data = array(
                    "waktu_skpd" => $_POST['waktu_skpd'],
                    "waktu_pns" => $_POST['waktu_pns'],

                );

                $this->db->insert('pensiun_waktu', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");

                redirect_back();
            }
        }

	    public function edit_setting_bkd()
        {
            $this->form_validation->set_rules('id', 'id', 'required');
            $this->form_validation->set_rules('waktu_skpd', 'waktu_skpd', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Di Edit");
                redirect_back();
            } else {
                $data = array(
                    "waktu_skpd" => $_POST['waktu_skpd'],
                    "waktu_pns" => $_POST['waktu_pns'],
                );
                $this->db->where('id', $_POST['id']);
                $this->db->update('pensiun_waktu', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");
                //      $id= $this->input->post('B_02B');
                // 	redirect ('Bup/edit_bup_bkd/',$id);
                redirect_back();
            }
        }

	    function list_waktu_skpd()
        {
            $skpd = $this->input->post('A_01');

            $sub = $this->M_bup->get_list_skpd($skpd);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='0'>Pilih</option>";

            foreach ($sub as $data) {
                $lists .= "<option value='" . $data->waktu_skpd . "'>" . $data->waktu_skpd . " Hari</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('l_waktu_skpd' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }

        function list_tahun()
        {
            $skpd = $this->input->post('A_01');

            $sub = $this->M_bup->get_list_tahun($skpd);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='0'>Pilih</option>";

            foreach ($sub as $data) {
                $lists .= "<option value='" . $data->thpensiun . "'>" .$data->thpensiun . "</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('l_tahun' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }

        function list_tmt()
        {
            $skpd = $this->input->post('A_01');
            $thn = $this->input->post('thpensiun');
          

            $sub = $this->M_bup->get_list_tmt($skpd,$thn);

            // Buat variabel untuk menampung tag-tag option nya
            // Set defaultnya dengan tag option Pilih
            $lists = "<option value='0'>Pilih</option>";

            foreach ($sub as $data) {
              
                $lists .= "<option value='" . $data->tmtpensiun . "'>" . date_indo($data->tmtpensiun )  . "</option>"; // Tambahkan tag option ke variabel $lists
            }

            $callback = array('l_tmt' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

            echo json_encode($callback); // konversi varibael $callback menjadi JSON
        }

	//END
	//SKPD
	    public function setting_skpd()
        {
            $id = $this->session->userdata('A_01');
            $row = $this->M_bup->get_setting($id);
            $setting = $this->M_bup->get_setting_skpd($id);
            $data = array(
                'row' => $row,
                'setting' => $setting
            );
            // echo json_encode($data);
            $this->template->load('template', 'admin/setting/waktu_skpd', $data); //dari tabel EPS
        }
	    public function insert_setting_skpd()
        {

            $this->form_validation->set_rules('A_01', 'A_01', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', "Data Gagal Ditambahkan");
                redirect_back();
            } else {
                $data = array(
                    "waktu_skpd" => $_POST['waktu_skpd'],
                    "waktu_bkd" => $_POST['waktu_bkd'],

                );

                $this->db->insert('pensiun_waktu', $data);
                $this->session->set_flashdata('sukses', "Data Berhasil Diedit");

                redirect_back();
            }
        }
	//END
    
    //EXPIRED
        function expired()
        {
            $bulan = $this->input->post('sewa');
            $tanggalSekarang = date('d-m-Y');
            $tanggalDuedate = date("d-m-Y", strtotime($tanggalSekarang . ' + ' . $bulan . ' Days'));
            echo "tanggal sekarang :" . $tanggalSekarang . "<br>";
            echo "tanggal overdue :" . $tanggalDuedate;
        }
    //END
//----------------------------------------------END SETTING WAKTU---------------------------------------------------------------------//

//----------------------------------------------ACTION------------------------------------------------------------------------------//

	// Create zip
        public function createzip()
            {


                // Read file from path
                if ($this->input->post('but_createzip1') != NULL) {


                    // File path
                    // $filepath1 = FCPATH.'/uploads/image1.jpg';
                    // $filepath2 = FCPATH.'/uploads/document/users.csv';
                    // $filepath1 = FCPATH.;
                    $filepath2 = FCPATH . 'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf';

                    // Add file
                    $this->zip->read_file($filepath2);
                    // $this->zip->read_file($filepath2);
                    // echo json_encode($filepath1);

                    // Download
                    $filename = "backup.zip";
                    $this->zip->download($filename);
                }

                // Read files from directory
                if ($this->input->post('but_createzip2') != NULL) {

                    // File name
                    $filename = "backup.zip";
                    // Directory path (uploads directory stored in project root)
                    $path = 'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf';

                    // Add directory to zip
                    // echo json_encode($path);
                    $this->zip->read_dir($path);

                    // Save the zip file to archivefiles directory
                    $this->zip->archive(FCPATH . '/archivefiles/' . $filename);

                    // Download
                    // echo json_encode($filename);
                    $this->zip->download($filename);


                    // Load view
                    $this->template->load('template', 'admin/bup/bkd/view_bup_acc');
                }
            }
    //END

    // DOWNLOAD
        public function lakukan_download($filename = NULL)
            {
                $filename = '1';

                // force_download($file);
                // load download helder
                $this->load->helper('download');
                // read file contents
                // $data = file_get_contents('http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf'.$filename);
                $data = 'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf' . $filename;

                force_download($filename, $data);
            }

        function download($id)
            {
                $data = $this->db->get_where('pensiun_pengajuan', ['B_02B' => $id])->row();
                force_download('views/sk/' . $data->file_sk, NULL);
            }

	    function download_file($name, $path, $path_2, $path_3, $path_4, $path_5, $path_6)
            {


                // die();
                // $ci = get_instance();
                $this->load->helper('download');
                $paths = 'https://' . $path . '/' . $path_2 . '/' . $path_3 . '/' . $path_4 . '/' . $path_5 . '/' . $path_6;
                force_download($name, file_get_contents($paths));
                // echo json_encode($name);
            }

    //END DOWNLOAD

    // UPLOAD SK
        function upload_sk()
        {
            $config['upload_path'] = './views/sk/'; //path folder
            $config['allowed_types'] = 'pdf|docx'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

            $this->upload->initialize($config);
            if (!empty($_FILES['sk']['name'])) {

                if ($this->upload->do_upload('sk')) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './views/sk/' . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['quality'] = '50%';
                    $config['width'] = 600;
                    $config['height'] = 400;
                    $config['new_image'] = './views/sk/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $file = $gbr['file_name'];
                    $id = $this->input->post('id', true);
                    $status = $this->input->post('status_bkn');
                    // $id=$this->input->post('id');
                    // $nosk=$this->input->post('no_sk');
                    // $this->M_bup->simpan_upload($file,$nosk,$id);
                    // echo "Image berhasil diupload";

                    $nosk = $this->input->post('no_sk', true);
                    $data = array(
                        // 'B_02B'          =>$id,
                        'file_sk'         => $file,
                        'no_sk'          => $nosk,
                        'status_bkn'	=> $status
                    );
                    $this->db->where('B_02B', $id);
                    $this->db->update('pensiun_pengajuan', $data);
                    redirect_back();
                    // echo json_encode($data);

                }
            } else {
                echo "Image yang diupload kosong";
            }
        }
    //END 

    //DOWNLOAD BULK

        function download_bulk($nip)
            {
                // $kodes = '04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                $token = '!BKDoyee123';

                $params = array(
                    'token' => $token,
                    'kodes' => $kodes,
                    'nip' => $nip
                );

                $res = ApiClientPost($params);
                $result = json_decode($res, TRUE);
                $efilenya = $result['data'];
                // echo json_encode($efilenya);
                // die();


                foreach ($efilenya as $key => $val) {

                    $namej = str_replace(' ', '_', $val['nama_jenis']);
                    $namek = str_replace('(', '', $namej);
                    $namel = str_replace(')', '', $namek);
                    $name_f = $namel . '_' . $nip . '.pdf';

                    $paths = $val['efile'];

                    // echo $paths;
                    // die();
                    $this->load->helper('download');
                    $paths = $val['efile'];
                    force_download($name_f, file_get_contents($paths));
                }
            }

        function download_bulk_v2_($nip)
            {
                ob_start();
                $this->load->library('zip');
                $this->load->helper('download');

                // $kodes = '04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                $token = '!BKDoyee123';

                $params = array(
                    'token' => $token,
                    'kodes' => $kodes,
                    'nip' => $nip
                );

                $res = ApiClientPost($params);
                $result = json_decode($res, TRUE);
                $efilenya = $result['data'];

                $this->load->library('zip');
                $this->load->helper('download');

                // create new folder 
                // $fldr = './home/bkd/domains/sinaga.bkd.jatengprov.go.id/public_html/purna_tugas/'.$nip;
                // $fldr = './assets/'.$nip;
                // $this->zip->add_dir($fldr);

                

                foreach ($efilenya as $val) {

                    // $namej = str_replace(' ', '_', $val['nama_jenis']);
                    // $namek = str_replace('(', '', $namej);
                    // $namel = str_replace(')', '', $namek);
                    $namel = kodefile_yk($val['id_jenis']);
                                // $name_f = $namel.'_'.$row->B_02B.'.pdf';
                    $name_f = $namel . '_' . $nip . '.pdf';

                    $paths = $val['efile'];
                    $this->zip->add_data($name_f, file_get_contents($paths));

                    // echo $paths;
                    // die();
                    // $this->load->helper('download');
                    // $paths = $val['efile'];
                    // force_download($name_f, file_get_contents($paths));
                    
                }
                $this->zip->archive($nip.'.zip');
                $this->zip->download($nip . '.zip');


            }

        function download_bulk_v2($nip)
            {
                // ob_start();
                $this->load->library('zip');
                $this->load->helper('download');

                // $kodes = '04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                $token = '!BKDoyee123';

                $params = array(
                    'token' => $token,
                    'kodes' => $kodes,
                    'nip' => $nip
                );

                $res = ApiClientPost($params);
                $result = json_decode($res, TRUE);
                $efilenya = $result['data'];

                $this->load->library('zip');
                $this->load->helper('download');

                // create new folder 
                // $fldr = './home/bkd/domains/sinaga.bkd.jatengprov.go.id/public_html/purna_tugas/'.$nip;
                // $fldr = './assets/'.$nip;
                // $this->zip->add_dir($fldr);

                
                // echo json_encode($efilenya);
                // die();

                foreach ($efilenya as $val) {

                    
                    if($val['id_jenis'] != NULL){
                        
                        $namel = kodefile_yk($val['id_jenis']);
                        // $name_f = $namel.'_'.$row->B_02B.'.pdf';
                        $name_f = $namel . '_' . $nip . '.pdf';

                        $paths = $val['efile'];

                        // echo $paths.'<br/>';
                        $this->zip->add_data($name_f, file_get_contents($paths));
                    }
                    

                    // echo json_encode($paths);
                    // die();
                    // $this->load->helper('download');
                    // $paths = $val['efile'];
                    // force_download($name_f, file_get_contents($paths));
                    
                }
                $this->zip->archive($nip.'.zip');
                $this->zip->download($nip.'.zip');


            }


        function download_bulk_jkt($nip)
            {
                // ob_start();
                $this->load->library('zip');
                $this->load->helper('download');

                // $kodes = '04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
                $kodes = '28_7,02,150_1,16,31,28_8,10,23_3,25,28_9,27,47_7,47_9,19,29_2';
                $token = '!BKDoyee123';

                $params = array(
                    'token' => $token,
                    'kodes' => $kodes,
                    'nip' => $nip
                );

                $res = ApiClientPost($params);
                $result = json_decode($res, TRUE);
                $efilenya = $result['data'];

                $this->load->library('zip');
                $this->load->helper('download');

                // create new folder 
                // $fldr = './home/bkd/domains/sinaga.bkd.jatengprov.go.id/public_html/purna_tugas/'.$nip;
                // $fldr = './assets/'.$nip;
                // $this->zip->add_dir($fldr);

                

                foreach ($efilenya as $val) {

                    // $namej = str_replace(' ', '_', $val['nama_jenis']);
                    // $namek = str_replace('(', '', $namej);
                    // $namel = str_replace(')', '', $namek);
                    if($val['id_jenis'] != NULL){
                        $namel = kodefile_jkt($val['id_jenis']);
                                    // $name_f = $namel.'_'.$row->B_02B.'.pdf';
                        $name_f = $namel . '_' . $nip . '.pdf';
                        $paths = $val['efile'];
                        $this->zip->add_data($name_f, file_get_contents($paths));
                    }

                   

                    // echo $paths;
                    // die();
                    // $this->load->helper('download');
                    // $paths = $val['efile'];
                    // force_download($name_f, file_get_contents($paths));
                    
                }
                $this->zip->archive($nip.'.zip');
                $this->zip->download($nip . '.zip');


            }
    
    // END DOWNLOAD BULK
    

    // VERIF
        function verifikasi_efile($id_dok, $nip)
        {

            $token = '!BKDoyee123';
            $params = array(
                'token' => $token,
                'id_dokumen' => $id_dok,
                'nip_verify' => $this->session->userdata('B_02B')
            );


            $res = ApiClientPostGeneral($params, 'verif_efile');
            $result = json_decode($res, TRUE);
            $data['efilenya'] = $result['data'];

            redirect('Bup/edit_bup_bkd/' . $nip);
        }


    //END

    // UPLOAD SEMAR 
        function upload_semar($nama_dok, $path, $path_2, $path_3, $path_4, $path_5, $path_6, $nip)
        {


            $paths = 'https://' . $path . '/' . $path_2 . '/' . $path_3 . '/' . $path_4 . '/' . $path_5 . '/' . $path_6;
            $x = file_get_contents($paths);
            $params = array(
                'namafile' => $nama_dok,
                'file' => $x,
            );


            $res = ApiClientPostSemar($params, 'verif_efile');
            $result = json_decode($res, TRUE);
            $data['efilenya'] = $result['data'];

            redirect('Bup/edit_bup_bkd/' . $nip);
        }

    // END

    //CETAK DPCP
	    function cetak($id)
        {
             $query = $this->M_bup->getall($id);
             $kode = $this->session->userdata('A_01');
             
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $kasubag = $this->M_bup->getall_kasubag($kode);
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                   
                   
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'kasubag' => $kasubag,
                        'status_acc' => $status_acc, 

                    );
                //    echo json_encode($kasubag);

                    $this->load->view('admin/bup/print/print_dpcp',$data);
                }
        }
    //END

    //CETAK SK PENSIUN
	    function cetak_sk($id)
        {
            $query = $this->M_bup->getall($id);
             $kode = $this->session->userdata('A_01');
             
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $kasubag = $this->M_bup->getall_kasubag($kode);
                    $sutri = $this->M_bup->get_suami_istri($id);
                    $anak = $this->M_bup->get_anak($id);
                   
                   
                    $status_acc = $this->M_bup->get_status_pengajuan($id);
                    $data = array(
                        'row' => $row,
                        'sutri' => $sutri,
                        'anak' => $anak,
                        'kasubag' => $kasubag,
                        'status_acc' => $status_acc,

                    );
            $this->load->view('admin/bup/print/print_sk',$data);
        }
    }
    //END

     function cetak_sk_v2($id)
        {
           
            $file = $_POST['file_sk'];
            $query = $this->M_bup->getall($id,$file);
            $file = $this->M_bup->get_sk($id,$file);
             $kode = $this->session->userdata('A_01');
             
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                   
                    $data = array(
                        'row' => $row,
                        'file' => $file
                       

                    );
                    
                    // echo json_encode($data);
            $this->load->view('admin/bup/print/cetak_sk',$data);
        }
        
    }

    function cetak_($id)

	{
        $query = $this->M_bup->getall($id)->result();
		$no = 1;
        $urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/'.$id;
        $file = 'views/sk/SK_SAMPLE.pdf'; 
        $text_image = 'views/sk/196406141985101001.png'; 
        
        // Set source PDF file 
        $pdf = new Fpdi(); 
        if(file_exists("./".$file)){ 
            $pagecount = $pdf->setSourceFile($file); 
        }else{ 
            die('Source PDF not found!'); 
        } 
        
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            $tpl = $pdf->importPage($i); 
            $size = $pdf->getTemplateSize($tpl); 
            $pdf->addPage(); 
            $pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height'], TRUE); 
            
            //Put the watermark 
            $xxx_final = ($size['width']-90); 
            $yyy_final = ($size['height']-80); 
            $pdf->Image($text_image, $xxx_final, $yyy_final, 30, 40, 'png'); 
        } 
        
        // Output PDF with watermark 
        $pdf->Output();
	}

//----------------------------------------------END ACTION------------------------------------------------------------------------------//

    
}

