<?php
defined('BASEPATH') or exit('No direct script access allowed');
use setasign\Fpdi\Fpdi; 
require_once('vendor/autoload.php');

class Cetak extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		// not_login();
        $this->load->model('M_login');
	    $this->load->library('form_validation');
		
	}


    public function index() 
    {
		
		$nipPns = 196511091993032002;
		$urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/'.$nipPns;
                
        // Source file and watermark config 
		$file = 'views/sk/SK_SAMPLE_TEST.pdf'; 
		$text_image = 'views/sk/196406141985101001.png'; 
		
		// Set source PDF file 
		$pdf = new Fpdi(); 
		if(file_exists("./".$file)){ 
			$pagecount = $pdf->setSourceFile($file); 
		}else{ 
			die('Source PDF not found!'); 
		} 
		
		// Add watermark image to PDF pages 
		for($i=1;$i<=$pagecount;$i++){ 
			$tpl = $pdf->importPage($i); 
			$size = $pdf->getTemplateSize($tpl); 
			$pdf->addPage(); 
			$pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height'], TRUE); 
			
			//Put the watermark 
			$xxx_final = ($size['width']-89); 
			$yyy_final = ($size['height']-80); 
			$pdf->Image($text_image, $xxx_final, $yyy_final, 0, 35, 'png'); 
		} 
		
		// Output PDF with watermark 
		$pdf->Output();
    }
    
    

    

    
}
