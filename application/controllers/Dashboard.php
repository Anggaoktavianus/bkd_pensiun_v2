<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		not_login();
		 $this->load->model('M_bup');
		
	}

	// FILTER DASHBOARD
     public function index()
	{
		$nip = $this->input->get('B_02B');
		$thn = $this->input->get('thpensiun');
		$tmt = $this->input->get('tmtpensiun');
		$idskpd = $this->session->userdata('A_01');
		
                $data['filter'] = $this->M_bup->get_history($nip)->result_array();
		$data['row'] = $this->M_bup->get($nip,$thn,$tmt); 
		$data['filters'] = $this->M_bup->get_history_by_skpd($nip,$idskpd)->result_array();; 
                //     echo json_encode($data);
                $this->template->load('template', 'admin/dashboard', $data); //dari tabel EPS
		
		
	}
    // ENDFILTER DASHBOARD

    // FILTER DASHBOARD
     public function umum()
	{
		$nip = $this->input->get('B_02B');
		
                $data['filter'] = $this->M_bup->get_history($nip)->result_array();
		$data['row'] = $this->M_bup->get($nip);
                //     echo json_encode($data);
                $this->template->load('template_umum', 'admin/dashboard', $data); //dari tabel EPS
		
		
	}
    // ENDFILTER DASHBOARD

	


	
}
