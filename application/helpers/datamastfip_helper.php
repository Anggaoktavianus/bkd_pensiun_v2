<?php
    function getDataMastfip($nip){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE B_02B = ?";

        $query = $dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataMastfipByI_05($I_05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE I_05 = ?";

        $query = $dbeps->query($sql, array($I_05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getKepalaDinasByA01A05($A01A05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = ?
        AND I_5A = '1'";

        $query = $dbeps->query($sql, array($A01A05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getKepalaDinasPenggantiByA01A05($A01A05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbsinaga = $ci->load->database('sinaga', TRUE);
        

        $sql = 
        "SELECT *
        FROM `pejabat_pengganti`
        WHERE unitkerja = ?";

        $query = $dbsinaga->query($sql, array($A01A05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function kodestaff(){

        $data = 'B200201000';
        return $data;
    }

    function getDataMastfipByAstaff(){
        // get data mastfip by kolom A 1 - 5 (Pencarian Staff Subbidang Formasi)
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT * FROM MASTFIP08 WHERE A_01 = 'B2' AND A_02 = 00 AND A_03 = 20 AND A_04 = 10 AND A_05 = 00 AND I_5A != 1";

        $query = $dbeps->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataMastfipByAstr($A_01,$A_02,$A_03,$A_04,$A_05,$I_5A){
        // get data mastfip by kolom A 1 - 5 (Pencarian Struktural)
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE A_01 = ?
        AND A_02 = ?
        AND A_03 = ?
        AND A_04 = ?
        AND A_05 = ?
        AND I_5A = ?
        ";

        $query = $dbeps->query($sql, array($A_01,$A_02,$A_03,$A_04,$A_05,$I_5A));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataUserNewEps($nip){
        $ci = get_instance();

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `USER_NEW`
        WHERE username = ?";

        $query = $dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getUmpegMastfip($A_01){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE A_01 = ?";

        $query = $dbeps->query($sql, array($A_01));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function formatTulisNIP($nip){
        $fixnip = '';

        $first = substr($nip, 0, 8);
        $second = substr($nip, 8, 6);
        $third = substr($nip, 14, 1);
        $fourth = substr($nip, 15, 3);

        $fixnip = $first.' '.$second.' '.$third.' '.$fourth;

        return $fixnip;
    }

    function getPLT($nip){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbsinaga = $ci->load->database('sinaga', TRUE);
        

        $sql = 
        "SELECT *
        FROM pejabat_pengganti
        WHERE nip_pengganti = ?";

        $query = $dbsinaga->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function kodeBidangFormasiPengembangan(){
        $kode = 'B200201000';
        return $kode;
    }

    function namaBidangFormasiPengembangan(){
        $kode = 'STAFF SUB BIDANG FORMASI DAN PENGEMBANGAN';
        return $kode;
    }

    function cari_kode_umpeg($lok){
        switch ($lok) {
            // case 'B4'	: $kumpeg="B400001000";break;
            // case 'B5'	: $kumpeg="B500001000";break;
            // case 'D0'	: $kumpeg="D000001000";break;
            // case 'D2'	: $kumpeg="D200001000";break;
            // case 'D3'	: $kumpeg="D300001000";break;
            // case 'D5'	: $kumpeg="D500001000";break;
            // case 'D6'	: $kumpeg="D600103000";break;
            // case 'D7'	: $kumpeg="D700001000";break;
            // case 'D9'	: $kumpeg="D900001000";break;
            // case 'E1'	: $kumpeg="E100001000";break;
            // case 'E2'	: $kumpeg="E200001000";break;
            // case 'E3'	: $kumpeg="E300001000";break;
            // case 'E4'	: $kumpeg="E400001000";break;
            // case 'E5'	: $kumpeg="E500001000";break;
            // case 'E6'	: $kumpeg="E600001000";break;
            // case 'E7'	: $kumpeg="E700001000";break;
            // case 'F1'	: $kumpeg="F100001000";break;
            case 'A2'	: $kumpeg="A200313300";break;
            case 'A3'	: $kumpeg="A300101000";break;
            case '29'	: $kumpeg="2900100000";break;
            case 'C1'	: $kumpeg="C100001000";break;
            case '80'	: $kumpeg="8000332000";break;
            case '81'	: $kumpeg="8100332000";break;
            case '82'	: $kumpeg="8200232000";break;
            case '83'	: $kumpeg="8300302000";break;
            case '84'	: $kumpeg="8400231000";break;
            case '85'	: $kumpeg="8500231000";break;
            case '86'	: $kumpeg="8600100000";break;
            default		: $kumpeg=$lok."00103000";break;
        }
        return $kumpeg;
    }


    function download_file($name, $path){
        $ci = get_instance();
        $$ci->load->helper('download');
        force_download($name,file_get_contents($path));

    }

    
?>