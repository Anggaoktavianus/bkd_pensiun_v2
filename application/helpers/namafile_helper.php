<?php
function kodefile_yk($kodes){

    switch ($kodes) {

    case '28_7':
            $nama = 'DPCP';
            break;
	case '02':
            $nama = 'SK_CPNS';
            break;
    case '150_1':
            $nama = 'SK_KP*_';
            break;
    case '16':
            $nama = 'SK_PMK';
            break;
    case '31':
            $nama = 'SKP_thn';
            break;
    case '28_8':
            $nama = 'DSK';
            break;
	case '10':
            $nama = 'AKTA_NIKAH';
            break;
    case '23_3':
            $nama = 'AKTA_ANAK';
            break;
    case '25':
            $nama = 'HD';
            break;
     case '28_9':
            $nama = 'PIDANA';
            break;
    case '29_2':
            $nama = 'AKTA_NIKAH';
            break;
    case '27':
            $nama = 'SKJAB';
            break;
    case '47_7':
            $nama = 'SKCLTN';
            break;
    case '47_9':
            $nama = 'SKHENTIS';
            break;
        
	case '00':
            $nama = 'SUKET';
            break;
    default:
            $nama = "-";
            break;
    }


    return $nama;
    

}

function kodefile_jkt($kodes){

    switch ($kodes) {

	case '28_7':
            $nama = 'DPCP';
            break;
	case '02':
            $nama = 'SKCP';
            break;
    case '150_1':
            $nama = 'SKKP';
            break;
    case '16':
            $nama = 'SKPMK';
            break;
    case '31':
            $nama = 'SKP1THN';
            break;
    case '28_8':
            $nama = 'DSK';
            break;
	case '10':
            $nama = 'AKTA_NIKAH';
            break;
    case '23_3':
            $nama = 'AKTA_ANAK';
            break;
    case '25':
            $nama = 'SUPERHD';
            break;
     case '28_9':
            $nama = 'SUPERPDNA';
            break;
    case '29_2':
            $nama = 'AKTA_NIKAH';
            break;
    case '27':
            $nama = 'SKJAB';
            break;
    case '47_7':
            $nama = 'SKCLTN';
            break;
    case '47_9':
            $nama = 'SKHENTIS';
            break;
        
	case '00':
            $nama = 'SUKET';
            break;
       
        default:
            $nama = "-";
            break;
    }


    return $nama;
    

}

function kodefile($kodes){

    switch ($kodes) {

	case '02':
            $nama = 'SK_CPNS';
            break;
        case '03':
            $nama = 'SK_PNS';
            break;
	case '04_11':
            $nama = 'KONVERSI_NIP';
            break;
        case '04_12':
            $nama = 'KARPEG';
            break;
	case '10':
            $nama = 'AKTA_NIKAH';
            break;
        case '11':
            $nama = 'KARIS/KARSU';
            break;
	case '16':
            $nama = 'SK_PMK';
            break;
        case '23_3':
            $nama = 'AKTA_ANAK';
            break;
	case '28_7':
            $nama = 'DPCP';
            break;
        case '28_8':
            $nama = 'DSK';
            break;
	case '30_14':
            $nama = 'DP3 TAHUN 2014 + SKP 2014';
            break;
        case '31':
            $nama = 'SKP';
            break;
	case '44_1':
            $nama = 'KTP';
            break;
	case '44_2':
            $nama = 'KK';
            break;
	case '150_1':
            $nama = 'SK_KP_GOLRU';
            break;
        case '150_2':
            $nama = 'GAJI_BERKALA_TERAKHIR';
            break;
	case '00':
            $nama = 'SUKET';
            break;
        
        default:
            $nama = "-";
            break;
    }


    return $nama;
    

}

function namafilenya($kodes){

    switch ($kodes) {

	case 'SK CPNS':
            $nama = 'SK_CPNS';
            break;
        case 'SK PNS':
            $nama = 'SK_PNS';
            break;
	case 'KONVERSI NIP':
            $nama = 'KONVERSI_NIP';
            break;
        case 'KARTU PEGAWAI ( KARPEG )':
            $nama = 'KARPEG';
            break;
	case 'SURAT NIKAH / AKTA PERNIKAHAN':
            $nama = 'AKTA_NIKAH';
            break;
        case 'KARIS / KARSU':
            $nama = 'KARIS/KARSU';
            break;
	case 'SK PENINJAUAN MASA KERJA':
            $nama = 'SK_PMK';
            break;
        case 'AKTA KELAHIRAN ANAK DARI PEGAWAI':
            $nama = 'AKTA_ANAK';
            break;
	case 'DATA PENERIMA CALON PENSIUN ( DPCP )':
            $nama = 'DPCP';
            break;
        case 'DAFTAR SUSUNAN KELUARGA':
            $nama = 'DSK';
            break;
	case 'DP3 TAHUN 2014 + SKP 2014':
            $nama = 'DP3 TAHUN 2014 + SKP 2014';
            break;
        case 'SKP (SASARAN KERJA PEGAWAI)':
            $nama = 'SKP';
            break;
	case 'KARTU TANDA PENDUDUK ( KTP )':
            $nama = 'KTP';
            break;
	case 'KARTU KELUARGA ( KK )':
            $nama = 'KK';
            break;
	case 'SKKP TERAKHIR':
            $nama = 'SK_KP_GOLRU';
            break;
        case 'GAJI BERKALA TERAKHIR':
            $nama = 'GAJI_BERKALA_TERAKHIR';
            break;
	case 'SK PENSIUN':
            $nama = 'SUKET';
            break;
        
        default:
            $nama = "-";
            break;
    }


    return $nama;
    

}

function namafilewjb($kodes){

    switch ($kodes) {

	
	case 'DATA PENERIMA CALON PENSIUN ( DPCP )':
            $nama = 'DATA PENERIMA CALON PENSIUN ( DPCP )';
            break;
            case 'SK CPNS':
            $nama = 'SK CPNS';
            break;
            case 'SKKP TERAKHIR':
            $nama = 'SKKP TERAKHIR';
            break;
            case 'SKP (SASARAN KERJA PEGAWAI)':
            $nama = 'SKP (SASARAN KERJA PEGAWAI)';
            break;
            case 'DAFTAR SUSUNAN KELUARGA':
            $nama = 'DAFTAR SUSUNAN KELUARGA';
            break;
            case 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN':
            $nama = 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN';
            break;
            case 'SURAT PERNYATAAN PIDANA':
            $nama = 'SURAT PERNYATAAN PIDANA';
            break;
       
	
        default:
            $nama = "-";
            break;
    }


    return $nama;
    

}

function namafilewjb_kode($kodes){

    switch ($kodes) {

	
	case 'DATA PENERIMA CALON PENSIUN ( DPCP )':
            $nama = '28_7';
            break;
            case 'SK CPNS':
            $nama = '02';
            break;
            case 'SKKP TERAKHIR':
            $nama = '150_1';
            break;
            case 'SKP (SASARAN KERJA PEGAWAI)':
            $nama = '31';
            break;
            case 'DAFTAR SUSUNAN KELUARGA':
            $nama = '28_8';
            break;
            case 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN':
            $nama = '25';
            break;
            case 'SURAT PERNYATAAN PIDANA':
            $nama = '28_9';
            break;
       
	
        default:
            $nama = "";
            break;
    }


    return $nama;
    

}
function namafilewjb_kodejd($kodes){

    switch ($kodes) {

	
	case 'DATA PENERIMA CALON PENSIUN ( DPCP )':
            $nama = '28_7';
            break;
            case 'SK CPNS':
            $nama = '02';
            break;
            case 'SKKP TERAKHIR':
            $nama = '150_1';
            break;
            case 'SKP (SASARAN KERJA PEGAWAI)':
            $nama = '31';
            break;
            case 'AKTA KELAHIRAN ANAK DARI PEGAWAI':
            $nama = '23_3';
            break;
            case 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN':
            $nama = '25';
            break;
            case 'AKTA KEMATIAN':
            $nama = '29';
            break;
       
	
        default:
            $nama = "";
            break;
    }


    return $nama;
    

}

function namafilewjb_jd($kodes){

    switch ($kodes) {

	
	case 'DATA PENERIMA CALON PENSIUN ( DPCP )':
            $nama = 'DATA PENERIMA CALON PENSIUN ( DPCP )';
            break;
            case 'SK CPNS':
            $nama = 'SK CPNS';
            break;
            case 'SKKP TERAKHIR':
            $nama = 'SKKP TERAKHIR';
            break;
            case 'SKP (SASARAN KERJA PEGAWAI)':
            $nama = 'SKP (SASARAN KERJA PEGAWAI)';
            break;
            case 'AKTA KELAHIRAN ANAK DARI PEGAWAI':
            $nama = 'AKTA KELAHIRAN ANAK DARI PEGAWAI';
            break;
            case 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN':
            $nama = 'SK HUKUMAN DISIPLIN/TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN';
            break;
            case 'AKTA KEMATIAN':
            $nama = 'AKTA KEMATIAN';
            break;
       
	
        default:
            $nama = "-";
            break;
    }


    return $nama;
    

}
function kodegol($kodes){

    switch ($kodes) {

	case 'I/a':
            $gol = '11';
            break;
    case 'I/b':
            $gol = '12';
            break;
    case 'I/c':
            $gol = '13';
            break;
    case 'I/d':
            $gol = '14';
            break;
    case 'II/a':
            $gol = '21';
            break;
    case 'II/b':
            $gol = '22';
            break;
    case 'II/c':
            $gol = '23';
            break;
    case 'II/d':
            $gol = '24';
            break;
	case 'III/a':
            $gol = '31';
            break;
    case 'III/b':
            $gol = '32';
            break;
    case 'III/c':
            $gol = '33';
            break;
    case 'III/d':
            $gol = '34';
            break;
    case 'IV/a':
            $gol = '41';
            break;
    case 'IV/b':
            $gol = '42';
            break;
    case 'IV/c':
            $gol = '43';
            break;
    case 'IV/d':
            $gol = '44';
            break;
    }


    return $gol;
    

}


?>