<?php

    class Fungsi
    {
        protected $ci;

        function __construct()
        {
            $this->ci = &get_instance();
        }
        function user_login()
        {
            $this->ci->load->model('m_user');
            $id = $this->ci->session->userdata('id');
            $user_data = $this->ci->m_user->getall($id)->row();
            return $user_data;
            # code...
        }

         function last_position()
        {
            $this->ci->load->model('M_bup');
            $id = $this->ci->session->userdata('B_02B');
            $user_data = $this->ci->M_bup->getall($id)->row();
            return $user_data;
            # code...
        }

        function kunci_nominatif()
        {
            $this->ci->load->model('M_bup');
            $id = $this->ci->session->userdata('waktu_skpd');
            $user_data = $this->ci->M_bup->get_waktu_skpd($id)->row();
            return $user_data;
            # code...
        }
        
        
    }
