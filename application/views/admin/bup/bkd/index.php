<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Calon BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Calon BUP </li>
					</ol>
				</div>
			</div>
			
			
		</div>
	    <!-- /.container-fluid -->
	</section>
	<section class="content">
	<div class="container-fluid">
		<!-- SELECT2 EXAMPLE -->
		<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Proses Calon BUP  <?php echo date_indo('2017-10-5');?></h3>
			
			<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse">
			<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove">
			<i class="fas fa-times"></i>
			</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form method="get" action="<?php echo base_url("Bup/")?>">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						<label>SKPD</label>
						<?php 
							foreach ($row as $x ) {?>

											
						<?php } ?> 
							<select name="A_01" class="form-control not-dark" id="w_skpd">
								<option value="">--Select--</option>
								<?php foreach ($skpd as $key => $data) { ?>
								<option value="<?= $data->A_01?>" <?= $data->A_01 == $x['A_01']  ? "selected" : null ?>  ><?= $data->NALOK ?></option>
								<?php } ?>
							</select>
						</div>
						<!-- /.form-group -->
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label><small><strong>Kunci Batas Waktu Nominatif?</strong> (optional)</small></label><br>
							<select name="id_waktu_skpd" id="waktu_skpd" class="form-control"  style="width: 100%;">
							<option value="">--Select--</option>
							
							</select>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col -->
					<div class="col-md-4">
						<div class="form-group">
							<label>Tahun Pensiun</label>
							<select class="form-control" name="thpensiun" id="thpensiun" style="width: 100%;">
							<option value="">--Select--</option>
							
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>TMT Pensiun</label>
							<select class="form-control select2" name="tmtpensiun"  id="tmtpensiun" style="width: 100%;">
							<option value="">--Select--</option>
							
						</div>
					</div>
					<!-- <div class="col-md-4">
					<div class="form-group">
						<label>BUP</label>
						<select class="form-control select2"  style="width: 100%;">
						<option selected="selected">Madya</option>
						<option>Madya</option>
						<option>Utama</option>
						<option>Peneliti</option>
						</select>
					</div>
					</div> --> 
				</div>
				<!-- /.row -->
				<input type="submit" class="btn btn-primary" id="show" value="Proses">
				<form method="get" action="<?php echo base_url("Bup")?>">
				<input type="submit" class="btn btn-secondary" value="Reset">
				</form>
			</form>
		</div>
		
		</div>
	
	</div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.card-body -->
    <!-- /.content -->
	<!-- Main content -->
	<section class="content" > 
		<div class="container-fluid"  >
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP <?php echo date("Y"); ?></h3><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div><br>
						<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Bup/blast_bkd')?>" id="blast" method="post">
							<?php 
							// $id_waktu_skpd = $this->uri->segment('2');
							$id_waktu_skpd = $_GET['id_waktu_skpd'];
							// echo json_encode($id_waktu_skpd);
							?>
							
							<table  id="example" class="table table-bordered table-striped main-table" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
							 			<th>Nama</th>
										<th>Jabatan</th>
										<th>Gol & Ruang</th>
										<th>Tanggal Lahir</th>
										<th>SKPD</th>
										<th>TMT</th>
										<th>Waktu</th>
										<th><input id="checkAlls" type="checkbox" /></th>
									</tr>
								</thead> 
								<tbody >
									
									    <?php $no = 1;
										foreach ($row as $data ) {
											if ($data['tmtpensiun'] > 0) {	
									?>
									<tr>
										
										<input type="hidden" name="diusulkan_bkd[]" value="1">
										<input type="hidden" name="created_at[]" >
										<input type="hidden" name="A_01[]" value="<?= $data['A_01']?>">
										<td><?=$no++?></td>
										<td><input type="hidden" name="B_02[]" value="<?= $data['B_02B'] ?>"><?= $data['B_02B'] ?></td>
										<td><input type="hidden" name="B_03[]" value="<?= $data['B_03'] ?>"><?= $data['B_03'] ?></td>
										<td><?= $data['I_JB'];?></td>
										<td><?= $data['pangkat']?> & ruang : (<?= $data['gol']?>)</td>
										<td><?= $data['B_05'] ?></td>
										<td><?= $data['skpd']?></td>
										<td><input type="hidden" name="tmt[]" value="<?= $data['tmtpensiun']?>"><?= $data['tmtpensiun']?></td>
										<td><input type="hidden" name="id_waktu_skpd[]" value="<?php if ($id_waktu_skpd > 0) { echo $data['wskpd'];} else { echo"0";}?> ">
										<?php if ($id_waktu_skpd > 0) { echo $data['wskpd'].' Hari';} else { echo"Tidak dikunci";}?>
										<input type="hidden" name="id_waktu_skpd2" value="<?php if ($id_waktu_skpd > 0) { echo $data['wskpd'];} else { echo"0";}?> ">
										</td>
										<input type="hidden" name="due_date_skpd[]" value="<?=$data['due_date_skpd']?>">
										<td><input type="checkbox" name="B_02B[]" value="<?= $data['B_02B'] ?>"<?php if($data['diusulkan_bkd'] > 0 ){echo'checked disabled';} ?>></td>
										
									</tr>
									<?php } 
                    					}?> 
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
							 			<th>Nama</th>
										<th>Jabatan</th>
										<th>Gol & Ruang</th>
										<th>Tanggal Lahir</th>
										<th>SKPD</th>
										<th>TMT</th>
										<th>Waktu</th>
										<th></th>
									</tr>	
								</tfoot>
							</table>
							<!-- < href="" class="btn btn-success btn-sm" type="submit"><i class="fas fa-arrow-circle-right">&nbsp;</i>Blast Info ke SKPD</> -->
							 <div class="row">
								<!-- <div class="col-md-6">
									<div class="form-group">
										<label><small><strong>Kunci Nominatif ? </strong> (optional)</small></label><br>
										<div class="input-group " id="reservationdate" data-target-input="nearest">
											<select name="kunci_skpd[]" class="form-control not-dark" >
											<option value="">--Select--</option>
											<option value="1">&nbsp;Ya</option>
											<option value="0">&nbsp;Tidak</option>
											</select>
										</div>
									</div>
								</div> -->
								<!-- /.col -->
								<div class="col-md-6">
									<div class="form-group">
										<label><small><strong> Blast Nominatif</strong></small></label><br>
										 <button type="submit" class="btn btn-success btn-sm" onclick="submitResult(event)"><i class="fas fa-arrow-circle-right">&nbsp;</i>Blast Info ke SKPD</button>
									</div>
									<!-- /.form-group -->
								</div>
							</div>
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>

<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>
<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
    // initialising dt table
    $(document).ready(function() {
        

        $('#example').DataTable({
            
            
            // Definition of filter to display
            
            
        } );
    } );

</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#w_skpd").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#waktu_skpd").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_waktu_skpd"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        A_01: $("#w_skpd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#waktu_skpd").html(response.l_waktu_skpd).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#w_skpd").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#thpensiun").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_tahun"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        A_01: $("#w_skpd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#thpensiun").html(response.l_tahun).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#thpensiun").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#tmtpensiun").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_tmt"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
			 A_01: $("#w_skpd").val(),
                        thpensiun: $("#thpensiun").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#tmtpensiun").html(response.l_tmt).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>


<script>
 function submitResult(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Blast info BUP ke SKPD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim BUP!'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("blast").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>

