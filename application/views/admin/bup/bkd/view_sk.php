<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Diajukan BKN</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							
							<h3 class="card-title">Data Tabel Calon BUP </h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							
							<form action="<?= site_url('bup/bup_ajuan_bkn/') ?>" method="post" enctype="multipart/form-data">
								<table  id="examples" class="table table-bordered table-striped responsive" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th  style="width: 10px;">No</th>
											<th >No. Surat Pegantar</th>
											<th>Jumlah nominatif</th>
											
										</tr>
									</thead> 
									<tbody> 
										<?php $no = 1;
											foreach ($row as $data ) {
										?>
										<tr>
											
											<td><?=$no++?></td>
											<input type="hidden" name="id_surat">
											<td>
												<form action="" method="post">
											<a href="<?= site_url('Bup/bup_ajuan_bkn/'. $data['id_no_surat_pengantar_bkd'])?>" class="btn btn-outline-primary btn-block ">
											<?= $data['surat'] ?>
											</a>
											<input type="hidden" name="id_surat" value="<?= $data['id_no_surat_pengantar_bkd'] ?>">
											</form><?= $data->surat?></td>
											<td><?= $data['tot'] ?></td>
										</tr> 
										<?php
										} ?>
									</tbody>
									<tfoot>
										<tr>
											<th>No</th>
											<th>No. Surat Pegantar</th>
											<th>Jumlah nominatif</th>
										</tr>
									</tfoot>
								</table>
								 
								
							</form>
							
							<!-- <form method='post' action='<?= base_url() ?>bup/createzip/'>
							<input type="submit" name="but_createzip1" value='Add file from path and download zip'>
							<input type="submit" name="but_createzip2" value='Add directory files and sub-directory, save archive and download zip'>
							</form>
							<a href="<?php echo base_url().'bup/lakukan_download' ?>">Download file</a> -->
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>