<style>
  select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
 
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Diajukan SKPD</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Bup/ajuan_bkn')?>" method="post">
							<table  id="examples" class="table table-bordered table-striped responsive" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>SKPD</th>
										<th>Posisi Ajuan</th>
										<th>No Surat Pengantar</th>
										<th><input id="checkAlls" type="checkbox" /></th>
										<th>Action</th>

									</tr>
								</thead> 
								<tbody> 
									<?php $no = 1;
										foreach ($row as $data ) {
									?>
							
									<tr>
										<input type="hidden" name="last_position" value="3">
										<td><?=$no++?></td>
										<td><?= $data['B_02B'] ?></td>
										<td><?= $data['nama'] ?></td>
										<td><?= $data['F_PK']?> <?= $data['pangkat']?> <?= $data['I_JB']?></td>
										<td><?= $data['nmskpd']?></td>
										<td><?php if($data['last_position'] == 1){echo'SKPD';} elseif($data['last_position']==2){ echo"BKD"; } elseif($data['last_position']==3){ echo"BKN"; } elseif($data['last_position']==4){ echo"SKPD"; }else {echo"PNS";} ?></td>
										<td><input type="hidden" name="id_no_surat_pengantar_skpd[]" value="<?= $data['id_no_surat_pengantar_skpd'] ?>"> <?= $data['nomer_surat']?></td>
										<td><input type="checkbox" name="B_02B[]" value="<?= $data['B_02B'] ?>"<?php if($data['last_position'] == 3){echo'checked disabled';} ?>></td>
										<td>
											<form action="" method="post">
											<a href="<?= site_url('Bup/edit_bup_bkd/'. $data['B_02B'])?>" class="btn btn-success btn-xs text-white">
											<i class="fas fa-download "></i>
											</a>
											<input type="hidden" name="id" value="<?= $data['B_02B'] ?>">
											</form>
											<a href="<?= site_url('Bup/cetak_sk/'. $data['B_02B'])?>" class="btn btn-primary btn-xs text-white" target="_blank"><i class="fas fa-print "></i>
										</td>
									</tr> 
									<?php
                    							} ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>SKPD</th>
										<th>Posisi Ajuan</th>
										<th>No Surat Pengantar</th>
										<th></th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
								<div class="card-header">
									<div class="row">
										<div class="col-md-6">
											<!-- <div class="form-group">
												<a href="#" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white  dropdown-toggle dropdown-icon"  data-toggle="dropdown"> <i class="fas fa-check">Download Bulk</i></a>
													<div class="dropdown-menu" role="menu">
														<a class="dropdown-item" href="<?=base_url('bup/download_bulk_v2/'.$row->B_02B)?>" target="_blank">BKN Yogya</a>
														<a class="dropdown-item" href="<?=base_url('bup/download_bulk_jkt/'.$row->B_02B)?>" target="_blank">BKN JKT</a>
													</div>
												<label><small><strong> Action</strong></small></label><br>
												<button type="submit" class="btn btn-success"><i class="fas fa-download">&nbsp;</i>Download All</button>
											</div> -->
											<!-- /.form-group -->
										</div>
									</div>
								</div>
							</form>
							<!-- <form method='post' action='<?= base_url() ?>bup/createzip/'>
							<input type="submit" name="but_createzip1" value='Add file from path and download zip'>
							<input type="submit" name="but_createzip2" value='Add directory files and sub-directory, save archive and download zip'>
							</form>
							<a href="<?php echo base_url().'bup/lakukan_download' ?>">Download file</a> -->
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
// initialising dt table
$(document).ready(function() {

	$('#examples').DataTable({
		
		// Definition of filter to display
		
		filterDropDown: {
			columns: [
				{
					idx: 4
				},
				{
					idx: 3
				}
			],
			bootstrap: true,
			// responsive: true
		}
	} );
} );

</script>
