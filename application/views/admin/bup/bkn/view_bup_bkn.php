<style>
  select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
 
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Diajukan SKPD</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Bup/acc_tolak_bkn')?>" method="post">
							<table  id="examples" class="table table-bordered table-striped responsive" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>SKPD</th>
										<th>Posisi Ajuan</th>
										<th>No Surat Pengantar</th>
										<th>Status </th>
										<th><input id="checkAlls" type="checkbox" /></th>
										<!-- <th>Action</th> -->

									</tr>
								</thead> 
								<tbody> 
									<?php $no = 1;
										foreach ($row as $data ) {
									?>
							
									<tr>
										<input type="hidden" name="last_position" value="3">
										<td><?=$no++?></td>
										<td>
											<form action="" method="post">
											<a href="<?= site_url('Bup/edit_bup_bkn/'. $data['B_02B'])?>" ><?= $data['B_02B'] ?>
											</a>
											<input type="hidden" name="id" value="<?= $data['B_02B'] ?>">
											</button>
											</form></td>
										<td><?= $data['nama'] ?></td>
										<td><?= $data['F_PK']?> <?= $data['pangkat']?> <?= $data['I_JB']?></td>
										<td><?= $data['nmskpd']?></td>
										
										<td><?php if($data['last_position'] == 1){echo'SKPD';} elseif($data['last_position']==2){ echo"BKD"; }else {echo"PNS";} ?></td>
										<td><input type="hidden" name="id_no_surat_pengantar_skpd[]" value="<?= $data['id_no_surat_pengantar_skpd'] ?>"> <?= $data['nomer_surat']?></td>
										<td><?php if($data['status_acc'] == 0){echo' <a href="#" class="btn btn-primary btn-xs">Menunggu Persetujuan BKN</a>';}else {
											
										}?></td>
										<td><input type="checkbox" name="B_02B[]" value="<?= $data['B_02B'] ?>"<?php if($data['last_position'] > 3){echo'checked disabled';} ?>></td>
										<!-- <td>
											<form action="" method="post">
											<a href="<?= site_url('Bup/edit_bup_bkn/'. $data['B_02B'])?>" class="btn btn-warning btn-xs text-white">
											<i class="fas fa-pencil-alt "></i>
											</a>
											<input type="hidden" name="id" value="<?= $data['B_02B'] ?>">
											
											</button>
											</form>
										</td> -->
									</tr> 
									<?php
                    							} ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>SKPD</th>
										<th>Posisi Ajuan</th>
										<th>Status </th>
										<th>Status</th>
										<th></th>
										<!-- <th>Action</th> -->
									</tr>
								</tfoot>
							</table>
								<div class="card-header">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label><small><strong> Action</strong></small></label><br>
												<button type="submit" class="btn btn-success" name="acc" value="<?= $page?>"><i class="fas fa-check-circle">&nbsp;</i>Acc BKN</button>
											</div>
											<!-- /.form-group -->
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label><small><strong> Action</strong></small></label><br>
												<button type="submit" class="btn btn-warning" name="tolak" value="tolak"><i class="fas fa-window-close">&nbsp;</i>Tolak BKN</button>
											</div>
											<!-- /.form-group -->
										</div>
									</div>
								</div>
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
// initialising dt table
$(document).ready(function() {

	$('#examples').DataTable({
		
		// Definition of filter to display
		dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            autoFilter: true,
            sheetName: 'Exported data',
			exportOptions: {
                    columns: [ 1, 2 ]
                },
        } ],
		filterDropDown: {
			columns: [
				{
					idx: 4
				},
				{
					idx: 3
				}
			],
			bootstrap: true,
			// responsive: true
		}
	} );
} );

</script>
 
<!-- ACC BKN -->
<script>
 function accbkn(e) {
    e.preventDefault();
    Swal.fire({
        title: 'ACC BKN?',
        text: "Setelah Acc data BUP tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Acc BUP BKN!'
    }).then((result) => {
        if (result.isConfirmed) {
          // $.ajax({
          //   type: 'POST',
          //   url: '/Bup/acc_bkd/'+<?= $row->B_02B?>,
            
          // });accbkn
            document.getElementById("accbkn").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<!-- END -->

<!-- TOLAK BKN -->
<script>
 function tolakbkn(e) {
    e.preventDefault();
    Swal.fire({
        title: 'TOLAK BUP BKN?',
        text: "Setelah Tolak BKN tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Tolak BKN!'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("tolakbkn").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<!-- END -->
<script>
    $(document).ready( function() {
    $('#excel').DataTable( {
        dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            autoFilter: true,
            sheetName: 'Exported data'
        } ]
    } );
} );
</script>