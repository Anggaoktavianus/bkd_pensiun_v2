<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">
<head>
  <meta http-equiv=Content-Type content="text/html; charset=utf-8">
  <meta name=ProgId content=Word.Document>
  <meta name=Generator content="Microsoft Word 15">
  <meta name=Originator content="Microsoft Word 15">
  <link rel=File-List href="dpcp1.fld/filelist.xml">
  <link rel=Edit-Time-Data href="dpcp1.fld/editdata.mso">
  <link rel=themeData href="dpcp1.fld/themedata.thmx">
  <link rel=colorSchemeMapping href="dpcp1.fld/colorschememapping.xml">
  <style>
  /* Font Definitions */
    @font-face
          {font-family:"Cambria Math";
          panose-1:2 4 5 3 5 4 6 3 2 4;
          mso-font-charset:0;
          mso-generic-font-family:roman;
          mso-font-pitch:variable;
          mso-font-signature:-536870145 1107305727 0 0 415 0;}
        @font-face
          {font-family:Calibri;
          panose-1:2 15 5 2 2 2 4 3 2 4;
          mso-font-charset:0;
          mso-generic-font-family:swiss;
          mso-font-pitch:variable;
          mso-font-signature:-536859905 -1073732485 9 0 511 0;}
    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal
      {mso-style-unhide:no;
      mso-style-qformat:yes;
      mso-style-parent:"";
      margin:0cm;
      mso-pagination:widow-orphan;
      font-size:12.0pt;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
      {mso-style-priority:34;
      mso-style-unhide:no;
      mso-style-qformat:yes;
      margin-top:0cm;
      margin-right:0cm;
      margin-bottom:0cm;
      margin-left:36.0pt;
      mso-add-space:auto;
      mso-pagination:widow-orphan;
      font-size:12.0pt;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
      {mso-style-priority:34;
      mso-style-unhide:no;
      mso-style-qformat:yes;
      mso-style-type:export-only;
      margin-top:0cm;
      margin-right:0cm;
      margin-bottom:0cm;
      margin-left:36.0pt;
      mso-add-space:auto;
      mso-pagination:widow-orphan;
      font-size:12.0pt;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
      {mso-style-priority:34;
      mso-style-unhide:no;
      mso-style-qformat:yes;
      mso-style-type:export-only;
      margin-top:0cm;
      margin-right:0cm;
      margin-bottom:0cm;
      margin-left:36.0pt;
      mso-add-space:auto;
      mso-pagination:widow-orphan;
      font-size:12.0pt;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
      {mso-style-priority:34;
      mso-style-unhide:no;
      mso-style-qformat:yes;
      mso-style-type:export-only;
      margin-top:0cm;
      margin-right:0cm;
      margin-bottom:0cm;
      margin-left:36.0pt;
      mso-add-space:auto;
      mso-pagination:widow-orphan;
      font-size:12.0pt;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    span.SpellE
      {mso-style-name:"";
      mso-spl-e:yes;}
    .MsoChpDefault
      {mso-style-type:export-only;
      mso-default-props:yes;
      font-family:"Calibri",sans-serif;
      mso-ascii-font-family:Calibri;
      mso-ascii-theme-font:minor-latin;
      mso-fareast-font-family:Calibri;
      mso-fareast-theme-font:minor-latin;
      mso-hansi-font-family:Calibri;
      mso-hansi-theme-font:minor-latin;
      mso-bidi-font-family:"Times New Roman";
      mso-bidi-theme-font:minor-bidi;}
    @page WordSection1
      {size:1008.0pt 612.0pt;
      mso-page-orientation:landscape;
      margin:2.0cm 37.15pt 1.0cm 42.55pt;
      mso-header-margin:35.4pt;
      mso-footer-margin:35.4pt;
      mso-paper-source:0;}
    div.WordSection1
      {page:WordSection1;}

  </style>
</head>

<body lang=EN-ID style='tab-interval:36.0pt;word-wrap:break-word'>

  <div class=WordSection1>

    <p class=MsoNormal><span style='mso-ignore:vglayout;position:
    relative;z-index:-1895824384'><span style='position:absolute;left:578px;
    top:-27px;width:51px;height:48px'><br>
    <img width=51 height=48
    src="<?= base_url('upload/dpcp.png') ?>" v:shapes="Picture_x0020_2"></span></span><![endif]><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'>
    <o:p></o:p></span></p><br><br>

    <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'>BADAN ADMINISTRASI
    KEPEGAWAIAN NEGARA<o:p></o:p></span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:
      0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
        <td width=576 valign=top style='width:431.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=493
        style='width:369.85pt;border-collapse:collapse;border:none;mso-yfti-tbllook:
        1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
        none'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>INSTANSI<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PEMERINTAH<o:p></o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:1'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PROPINSI<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>JAWA
          TENGAH<o:p></o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KOTA<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->B_04?><o:p></o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:3'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>UNIT
          KERJA<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->nmskpd?><o:p></o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:4'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PEMBAYARAN<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KASDA<o:p></o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:5'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>BUP<o:p></o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
          style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes'>
          <td width=106 valign=top style='width:79.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=19 valign=top style='width:14.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=368 valign=top style='width:276.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
          <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
          </td>
        </tr>
        </table>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=576 valign=top style='width:431.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
    </table>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'>DATA PERORANGAN
    CALON PENERIMA PENSIUN (DPCP) PEGAWAI NEGERI SIPIL<o:p></o:p></span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
    style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=1238
    style='width:928.25pt;border-collapse:collapse;border:none;mso-yfti-tbllook:
    1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
    none'>
    <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
      <td width=576 valign=top style='width:431.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
      <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;
      mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
      none'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:18.15pt'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:18.15pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>1.</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:18.15pt'>
        <p class=MsoNormal><b><u><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KETERANGAN
        PRIBADI</span></u></b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:18.15pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:18.15pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:1'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>A.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>N
        A M A<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->B_03A?> <?= $row->nama ?>, <?= $row->B_03B ?>
        <o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:2'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>B.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NIP<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->B_02B?><o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:3'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>C.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>TEMPAT/TGL.
        LAHIR<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->B_04?>
        , <?= $row->tl?><o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:4'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>D.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>JABATAN<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span class=SpellE><span style='font-size:8.0pt;
        font-family:"Times New Roman",serif'><?= $row->I_JB ?></span></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:5'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>E.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PANGKAT/GOL.RUANG/TMT
        <o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span class=SpellE><span style='font-size:8.0pt;
        font-family:"Times New Roman",serif'></span></span><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->pangkat ?>, <?= $row->F_PK ?> TMT. <?= $row->TMT?><o:p></o:p></span></p>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:6'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>F.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>GAJI
        POKOK TERAKHIR<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= number_format($row->gaji) ?><o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:7'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>H.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>MASA
        KERJA GOLONGAN<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>...
        TAHUN ... BULAN<o:p></o:p></span></p>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:8'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>J.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>MASA
        KERJA PENSIUN<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>...
        TAHUN ... BULAN<o:p></o:p></span></p>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:9'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>K.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PENINJAUAN
        MASA KERJA<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>.....TAHUN
        .....BULAN<o:p></o:p></span></p>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:10'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>L.<o:p></o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PENDIDIKAN
        DASAR<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:11;mso-yfti-lastrow:yes'>
        <td width=29 valign=top style='width:21.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=190 valign=top style='width:142.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PENGANGKATAN
        PERTAMA<o:p></o:p></span></p>
        </td>
        <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>:</span></b><span
        style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
        </td>
        <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>....<o:p></o:p></span></p>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></b></p>
        </td>
      </tr>
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
      <td width=662 valign=top style='width:496.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
       <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>B.ANAK
        KANDUNG<o:p></o:p></span></b></p>
      <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
      mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
        <td width=561 colspan=5 valign=top style='width:420.45pt;border:solid windowtext 1.0pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
       
        </td>
      </tr>
      	
      <tr style='mso-yfti-irow:1;height:11.4pt'>
        <td width=21 valign=top style='width:15.8pt;border:solid windowtext 1.0pt;
        border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NO<o:p></o:p></span></p>
        </td>
        <td width=202 valign=top style='width:151.2pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:11.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NAMA<o:p></o:p></span></p>
        </td>
        <td width=114 valign=top style='width:85.35pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:11.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>TGL
        LAHIR<o:p></o:p></span></p>
        </td>
        <td width=119 valign=top style='width:89.55pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:11.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NAMA
        AYAH/IBU<o:p></o:p></span></p>
        </td>
        <td width=105 valign=top style='width:78.55pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:11.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KETERANGAN<o:p></o:p></span></p>
        </td>
      </tr>
      <?php
          $sl = '1';
          foreach($anak as $data): ?>
      <tr style='mso-yfti-irow:2' id="<?= $data->ID ?>">
        <td width=21 valign=top style='width:15.8pt;border:solid windowtext 1.0pt;
        border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $data->KF_03?><o:p></o:p></span></p>
        </td>
        <td width=202 valign=top style='width:151.2pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $data->KF_04?><o:p></o:p></span></p>
        </td>
        <td width=114 valign=top style='width:85.35pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $data->KF_05?>
        <o:p></o:p></span></p>
        </td>
        <td width=119 valign=top style='width:89.55pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $data->nama?><o:p></o:p></span></p>
        </td>
        <td width=105 valign=top style='width:78.55pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p><?= $data->keterangan?>&nbsp;</o:p></span></p>
        </td>
      </tr>
       <?php
       $sl++;
        endforeach; ?>
     
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
    </tr>
    </table>

    <p class=MsoNormal style='margin-left:-6.05pt'><span style='font-size:8.0pt;
    font-family:"Times New Roman",serif'><span style='mso-spacerun:yes'>    </span><b>2.
    KETERANGAN KELUARGA<o:p></o:p></b></span></p>

    <p class=MsoNormal style='margin-left:-6.05pt'><b><span style='font-size:8.0pt;
    font-family:"Times New Roman",serif'><span style='mso-tab-count:1'>   </span><span
    style='mso-spacerun:yes'>         </span>A. NAMA SUAMI/ISTERI<o:p></o:p></span></b></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
    style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:
    0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>
    <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
      <td width=576 valign=top style='width:432.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
      <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
      style='margin-left:16.8pt;border-collapse:collapse;border:none;mso-border-alt:
      solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
        <td width=33 rowspan=2 valign=top style='width:24.4pt;border:solid windowtext 1.0pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'>NO.<o:p></o:p></span></p>
        </td>
        <td width=173 rowspan=2 valign=top style='width:129.55pt;border:solid windowtext 1.0pt;
        border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
        style='font-size:8.0pt;line-height:115%;font-family:"Times New Roman",serif'>NAMA<o:p></o:p></span></p>
        </td>
        <td width=248 colspan=2 valign=top style='width:186.35pt;border:solid windowtext 1.0pt;
        border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
        style='font-size:8.0pt;line-height:115%;font-family:"Times New Roman",serif'>TANGGAL<o:p></o:p></span></p>
        </td>
        <td width=85 rowspan=2 valign=top style='width:63.7pt;border:solid windowtext 1.0pt;
        border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'>SUAMI/ISTERI KE<o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:1'>
        <td width=100 valign=top style='width:75.2pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'>LAHIR<o:p></o:p></span></p>
        </td>
        <td width=148 valign=top style='width:111.15pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'>KAWIN<o:p></o:p></span></p>
        </td>
      </tr>
      	<?php
          $sl = '1';
          foreach($sutri as $data): ?>
      <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes' id="<?= $data->ID ?>">
        <td width=33 valign=top style='width:24.4pt;border:solid windowtext 1.0pt;
        border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:
        solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'><?= $data->KF_03?><o:p></o:p></span></p>
        </td>
        <td width=173 valign=top style='width:129.55pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'><?= $data->KF_04?><o:p></o:p></span></p>
        </td>
        <td width=100 valign=top style='width:75.2pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'><?= $data->KF_05?><o:p></o:p></span></p>
        </td>
        <td width=148 valign=top style='width:111.15pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'><?= $data->KF_06?><o:p></o:p></span></p>
        </td>
        <td width=85 valign=top style='width:63.7pt;border-top:none;border-left:
        none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
        mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
        mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal style='line-height:115%'><span style='font-size:8.0pt;
        line-height:115%;font-family:"Times New Roman",serif'><?= $data->keterangan?><o:p></o:p></span></p>
        </td>
      </tr>
      <?php
          $sl++;
          endforeach; ?>
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
      <td width=661 valign=top style='width:495.7pt;padding:0cm 5.4pt 0cm 5.4pt'>
      <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;
      mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
      none'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
        <td width=22 valign=top style='width:16.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>3.<o:p></o:p></span></p>
        </td>
        <td width=623 valign=top style='width:467.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>ALAMAT
        SESUDAH PENSIUN :<o:p></o:p></span></b></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:1;mso-row-margin-left:16.8pt'>
        <td style='mso-cell-special:placeholder;border:none;padding:0cm 0cm 0cm 0cm'
        width=22><p class='MsoNormal'>&nbsp;</td>
        <td width=623 valign=top style='width:467.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->alamat ?><o:p></o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:2'>
        <td width=646 colspan=2 valign=top style='width:484.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
        <td width=22 valign=top style='width:16.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>4.<o:p></o:p></span></p>
        </td>
        <td width=623 valign=top style='width:467.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><b><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>DENGAN
        DPCP INI DIBUAT DENGAN SEBENARNYA DIPERGUNAKAN SEBAGAIMMESTINYA<o:p></o:p></span></b></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
    </tr>
    </table>

    <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
    style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:
    0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>
    <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
      <td width=576 valign=top style='width:432.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
      <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;
      mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
      none'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:118.1pt'>
        <td width=483 valign=top style='width:362.05pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:118.1pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes;height:18.9pt'>
        <td width=483 valign=top style='width:362.05pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:18.9pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KETERANGAN<o:p></o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>DIISI
        DENGAN HURUF CETAK <o:p></o:p></span></p>
        </td>
      </tr>
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
      <td width=661 valign=top style='width:495.7pt;padding:0cm 5.4pt 0cm 5.4pt'>
      <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
      style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;
      mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
      none'>
      <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
        <td width=276 valign=top style='width:206.65pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>MENGETAHUI<o:p></o:p></span></p>
        </td>
        <td width=123 rowspan=4 valign=top style='width:91.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=236 valign=top style='width:177.35pt;padding:0cm 5.4pt 0cm 5.4pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>SEMARANG,
        <o:p></o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:1;height:4.55pt'>
        <td width=276 valign=top style='width:206.65pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:4.55pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>KASUBBAG
        UMUM DAN KEPEGAWAIAN<o:p></o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=236 valign=top style='width:177.35pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:4.55pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>PNS
        YANG BERSANGKUTAN<o:p></o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:2;height:74.0pt'>
        <td width=276 valign=top style='width:206.65pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:74.0pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=236 valign=top style='width:177.35pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:74.0pt'>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes;height:26.9pt'>
        
        <td width=276 valign=top style='width:206.65pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:26.9pt'>
        <p class=MsoNormal><u><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $kasubag->B_03 ?>, <?= $kasubag->B_03B ?>
        <o:p></o:p></span></u></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NIP.
        <?= $kasubag->B_02B ?><o:p></o:p></span></p>
        </td>
       
       
        <td width=236 valign=top style='width:177.35pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:26.9pt'>
        <p class=MsoNormal><u><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><?= $row->nama?><o:p></o:p></span></u></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'>NIP.
        <?= $row->B_02B?><o:p></o:p></span></p>
        <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
        </td>
      </tr>
      </table>
      <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
      </td>
    </tr>
    </table>

    <p class=MsoNormal><span style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

  </div>

</body>

</html>
<script>
		window.print();
	</script>