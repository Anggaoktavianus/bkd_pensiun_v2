<style>
  select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
 .dataTables_wrapper {
    font-family: tahoma;
    font-size: 12px;
    position: relative;
    clear: both;
    *zoom: 1;
    zoom: 1;

}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun BUP - SKPD</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Mutasi PNS Masuk</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			
		</div>
	<!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP-SKPD</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Bup/blast_skpd')?>" id="blast" method="post">
								<table  id="examples" class="table table-bordered table-striped main-table" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>No</th>
											<th>NIP</th>
											<th>Nama</th>
											<th style="width:25%">Pangkat/Jabatan</th>
											<th>SKPD</th>
											<th style="width:20%">Status </th>
											<th> <br><input id="checkAlls" type="checkbox" /></th>
											<th>Action</th>

										</tr>
									</thead> 
									<tbody> 
										<?php $no = 1;
											foreach ($row as $key => $data) {
										?>
								
										<tr>
											<input type="hidden" name="diusulkan_skpd" value="1">
											<input type="hidden" name="last_position" value="0">
											<!-- <input type="text" name="id_waktu_pns[]" value="3"> -->
											<td><?=$no++?></td>
											<td><?= $data->B_02B ?></td>
											<td><?= $data->nama ?></td>
											<td style="width:20%"><?=$data->F_PK ?> <?= $data->pangkat?> <?= $data->I_JB?></td>
											<td><?= $data->nmskpd?>  </td>
											<td>
												<?php
												$date_now = date("Y:m:d"); // this format is string comparable

												if ($date_now < date('Y:m:d', strtotime($data->due_date_skpd))) {
													echo '
														<button type="button" class="btn btn-outline-primary btn-block btn-xs" data-placement="top" data-toggle="tooltip" title="Batas Waktu Usulan Dari BKD" ><i class="fa fa-clock" ></i> '. date($data->due_date_skpd).'</button>
														';
												}elseif ($data->id_waktu_skpd == 0) {
													echo '<button type="button" class="btn btn-outline-success btn-block btn-xs" data-placement="top" data-toggle="tooltip" title="Batas Waktu Usulan Dari BKD" ><i class="fa fa-clock" ></i> Batas waktu tidak ada</button>';
												}else{
													echo '<button type="button" class="btn btn-outline-warning btn-block btn-xs" data-placement="top" data-toggle="tooltip" title="Batas Waktu Usulan Dari BKD" ><i class="fa fa-clock" ></i> Batas waktu terlewat</button>';
												};
												if($data->kelengkapan_data == 0) { 

													echo '
														<button type="button" class="btn btn-outline-danger btn-block btn-xs" data-placement="top"  data-toggle="tooltip" title="Kelengkapan persyaratan YBS" ><i class="fa fa-book" ></i> Data Belum lengkap</button>
														
													';
												} else { 
													echo'
													<button type="button" class="btn btn-outline-success btn-block btn-xs"><i class="fa fa-book"></i> Berkas lengkap</button>
													
													';
												}
												?>
											</td>

											<td><input type="checkbox" name="B_02B[]" value="<?= $data->B_02B ?>"<?php if($data->diusulkan_skpd == 1){echo'checked disabled';} ?>></td>
											
											<td>
												<form action="" method="post">
												<a href="<?= site_url('Bup/edit_bup_skpd/'. $data->B_02B)?>" class="btn btn-success btn-xs text-white">
												<i class="fas fa-eye "></i>
												</a>
												<input type="hidden" name="id" value="<?= $data->B_02B ?>">
												
												</button>
												</form> 
											</td>
										</tr> 
										<?php
											} ?>
									</tbody>
									<tfoot>
										<tr>
											<th>No</th>
											<th>NIP</th>
											<th>Nama</th>
											<th>Pangkat/Jabatan</th>
											<th>SKPD</th>
											<th>Status</th>
											<th></th>
											<th>Action</th>
										</tr>
									</tfoot>
									
								</table>
								<div class="card-body">
                                    <div class="margin">
                                        <div class="btn-group">
                                            <div class="form-group">
												<label>Kunci Nominatif (optional)</label><br>
												<select name="id_waktu_pns" class="form-control not-dark">
													<option value="">--Select--</option>
													<?php foreach ($pns as $key => $data) { ?>
													<option value="<?= $data->waktu_pns?>" ><?= $data->waktu_pns ?> Hari</option>
													<?php } ?>
												</select>
											</div>
                                        </div>
                                        <div class="btn-group">
                                            <div class="form-group" id="user">
											<label>Proses</label><br>
												<button type="submit"  class="btn btn-success btn-sm" onclick="submitResult(event)"><i class="fas fa-arrow-circle-right">&nbsp;</i>Kirim Usulan ke USER</button>
											</div>
                                               
                                        </div>
                                        <!-- <div class="btn-group">
                                        	<div class="form-group" id="bkd">
												<label>Proses</label><br>
												<button type="submit" id="bkd" class="btn btn-primary btn-sm" onclick="kembalibkd(event)"><i class="fas fa-arrow-circle-right">&nbsp;</i>Kirim Usulan ke BKD</button>
											</div>
                                        </div> -->
                                    </div>
                           		 </div>
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>


  <!-- /.content-wrapper -->
<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
// initialising dt table
$(document).ready(function() {

	$('#examples').DataTable({
		
      
		// Definition of filter to display
		
		filterDropDown: {
			columns: [
				{
					idx: 3
				},
				
			],
			scrollY: 200,
        	scrollX: true,
			bootstrap: true
		}
	} );
} );

</script>
<script>
 function submitResult(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Blast info BUP ke USER?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim BUP!'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("blast").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>

<script>
 function kembalibkd(e) {
    e.preventDefault();
    Swal.fire({
        title: 'kembalikan usulan BUP ke BKD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kembalikan BUP!'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("blast").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
