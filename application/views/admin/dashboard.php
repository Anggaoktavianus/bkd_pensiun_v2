<?php $no = 1;
    foreach ($row as $key ) {   
  ?>
  <?php } ?> 
  <?php
  $role_pensiun = $this->session->userdata('role_pensiun');
  ?>
  <?php if($role_pensiun == 1){ ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      
      <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Filter History</a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Progress</a>
                  </li> -->
                  <!-- <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Messages</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Settings</a>
                  </li> -->
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <div class="card-body row">
                      <div class="card-body">
                        <form method="get" action="<?php echo base_url("Dashboard/index/")?>">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Jenis Pengajuan</label>
                                <select class="form-control select2"  style="width: 100%;" >
                                <option value="">--Select--</option>
                                <option value="0" disabled>Anumerta(belum tersedia)</option>
                                <option value="1" disabled>APS(belum tersedia)</option>
                                <option value="2">BUP</option>
                                <option value="3" disabled>Janda Duda(belum tersedia)</option>
                                <option value="4" disabled>Keuzuran(belum tersedia)</option>
                                <option value="5" disabled>PDH TAPS(belum tersedia)</option>
                                <option value="6" disabled>Penyederhanaan Organisasi(belum tersedia)</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                              <label>NIP</label>
                                <input type="text" class="form-control" name="B_02B" placeholder="Input NIP YBS">
                              </div>
                              <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <!-- <div class="col-md-4">
                              <div class="form-group">
                                <label>Status BUP</label>
                                <select class="form-control select2"  style="width: 100%;">
                                <option value="">--Select--</option>
                                <option value="0">BKN</option>
                                <option value="1">Kanreg</option>
                                <option value="2">BKD</option>
                                <option value="3">SKPD</option>
                                <option value="4">PNS</option>
                                </select>
                              </div>
                            </div> -->
                            
                          </div>
                          <!-- /.row -->
                            <input type="submit" class="btn btn-primary" id="show" value="Cari">
                            <form method="get" action="<?php echo base_url("Dashboard/index")?>">
                            <input type="submit" class="btn btn-secondary" value="Reset">
                            </form>
                        </form>
                      </div>
                      <div class="container-fluid">
                        <!-- Timelime example  -->
                        <div class="row">
                          <div class="col-md-12">
                            <!-- The time line -->
                            <Table id="example"  class="table table-bordered table-striped" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>NIP Acc</th>
                                  <th>Jabatan</th>
                                  <th>NIP PNS</th>
                                  <th>Posisi </th>
                                  <th>Status </th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $no = 1;
                                      foreach ($filter as $data ) {   
                                    ?>
                                    
                                  <tr>
                                    <td><?=$no++?></td>
                                    <td><?= $data['nip_acc']?></td>
                                    <td><?= $data{'jabatan_acc'}?></td>
                                    <td><?= $data['id_pengajuan']?></td>
                                    <td><?php if ($data{'status_ajuan'} == 0) { echo '<span class="badge badge-pill badge-info">Dikirim dari BKD ke SKPD</span>';
                                      } elseif ($data{'status_ajuan'} == 1) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke PNS</span>';
                                      }elseif ($data{'status_ajuan'} == 2) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari PNS ke SKPD</span>';
                                      }elseif ($data{'status_ajuan'} == 3) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke BKD</span>';
                                      }elseif ($data{'status_ajuan'} == 4) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim Revisi dari SKPD ke PNS</span>';
                                      }elseif ($data{'status_ajuan'} == 5) {
                                        echo '<span class="badge badge-pill badge-info">ACC Oleh BKD Prov Jateng</span>';
                                      }else { echo'<span class="badge badge-pill badge-info">Dikirim dari BKD ke BKN</span>';}?>
                                    </td>
                                      <td><?php if ($data['status_acc'] == 0) { echo '<span class="badge badge-pill badge-info">Menunggu</span>';} elseif ($data['status_acc'] == 1) {
                                      echo '<span class="badge badge-pill badge-success">ACC</span>';
                                      }elseif ($data['status_acc'] == 5) {
                                        echo '<span class="badge badge-pill badge-success">ACC BKN</span>';
                                      }elseif ($data['status_acc'] == 6) {
                                        echo '<span class="badge badge-pill badge-warning">BTL BKN</span>';
                                      }else { echo'<span class="badge badge-pill badge-secondary">Revisi</span>';}?>
                                    </td>
                                  </tr>
                                  <?php } ?> 
                                  
                              </tbody>
                            </Table>
                          </div>
                          <!-- /.col -->
                        </div>
                      </div>
                      <!-- <div class="col-12 text-center d-flex align-items-center justify-content-center">
                        <div class="">
                          <img src="<?= base_url('assets/dist/img/jateng.png')?>" class="brand-image img-circle" style="opacity: 0.8" width="30%">
                          <h2>Purna<strong>Tugas</strong></h2>
                          <p class="lead mb-5">Badan Kepegawaian Daerah<br>
                            Provinsi Jawa Tengah
                          </p>
                        </div>
                        
                      </div> -->
                    </div>
                  </div>
                 
                </div>
              </div>
             
              <!-- /.card -->
            </div>
          </div>

    </section>
    <!-- /.content -->
  </div>
<?php } ?>

<?php if($role_pensiun == 2  ){ ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      
      <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Dashboard</a>
                  </li>
                 
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <div class="card-body row">
                      
                     
                      <!-- <div class="col-12 text-center d-flex align-items-center justify-content-center">
                        <div class="">
                          <img src="<?= base_url('assets/dist/img/jateng.png')?>" class="brand-image img-circle" style="opacity: 0.8" width="30%">
                          <h2>Purna<strong>Tugas</strong></h2>
                          <p class="lead mb-5">Badan Kepegawaian Daerah<br>
                            Provinsi Jawa Tengah
                          </p>
                        </div>
                        
                      </div> -->
                      <div class="card-body">
                        <form method="get" action="<?php echo base_url("Dashboard/index/")?>">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Jenis Pengajuan</label>
                                <select class="form-control select2"  style="width: 100%;" >
                                <option value="">--Select--</option>
                                <option value="0" disabled>Anumerta(belum tersedia)</option>
                                <option value="1" disabled>APS(belum tersedia)</option>
                                <option value="2">BUP</option>
                                <option value="3" disabled>Janda Duda(belum tersedia)</option>
                                <option value="4" disabled>Keuzuran(belum tersedia)</option>
                                <option value="5" disabled>PDH TAPS(belum tersedia)</option>
                                <option value="6" disabled>Penyederhanaan Organisasi(belum tersedia)</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                              <label>NIP</label>
                                <input type="text" class="form-control" name="B_02B" placeholder="Input NIP YBS">
                              </div>
                              <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <!-- <div class="col-md-4">
                              <div class="form-group">
                                <label>Status BUP</label>
                                <select class="form-control select2"  style="width: 100%;">
                                <option value="">--Select--</option>
                                <option value="0">BKN</option>
                                <option value="1">Kanreg</option>
                                <option value="2">BKD</option>
                                <option value="3">SKPD</option>
                                <option value="4">PNS</option>
                                </select>
                              </div>
                            </div> -->
                            
                          </div>
                          <!-- /.row -->
                            <input type="submit" class="btn btn-primary" id="show" value="Cari">
                            <form method="get" action="<?php echo base_url("Dashboard/index")?>">
                            <input type="submit" class="btn btn-secondary" value="Reset">
                            </form>
                        </form>
                      </div>
                      <div class="container-fluid">
                        <!-- Timelime example  -->
                        <div class="row">
                          <div class="col-md-12">
                            <!-- The time line -->
                            <Table id="example"  class="table table-bordered table-striped" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>NIP Acc</th>
                                  <th>Jabatan</th>
                                  <th>NIP PNS</th>
                                  <th>Posisi </th>
                                  <th>Status </th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $no = 1;
                                      foreach ($filters as $data ) {   
                                    ?>
                                    
                                  <tr>
                                    <td><?=$no++?></td>
                                    <td><?= $data['nip_acc']?></td>
                                    <td><?= $data{'jabatan_acc'}?></td>
                                    <td><?= $data['id_pengajuan']?></td>
                                    <td><?php if ($data{'status_ajuan'} == 0) { echo '<span class="badge badge-pill badge-info">Dikirim dari BKD ke SKPD</span>';
                                      } elseif ($data{'status_ajuan'} == 1) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke PNS</span>';
                                      }elseif ($data{'status_ajuan'} == 2) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari PNS ke SKPD</span>';
                                      }elseif ($data{'status_ajuan'} == 3) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke BKD</span>';
                                      }elseif ($data{'status_ajuan'} == 4) {
                                        echo '<span class="badge badge-pill badge-info">Dikirim Revisi dari SKPD ke PNS</span>';
                                      }elseif ($data{'status_ajuan'} == 5) {
                                        echo '<span class="badge badge-pill badge-info">ACC Oleh BKD Prov Jateng</span>';
                                      }else { echo'<span class="badge badge-pill badge-info">Dikirim dari BKD ke BKN</span>';}?>
                                    </td>
                                      <td><?php if ($data['status_acc'] == 0) { echo '<span class="badge badge-pill badge-info">Menunggu</span>';} elseif ($data['status_acc'] == 1) {
                                      echo '<span class="badge badge-pill badge-success">ACC</span>';
                                      }elseif ($data['status_acc'] == 5) {
                                        echo '<span class="badge badge-pill badge-success">ACC BKN</span>';
                                      }elseif ($data['status_acc'] == 6) {
                                        echo '<span class="badge badge-pill badge-warning">BTL BKN</span>';
                                      }else { echo'<span class="badge badge-pill badge-secondary">Revisi</span>';}?>
                                    </td>
                                  </tr>
                                  <?php } ?> 
                                  
                              </tbody>
                            </Table>
                          </div>
                          <!-- /.col -->
                    </div>
                  </div>
                  
                </div>
              </div>
             
              <!-- /.card -->
            </div>
          </div>

    </section>
    <!-- /.content -->
  </div>
<?php } ?>
<?php if($role_pensiun == 3 ){ ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      
      <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Dashboard</a>
                  </li>
                 
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <div class="card-body row">
                      
                     
                      <div class="col-12 text-center d-flex align-items-center justify-content-center">
                        <div class="">
                          <img src="<?= base_url('assets/dist/img/jateng.png')?>" class="brand-image img-circle" style="opacity: 0.8" width="30%">
                          <h2>Purna<strong>Tugas</strong></h2>
                          <p class="lead mb-5">Badan Kepegawaian Daerah<br>
                            Provinsi Jawa Tengah
                          </p>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
             
              <!-- /.card -->
            </div>
          </div>

    </section>
    <!-- /.content -->
  </div>
<?php } ?>
  <script>
    // initialising dt table
    $(document).ready(function() {
        

        $('#example').DataTable({
            
            
            // Definition of filter to display
            
            
        } );
    } );

</script>
  <?php 
		$message = $this->session->flashdata('message');
		if (isset($message)) { ?>
		<script>
		
		$(function() {
		var Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 10000
		});
		Toast.fire({
			icon: 'success',
			text : '<?=$message?>',
		})
		
		
		});
		</script>
		<?php 	} ?>