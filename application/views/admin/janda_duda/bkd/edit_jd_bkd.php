<style>
  .main-table {
  /* border-collapse: collapse; */
  cursor: pointer;
  /* margin: 20px; */
  }
  tr.ui-sortable-handle > td:first-child{
      /* width: 50px;
      background-color: lightgreen; */
  }

  .multipleRows tr > td {
      border: none;
      padding-left: 0px;
  }
  td {
    cursor: pointer;
  }

  .editor{
    display: none;
  }


</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>View Data Purna Tugas Janda/Duda </h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Purna Tugas Janda/Duda </li>
					</ol>
				</div>
			</div>
			
		</div>
	<!-- /.container-fluid -->
	</section>	<!-- /.content -->
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <?php

                  $nipPns = $row->B_02B;
                  $urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/'.$nipPns;


                ?>
                <!-- <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/img_avatar.png')?>" alt="User profile picture">
                </div> -->
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="<?= $urlFoto?>" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?= $row->nama ?></h3>

                <p class="text-muted text-center"><?= $row->B_02B ?></p>

               <div class="card-body">
                 <strong><i class="fas fa-calendar-alt mr-1"></i> Tanggal Lahir</strong>
                <p class="text-muted"><?= $row->tl ?></p>
                <hr>
                <strong><i class="fas fa-users mr-1"></i> Unit Kerja</strong>
                <p class="text-muted">
                  <?= $row->nmskpd?>
                </p>
                <hr>
                <strong><i class="fas fa-book-reader mr-1"></i> Pangkat</strong>
                <p class="text-muted"><?= $row->pangkat ?>, <?= $row->F_PK ?> </p>
                <hr>
                <strong><i class="fas fa-award mr-1"></i> Jabatan</strong>
                <p class="text-muted"><?= $row->I_JB ?> </p>
                <hr>
              </div>
                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link " href="" data-toggle="tab">Data Purna Tugas Janda Duda</a></li>
                  <!-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="tab-pane" id="settings">
                      <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" >
                        <div class="row">
                          <?php if ( $row->last_position ==2) { ?>
                          <div class="col-md-3">
                          <div class="form-group">
                            <label>Provinsi</label>
                             <select class="form-control not-dark" id="provinsi" disabled="true">
                               <option value="">--Select--</option>
                                <?php foreach ($provinsi as $key => $data) { ?>
                                <option value="" <?= $data->id == $row->provinsi_id ? "selected" : null ?>><?= $data->name ?></option>
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                          <!-- /.col -->
                          
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kab/Kota</label>
                            <select  class="form-control not-dark" id="kota" disabled="true">
                              <option value="">--Select--</option>
                              <?php foreach ($kabkota as $key => $data) { ?>
                              <option value="" <?= $data->id == $row->kabkota_id ? "selected" : null ?>><?= $data->name ?></option>
                              <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>        
                        <!-- /.col -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kecamatan</label>
                            <select  class="form-control not-dark" id="kecamatan" disabled="true">
                                <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                                <option value=""<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>        
                        <!-- /.col -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kelurahan </label>
                            <select  class="form-control not-dark" id="kelurahan" disabled="true">
												        <option value="">--Select--</option>
                                <?php foreach ($desa as $key => $data) { ?>
                                <option value=""<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>  
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Alamat Setelah Pensiun</label>
                            <textarea class="form-control" rows="3" placeholder="Masukkan Alamat Lengkap ..." disabled="true"><?= $row->alamat ?></textarea>
                          </div>
                            <!-- /.form-group -->
                          <!-- /.form-group -->
                        </div> 
                        <?php } else {?>
                          <div class="col-md-3">
                            <input type="hidden" name="B_02B" class="form-control"  value="<?= $row->B_02B ?>" >
                            <input type="hidden" name="skpd" class="form-control"  value="<?= $row->A_01 ?>" >
                            <input type="hidden" name="nip_lama" class="form-control"  value="<?= $row->B_02 ?>" >
                            <input type="hidden" name="gelar_dpn" class="form-control"  value="<?= $row->B_03A ?>" >
                            <input type="hidden" name="nama" class="form-control"  value="<?= $row->nama ?>" >
                            <input type="hidden" name="gelar_blkg" class="form-control"  value="<?= $row->B_03B ?>" >
                            <input type="hidden" name="t_lahir" class="form-control"  value="<?= $row->B_04 ?>" >
                            <input type="hidden" name="tl" class="form-control"  value="<?= $row->tl ?>" >
                            <div class="form-group">
                              <label>Provinsi</label>
                              <select name="provinsi_id" class="form-control not-dark" id="provinsi" required>
                                <option value="">--Select--</option>
                                  <?php foreach ($provinsi as $key => $data) { ?>
                                  <option value="<?= $data->id ?>" <?= $data->id == $row->provinsi_id ? "selected" : null ?>><?= $data->name ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                          </div>
                            <!-- /.col -->
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>Kab/Kota</label>
                              <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                                <option value="">--Select--</option>
                                <?php foreach ($kabkota as $key => $data) { ?>
                                <option value="<?= $data->id?>" <?= $data->id == $row->kabkota_id ? "selected" : null ?>><?= $data->name ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <!-- /.form-group -->
                          </div>        
                          <!-- /.col -->
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>Kecamatan</label>
                              <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
                                  <option value="">--Select--</option>
                                  <?php foreach ($keca as $key => $data) { ?>
                                  <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                            <!-- /.form-group -->
                          </div>        
                          <!-- /.col -->
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>Kelurahan</label>
                              <select name="kelurahan_id" class="form-control not-dark" id="kelurahan" >
                                  <option value="">--Select--</option>
                                  <?php foreach ($desa as $key => $data) { ?>
                                  <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                            <!-- /.form-group -->
                          </div>  
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Alamat Setelah Pensiun</label>
                              <textarea name="alamat" class="form-control" rows="3" placeholder="Masukkan Alamat Lengkap ..." onkeyup="this.value = this.value.toUpperCase();"><?= $row->alamat ?></textarea>
                            </div>
                              <!-- /.form-group -->
                            <!-- /.form-group -->
                          </div> 
                          <?php } ?> 
                          <!-- <div class="col-md-3">
                            <div class="form-group">
                              <label>RT</label>
                              <input type="text" name="rt" autocomplete="off" value="<?=$row->RT;?>" required placeholder="Masukkan RT" class="form-control" >
                            </div>
                          </div>   
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>RW</label>
                              <input type="text" name="rw" autocomplete="off" value="<?=$row->RW;?>" required placeholder="Masukkan RW" class="form-control" >
                            </div>
                          </div>        -->
                        </div>

                        <!-- Data Ahli Waris -->
                        <!-- Data Ahli Waris -->
                        <h3 class="card-title"><strong> Data Ahli Waris </strong></h3><br>
                        <h3 class="card-title"><small>A.Data Suami/Isteri</small> </h3><br>
                          <?php if ($row->last_position ==0 || $row->last_position ==1  ||  $row->last_position ==3  || $row->last_position ==4) { ?>
                                <a data-toggle="modal" data-target="" class="badge badge-secondary" data-popup="tooltip" data-placement="top" aria-disabled=""><i class="fas fa-plus">&nbsp;</i>Add Data</a>
                                <?php } else {?>
                                  <a data-toggle="modal" data-target="#modal-add-sutri" class="badge badge-primary" data-placement="top"><i class="fas fa-plus">&nbsp;</i>Add Data</a><br>
                          <?php } ?> 
                      
                        <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Tanggal Lahir</th>
                              <th>Tanggal Menikah</th>
                              <th>Umur</th>
                              <th>Keterangan</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody class="row_position">
                              <?php
                              $sl = '1';
                              foreach($sutri as $data): ?>
                            <tr id="<?= $data->ID ?>" >
                              <td><?= $data->KF_03?></td>
                              <td><?= $data->KF_04?></td>
                              <td><?= $data->KF_05?></td>
                              <td><?= $data->KF_06?></td>
                              <td><?= $data->umur?></td>
                              <td><?= $data->keterangan?></td>
                              <td><span style="background-color: #FFFF00"> <?= $data->ket?></span></td>
                              <td>

                                <?php if ($row->last_position ==1  || $row->last_position ==0 || $row->last_position ==3  || $row->last_position ==4) { ?>
                                <a data-toggle="modal"  class="btn btn-secondary btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data" aria-disabled><i class="fas fa-pencil-alt"></i></a>
                                <a data-toggle="modal"  class="btn btn-secondary btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data" aria-disabled><i class="fas fa-trash-alt"></i></a>
                                <?php } else {?>
                                  <div class="tooltip-demo">
                                      <a data-toggle="modal" data-target="#modal-edit-sutri<?=$data->ID;?>" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-pencil-alt"></i></a>
                                      <a href="<?php echo site_url('Bup/hapus_ahli_waris/'.$data->ID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$data->ID;?> ?');" class="btn btn-danger btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                                  </div>
                                <?php } ?> 
                                  
                                  
                              </td>
                            </tr>
                            
                            <?php
                              $sl++;
                              endforeach; ?>
                          </tbody>

                        </table>
                        <h3 class="card-title"><small>B.Data Anak</small> </h3><br>
                          <?php if ($row->last_position ==1  || $row->last_position ==0 || $row->last_position ==3  || $row->last_position ==4) { ?>
                                <a data-toggle="modal" data-target="" class="badge badge-secondary" data-popup="tooltip" data-placement="top" aria-disabled=""><i class="fas fa-plus">&nbsp;</i>Add Data</a>
                                <?php } else {?>
                                  <a data-toggle="modal" data-target="#modal-add" data-popup="tooltip" class="badge badge-primary" data-placement="top"><i class="fas fa-plus">&nbsp;</i>Add Data</a><br>
                          <?php } ?> 
                      
                        <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Tanggal Lahir</th>
                              <th>Umur</th>
                              <th>Nama Ayah/Ibu</th>
                              <th>Keterangan</th>
                              <th>Status Anak</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody class="row_positions">
                              
                            <?php
                              $sl = '1';
                              foreach($anak as $data): ?>
                          
                              <tr id="<?= $data->ID ?>" >
                                <td><?= $data->KF_03?></td>
                                <td><?= $data->KF_04?></td>
                                <td><?= $data->KF_05?></td>
                                <td><?= $data->umur?></td>
                                <td><?= $data->nama?></td>
                                <td><?= $data->keterangan?></td>
                                <td><span style="background-color: #87CEFA"> <?= $data->ket?></span></td>
                                <td>
                                  <?php if ($row->last_position ==1  || $row->last_position ==0 || $row->last_position ==3  || $row->last_position ==4) { ?>
                                    <a data-toggle="modal"  class="btn btn-secondary btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data" aria-disabled><i class="fas fa-pencil-alt"></i></a>
                                    <a data-toggle="modal"  class="btn btn-secondary btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data" aria-disabled><i class="fas fa-trash-alt"></i></a>
                                  <?php } else {?>
                                    <div class="tooltip-demo">
                                        <a data-toggle="modal" data-target="#modal-edit<?=$data->ID;?>" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="<?php echo site_url('Bup/hapus_ahli_waris/'.$data->ID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$data->ID;?> ?');" class="btn btn-danger btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                                      </div>
                                  <?php } ?> 
                                </td>
                              </tr>
                            <?php
                              $sl++;
                            endforeach; ?>
                          </tbody>
                        
                        </table>
                        <h3 class="card-title">Kelengkapan Dokumen Persyaratan di E-FILE</h3><br><br>
                        <p>* Wajib</p>
                        <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Jenis Berkas</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if ($row->last_position != 3) {?>
                               <?php foreach ($efilenya as $key => $val) :
                              
                              // echo json_encode($efilenya);
                              // die();
                              ?> 
                                  <tr>
                                      <td><?=$key+1;?></td>
                                      <td><?= $val['nama_jenis']?>
                                       </td>
                                      <td> 
                                        <?php if($val['efile'] != null) :?>
                                            <span class="badge badge-pill badge-info">Sudah Ada</span>
                                          <?php else :?>
                                            <!-- <button class="btn btn-warning btn-xs">Belum ada</button> -->
                                            <span class="badge badge-pill badge-danger">Belum Ada</span>
                                            </form>
                                          <?php endif ;?>
                                          &nbsp;
                                          <?php if($val['status_verif'] != null) :?>
                                            <span class="badge badge-pill badge-info">Terverifikasi</span>
                                          <?php else :?>
                                            <!-- <button class="btn btn-warning btn-xs">Belum ada</button> -->
                                            <span class="badge badge-pill badge-danger">Belum Terverifikasi</span>
                                            </form>
                                          <?php endif ;?>

                                        
                                        </td>
                                      <td>
                                       
                                          <?php if($val['efile'] != null) :
                                            // $namej = str_replace(' ', '_', $val['nama_jenis']);
                                            // $namek = str_replace('(', '', $namej);
                                            // $namel = str_replace(')', '', $namek);
                                            $gol = kodegol($row->F_PK);
                                            $namel = kodefile_yk($val['id_jenis']);
                                            $namez = kodefile_jkt($val['id_jenis']);
                                            $namek = str_replace('*', $gol, '');
                                            $name_f = $namel.'_'.$namek.$row->B_02B.'.pdf';
                                            $name_a = $namez.'_'.$row->B_02B.'.pdf';
                                            $trimmed = str_replace('http://','', $val['efile']) ; 
                                            // echo $$val['efile'];
                                            ?>
                                              <a href="<?=$val['efile']?>" target="_blank" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white"> <i class="fas fa-eye "></i></a>
                                              <a href="#" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white  dropdown-toggle dropdown-icon"  data-toggle="dropdown"> <i class="fas fa-download">Download</i></a>
                                                <div class="dropdown-menu" role="menu">
                                                  <a class="dropdown-item" href="<?=base_url('bup/download_file/'.$name_f.'/'.$trimmed)?>" target="_blank">BKN Yogya</a>
                                                  <a class="dropdown-item" href="<?=base_url('bup/download_file/'.$name_a.'/'.$trimmed)?>" target="_blank">BKN JKT</a>
                                                </div>
                                              <a href="<?=base_url('bup/upload_semar/'.$name_f.'/'.$trimmed.'/'.$row->B_02B)?>" target="_blank" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white"> <i class="fas fa-upload">Upload</i></a>
                                              <a href="<?=base_url('bup/verifikasi_efile/'.$val['id'].'/'.$row->B_02B)?>" rel="noopener noreferrer" class="btn btn-warning btn-xs text-white"> Verif</a> 
                                          <?php else :?>
                                              <span class="label label-warning"></span>
                                              <form action="" method="post">
                                              <a href="<?= site_url('Bup/cetak/'. $row->B_02B)?>" target="_blank" class="btn btn-danger btn-xs text-white">
                                              <?php if ($val['nama_jenis']=='DATA PENERIMA CALON PENSIUN ( DPCP )') {
                                              echo '<i class="fas fa-download "></i> ';
                                              }else {
                                                echo'<i class="fas fa-times-circle "></i>';
                                              } ?>
                                              </a>                                     
                                                <a href="http://efile.bkd.jatengprov.go.id/"  target="_blank" class="btn btn-success btn-xs text-white"><i class="fas fa-upload"></i></a>                                             
                                              <input type="hidden" name="B_02B" value="<?= $row->B_02B ?>">
                                            </form>
                                          <?php endif ;?>
                                          
                                      </td>
                                  </tr>
                                  <?php endforeach ;?>
                             
                           <?php } ?>
                           
                                   <tr>
                                    <td>1</td>
                                    <td>SK PENSIUN</td>
                                    <?php
                                    if ($row->status_bkn == 1) {
                                      echo '<td> <span class="badge badge-pill badge-success">Acc BKN</span></td>';
                                    }elseif ($row->status_bkn == 2) {
                                      echo '<td> <span class="badge badge-pill badge-warning "">BTL BKN</span></td>';
                                    }else {
                                      echo '<td> <span class="badge badge-pill badge-danger">Belum Ada</span></td>';
                                    }
                                    ?>
                                    
                                    <td>
                                      <?php
                                      if ($row->no_sk == null) {
                                        if ($row->status_bkn == null) {
                                          echo '<a class="btn btn-danger btn-xs text-white"><i class="fas fa-times-circle "></i></a>';
                                        }else{
                                          echo '<a data-toggle="modal" data-target="#modal-upload-sk" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-upload"></i></a>';
                                        }
                                      
                                      }else {
                                        echo ' <a href=" '. base_url('jd/cetak_sk/'.$row->B_02B).'" class="btn btn-success btn-circle btn-xs" target="_blank"><i class="fas fa-download"></i></a></td>';
                                      }
                                      ?>
                                      
                      
                                    
                                  </tr>
                          </tbody>
                          <!-- <a class="dropdown-item" href="<?=base_url('bup/download_bulk/'.$row->B_02B)?>" target="_blank">Download Bulk A</a> -->
                            <a href="#" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white  dropdown-toggle dropdown-icon"  data-toggle="dropdown"> <i class="fas fa-check">Download Bulk</i></a>
                              <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="<?=base_url('bup/download_bulk_v2/'.$row->B_02B)?>" target="_blank">BKN Yogya</a>
                                <a class="dropdown-item" href="<?=base_url('bup/download_bulk_jkt/'.$row->B_02B)?>" target="_blank">BKN JKT</a>
                              </div>
                        </table>
                        <!-- TABEL LAST POSITION -->
                        <h3 class="card-title">Posisi Ajuan</h3><br><br>
                        <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="width:5%">No</th>
                              <th>Posisi Ajuan</th>
                              <th>Status Ajuan</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td><?php if ($row->last_position == 0) { echo "PNS";} elseif ($row->last_position == 1) {
                                  echo$row->nmskpd;
                                }elseif ($row->last_position == 2) {
                                  echo "BKD";
                                }elseif ($row->last_position == 3) {
                                  echo "BKN";
                                }else { echo $row->nmskpd;}?></td>
                                <td><?php if ($row->status_ajuan == 0) { echo '<span class="badge badge-pill badge-info">Dikirim dari BKD ke SKPD</span>';
                                } elseif ($row->status_ajuan == 1) {
                                  echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke PNS</span>';
                                }elseif ($row->status_ajuan == 2) {
                                  echo '<span class="badge badge-pill badge-info">Dikirim dari PNS ke SKPD</span>';
                                }elseif ($row->status_ajuan == 3) {
                                  echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke BKD</span>';
                                }elseif ($row->status_ajuan == 4) {
                                  echo '<span class="badge badge-pill badge-info">Dikirim Revisi dari SKPD ke PNS</span>';
                                }elseif ($row->status_ajuan == 5) {
                                  echo '<span class="badge badge-pill badge-info">ACC Oleh BKD Prov Jateng</span>';
                                }else { echo'<span class="badge badge-pill badge-info">Dikirim dari BKD ke BKN</span>';}?>
                              </td>
                            </tr>
                          </tbody>
                          
                        </table>
                        <!-- END -->
                        <!-- TABEL HISTORI -->
                        <h3 class="card-title">Histori Ajuan</h3><br><br>
                        <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="width:5%">No</th>
                              <th>NIP Acc</th>
                              <th>Jabatan</th>
                              <th>Status Acc</th>
                            </tr>
                          </thead>
                        <tbody>
                              <?php $no = 1;
                                foreach ($status_acc as $data ) {   
                              ?>
                            <tr>
                              <td><?=$no++?></td>
                              <td><?= $data->nip_acc?></td>
                              <td><?= $data->jabatan_acc?></td>
                              <td><?php if ($data->status_acc == 0) { echo '<span class="badge badge-pill badge-info">Menunggu</span>';} elseif ($data->status_acc == 1) {
                                echo '<span class="badge badge-pill badge-success">ACC</span>';
                              }elseif ($data->status_acc == 5) {
                                echo '<span class="badge badge-pill badge-success">ACC BKN</span>';
                              }elseif ($data->status_acc == 6) {
                                echo '<span class="badge badge-pill badge-warning">BTL BKN</span>';
                              }else { echo'<span class="badge badge-pill badge-secondary">Revisi</span>';}?></td>
                            </tr>
                            <?php } ?> 
                          </tbody>
                        </table>
                        <!-- END -->
                        <!-- BUTTON BUP DIUSULKAN BKD -->
                        <div class="card-footer">
                          <?php if ($row->last_position == 2 && $row->status_ajuan == 3 ) { ?>
                            <div class="row">
                              <div class="col-md-2">
                                <div class="form-group">
                                <button type="submit" class="btn btn-success" ><i class="fas fa-save">&nbsp;</i>Simpan</button>
                                </div>
                              </div>
                              <!-- /.col -->
                             
                              <div class="col-md-4">
                                  <form action="<?= site_url('Jd/revisi_bkd'); ?>" id="kirimskpd" method="post" enctype="multipart/form-data">
                                    <input type="hidden" readonly value="<?=$row->B_02B;?>" name="B_02B" class="form-control" >
                                    <input type="hidden" name="last_position" value="1">
                                    <button type="submit" onclick="kirimskpd(event)"  class="btn btn-primary "><i class="fas fa-arrow-circle-down"> &nbsp;</i>Kembalikan ke SKPD</button>
                                  </form>
                                <!-- /.form-group -->
                              </div>
                              
                              <div class="col-md-4">
                                  <form action="<?= site_url('Jd/acc_bkd'); ?>" id="prosesacc" method="post" enctype="multipart/form-data">
                                    <input type="hidden" readonly value="<?=$row->B_02B;?>" name="B_02B" class="form-control" >
                                    <button type="submit" onclick="prosesacc(event)" class="btn btn-primary"><i class="fa fa-paper-plane">&nbsp;</i>Proses ACC</button>
                                  </form>
                                <!-- /.form-group -->
                              </div>
                            </div>
                            <?php } elseif ($row->status_ajuan == 5){
                              ?>
                            <div class="row">
                              <!-- /.col -->
                              <div class="col-md-4">
                                  <form action="<?= site_url('Jd/kirim_bkn/'); ?>" id="kirimbkn" method="post" enctype="multipart/form-data">
                                    <input type="hidden" readonly value="<?=$row->B_02B;?>" name="B_02B" class="form-control" >
                                    <button type="submit" onclick="submitResult(event)" class="btn btn-primary"><i class="fas fa-arrow-circle-down"> &nbsp;</i>Kirim ke BKN</button>
                                  </form>
                                <!-- /.form-group -->
                              </div>
                            </div>
                            <?php }
                            ?> 
                            <?php if ($row->last_position != 2  ) { ?>
                              <button type="submit" class="btn btn-danger" disabled><i class="fa fa-paper-plane" >&nbsp;</i>Data Sudah dikirim</button>
                              
                          <?php } ?> 
                        </div>
                        <!-- END BUP DIUSULKAN BKD -->
                      </form>
                    </div>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    
  <!-- modal keterangan -->
  <div class="modal fade" id="modal-upload-sk">
    <div class="modal-dialog">
      <!-- <form action="" method="post" enctype="multipart/form-data"> -->
        <!-- <form action="<?= site_url(''); ?>" method="post" enctype="multipart/form-data"> -->
         <form action="<?php echo base_url().'bup/upload_sk'?>" method="post" enctype="multipart/form-data">
        
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Form SK Pensiun</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" readonly value="<?=$row->B_02B;?>" name="id" class="form-control" >
                <label class='col-md-12'>Nomor SK</label>
                <div class='col-md-12'><input type="text" name="no_sk" autocomplete="off" value="" required placeholder="Input Nomer SK Pensiun" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Upload SK </label>
                <div class='col-md-12'><input type="file" name="sk" autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan Tempat Lahir" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'> Status </label>
                <select name="status_bkn" class="form-control"  required>
                  <option value="0">--Select--</option>
                  <option value="1">ACC BKN</option>
                  <option value="2">BTL BKN</option>
								</select>
              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary" value="berkas">Proses</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- end modal keterangan -->
    <!-- /.content -->
    <!-- Modal Add Anak -->
  <?php $no=0; foreach($anak as $data): $no++; ?>
    <div class="modal fade" id="modal-add">
      <div class="modal-dialog">
        <form action="<?= site_url('Bup/insert_anak'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Data Anak</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <div class='col-md-12'><input type="hidden" name="KF_02" autocomplete="off" value="2" required placeholder="Masukkan Modal" class="form-control" ></div>
                  <div class='col-md-12'><input type="hidden" name="KF_08" autocomplete="off" value="K" required placeholder="Masukkan Modal" class="form-control" ></div>
                  <div class='col-md-12'><input type="hidden" name="KF_01" autocomplete="off" value="<?= $data->KF_01?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              <div class="form-group">
                  <label class='col-md-12'>Nama Anak</label>
                  <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="" required placeholder="Masukkan Nama" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tempat Lahir</label>
                  <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan Tempat Lahir" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Lahir</label>
                  <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="KF_05" required class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Jenis Kelamin </label>
                  <select name="KF_10" class="form-control not-dark"  required>
                    <option value="">--Select--</option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Anak ke</label>
                  <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="KF_03" required placeholder="Masukkan Angka" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Anak ke.. dari istri ke.. </label>
                  <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="" required placeholder="Keterangan contoh : Anak ke 1 dari istri ke 1 " class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
                </div>
                
                <div class="form-group">
                  <label class='col-md-12'>Status</label>
                  <select name="status" class="form-control not-dark"  required>
                    <option value="">--Select--</option>
                    <?php foreach ($status_anak as $key => $data) { ?>
                    <option value="<?= $data->id ?>"<><?= $data->ket ?></option>
                    <?php } ?>
                  </select>
                </div>
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <?php endforeach; ?>
  <!-- /.modal -->
    <!-- end -->
    <!-- Modal Add Sutri -->
  <?php $no=0; foreach($sutri as $data): $no++; ?>
    <div class="modal fade" id="modal-add-sutri">
      <div class="modal-dialog">
        <form action="<?= site_url('Bup/insert_sutri'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Data Suami/Istri</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <div class='col-md-12'><input type="hidden" name="KF_02" autocomplete="off" value="1" required placeholder="Masukkan Modal" class="form-control" ></div>
                  <div class='col-md-12'><input type="hidden" name="KF_01" autocomplete="off" value="<?= $data->KF_01?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                <div class="form-group">
                  <label class='col-md-12'>Nama Suami/Istri</label>
                  <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="" required placeholder="Masukkan Nama" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tempat Lahir</label>
                  <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan Tempat Lahir" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Lahir</label>
                  <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="KF_05" required class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Menikah</label>
                  <div class='col-md-12'><input type="date" name="KF_06" autocomplete="off" value="KF_06" required class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Jenis Kelamin </label>
                  <select name="KF_10" class="form-control not-dark"  required>
                    <option value="">--Select--</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                
                <div class="form-group">
                  <label class='col-md-12'>Suami/Istri ke</label>
                  <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="KF_03" required placeholder="Masukkan Angka" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Suami/Istri ke...</label>
                  <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="" required placeholder="Keterangan contoh : Istri ke 1 " class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
                </div>
                
                <div class="form-group">
                  <label class='col-md-12'>Status</label>
                  <select name="status" class="form-control not-dark"  required>
                    <option value="">--Select--</option>
                    <?php foreach ($status_sutri as $key => $data) { ?>
                    <option value="<?= $data->id ?>"><?= $data->ket ?></option>
                    <?php } ?>
                  </select>
                </div>
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <?php endforeach; ?>
    <!-- END -->
  <!-- MODAL edit anak-->
  <?php $no=0; foreach($anak as $data): $no++; ?>
    <div class="modal fade" id="modal-edit<?=$data->ID;?>">
      <div class="modal-dialog">
        <form action="<?= site_url('Bup/edit_anak'); ?>" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Data Anak</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" readonly value="<?=$data->ID;?>" name="ID" class="form-control" >
                <div class="form-group">
                  <label class='col-md-12'>Nama Anak</label>
                  <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="<?=$data->KF_04;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tempat Lahir</label>
                  <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="<?=$data->KF_09;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Lahir</label>
                  <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="<?=$data->KF_05;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Jenis Kelamin </label>
                  <select name="KF_10" class="form-control not-dark"  required>
                    <option value="<?=$data->KF_10;?>"><?=$data->KF_10;?></option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Anak ke</label>
                  <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="<?= $data->KF_03?>" required placeholder="Masukkan Angka" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Anak ke.. dari istri ke..</label>
                  <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="<?= $data->keterangan?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Status</label>
                  <select name="status" class="form-control not-dark"  required>
                    <?php foreach ($status_anak as $key => $data) { ?>
                    <option value="<?= $data->id ?>" ><?= $data->ket ?></option>
                    <?php } ?>
                  </select>
                </div>
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
  <?php endforeach; ?>

  <!-- MODAL edit Suami/istri-->
  <?php $no=0; foreach($sutri as $data): $no++; ?>
    <div class="modal fade" id="modal-edit-sutri<?=$data->ID;?>"> 
      <div class="modal-dialog">
        <form action="<?php echo site_url('Bup/edit_sutri'); ?>" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Data Suami/Istri</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" readonly value="<?=$data->ID;?>" name="ID" class="form-control" >
              <div class="form-group">
                  <label class='col-md-12'>Nama Suami/Istri</label>
                  <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="<?=$data->KF_04;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tempat Lahir</label>
                  <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="<?=$data->KF_09;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Lahir</label>
                  <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="<?=$data->KF_05;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Tanggal Menikah</label>
                  <div class='col-md-12'><input type="date" name="KF_06" autocomplete="off" value="<?=$data->KF_06;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Jenis Kelamin </label>
                  <select name="KF_10" class="form-control not-dark"  required>
                    <option value="<?=$data->KF_10;?>"><?=$data->KF_10;?></option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Suami/Istri ke</label>
                  <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="<?=$data->KF_03;?>" required placeholder="Masukkan Angka" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Istri/Suami ke</label>
                  <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="<?= $data->keterangan?>" required placeholder="Masukkan Modal" class="form-control" ></div>
                </div>
                <div class="form-group">
                  <label class='col-md-12'>Status</label>
                  <select name="status" class="form-control not-dark"  required>
                    <?php foreach ($status_sutri as $key => $data) { ?>
                    <option value="<?= $data->id ?>"><?= $data->ket ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  <?php endforeach; ?>
</div>

<!-- draggable ortu-->
<script type="text/javascript">

    $(document).ready( function () {
      $('#MSG').slideUp(3500);
    });

    $( ".row_position" ).sortable({
      stop: function() {
			var selectedData = new Array();

              

            $('.row_position>tr').each(function() {

                selectedData.push($(this).attr("ID"));

            });

            updateOrder(selectedData);

        }

    });


    function updateOrder(data) {

        $.ajax({

            url:"<?php echo base_url('Bup/updatesort');?>",

            type:'post',

            data:{sortable:data},

            success:function(result){
            	window.location.reload();
             }

        })

    }

</script>

<!-- draggable anak-->
<script type="text/javascript">

    $(document).ready( function () {
      $('#MSG').slideUp(3500);
    });

    $( ".row_positions" ).sortable({
      stop: function() {
			var selectedData = new Array();

              

            $('.row_positions>tr').each(function() {

                selectedData.push($(this).attr("ID"));

            });

            updateOrder(selectedData);

        }

    });


    function updateOrder(data) {

        $.ajax({

            url:"<?php echo base_url('Bup/updatesort_anak');?>",

            type:'post',

            data:{anak:data},

            success:function(result){
            	window.location.reload();
             }

        })

    }

</script>

<!-- Load provinsi -->
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#provinsi").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kota").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_kota"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#provinsi").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kota").html(response.list_kota).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>

<!-- load kota -->
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("Bup/list_kecamatan"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kota").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#kecamatan").html(response.list_kecamatan).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>

<!-- load kecamatan -->
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#kelurahan").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("Bup/list_kelurahan"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kecamatan").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#kelurahan").html(response.list_kelurahan).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>

<!-- Edit inline -->
<script>
  function myFunction() {
    var txt;
    if (confirm("Kirim data ke SKPD? Setelah mengirim anda tidak dapat merubah data lagi")) {
      
    } 
  }
</script>

<script>
  $(document).ready(function() {
  $('.file-input').change(function() {
    $file = $(this).val();
    $file = $file.replace(/.*[\/\\]/, ''); //grab only the file name not the path
    $('.filename-container').append("<span  class='filename'>" + $file + "</span>").show();
  })

  })
</script>

<!-- KIRIM BKN -->
<script>
 function submitResult(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Kirim ke BKN?',
        text: "Setelah mengirim data ke BKN Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim !'
    }).then((result) => {
        if (result.isConfirmed) {
          // $.ajax({
          //   type: 'POST',
          //   url: '/Bup/kirim_bkn/'+<?= $row->B_02B?>,
            
          // });
            document.getElementById("kirimbkn").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<!-- END -->

<!-- PROSES ACC -->
<script>
 function prosesacc(e) {
    e.preventDefault();
    Swal.fire({
        title: 'ACC Ajuan?',
        text: "Setelah Acc data Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Acc!'
    }).then((result) => {
        if (result.isConfirmed) {
          // $.ajax({
          //   type: 'POST',
          //   url: '/Bup/acc_bkd/'+<?= $row->B_02B?>,
            
          // });
            document.getElementById("prosesacc").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<!-- END -->

<!-- KIRIM SKPD -->
<script>
 function kirimskpd(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Kembalikan ke SKPD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kembalikan !'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("kirimskpd").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<!-- END -->