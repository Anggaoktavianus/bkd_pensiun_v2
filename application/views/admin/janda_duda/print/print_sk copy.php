<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>BKD Pensiun</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { 
    size: legal landscape }
.indent {
    margin-left: 105px;
}
.indents {
    margin-left: 125px;
}
.indentx {
    margin-left: 130px;
}


 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Bookman Old Style";
	panose-1:2 5 6 4 5 5 5 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	text-autospace:none;
	font-size:11.0pt;
	font-family:"Bookman Old Style",serif;}
p.TableParagraph, li.TableParagraph, div.TableParagraph
	{mso-style-name:"Table Paragraph";
	margin:0cm;
	text-autospace:none;
	font-size:11.0pt;
	font-family:"Bookman Old Style",serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
@page WordSection1
	{size:595.0pt 842.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 0px;
   /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class=" landscape">
  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
    <div class="row">
      <div class="column" >
        <table border="1" width="100%">
          <tr>
            <th>a</th>
          </tr>
          <tr>
            <td>
              <p style="text-align:center; font-size:8pt"> <b>KEPUTUSAN GUBERNUR JAWA TENGAH <br> NOMOR : 882/9325/015/23300/AZ/12/21 <br>
                TENTANG PEMBERIAN KENAIKAN PANGKAT PENGABDIAN, PEMBERHENTIAN DAN <br> PEMBERIAN PENSIUN PEGAWAI NEGERI SIPIL YANG MENCAPAI BATAS USIA PENSIUN <br><br> DENGAN RAHMAT TUHAN YANG MAHA ESA <br> GUBERNUR JAWA TENGAH</b> 
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p style="text-align:left; font-size:10pt"> Menimbang </span>&emsp;&emsp; : <span style="text-align: justify">bahwa Pegawai Negeri Sipil yang namanya tercantum dalam keputusan ini telah mencapai batas usia </span><br><span class="indent">pensiun dan telah memenuhi syarat untuk diberikan kenaikan pangkat pengabdian dan diberhentikan </span> <span class="indent">  dengan hormat sebagai Pegawai Negeri Sipil dengan hak pensiun. </span> 
              </p>
              <p style="text-align:left; font-size:10pt"> Mengingat </span>&ensp;&emsp;&emsp; : 
              <span style="text-align: justify">1. &nbsp; Pasal 4 ayat (1) Undang-Undang Dasar Republik Indonesia Tahun 1945; </span><br>
              <span class="indent">2. &nbsp; Undang-Undang Nomor 11 Tahun 1969 tentang Pensiun Pegawai dan Pensiun Janda/Duda </span> <span class="indents">  Pegawai; </span> <br>
              <span class="indent">3. &nbsp; Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara; </span> <br>
              <span class="indent">4. &nbsp; Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji PNS jo Peraturan</span> <span class="indents"> Pemerintah Nomor 15 Tahun 2019; </span> <br>
              <span class="indent">5. &nbsp; Peraturan Pemerintah Nomor 18 Tahun 2019 tentang Penetapan Pensiun Pokok Pensiunan </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">6. &nbsp; Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil; </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">7. &nbsp; Peraturan Badan Kepegawaian Negara Nomor 2 Tahun 2018 tentang Pedoman Pemberian </span> <span class="indents"> Pertimbangan Teknis Pensiun Pegawai Negeri Sipil dan Pensiu Janda/Duda Pegawai Negeri Sipil.</span> <br>
              </p>
              <p style="text-align:left; font-size:10pt"> Memperhatikan </span>&ensp; : <span style="text-align: justify">Pertimbangan Teknis Kepala Badan Kepegawaian Negara/Kepala Kantor Regional Badan Kepegawaian </span><br>
              <span class="indent"> Negara Nomor PH-23300002684 Tanggal 16-11-2021.</span><br>
              </p>
              <p style="text-align:center; font-size:10pt">MEMUTUSKAN :</p>
              <p style="text-align:left; font-size:10pt"> Menetapkan </span>&emsp;&emsp; : 
              <span style="text-align: justify">(1) &nbsp; Memberikan kenaikan pangkat pengabdian kepada Pegawai Negeri Sipil yang namanya tersebut</span><br><span class="indentx"> dalam lajur 1 dari dan menjadi sebagaimana tersebut dalam lajur 6 dengan gaji pokok dari dan </span> <br><span class="indentx"> menjadi sebagaimana tersebut dalam lajur 8 Keputusan ini. </span> <br>
              <span class="indent">(2) &nbsp; Memberhentikan dengan hormat sebagai Pegawai Negeri Sipil yang namanya tersebut dalam lajur</span><br><span class="indentx"> 1 pada akhir bulan tersebut pada lajur 10 Keputusan ini, disertai ucapan terima kasih atas jasa- </span> <br><span class="indentx">jasa selama bekerja pada Pemerintah Republik Indonesia. </span> <br>
              <span class="indent">(3) &nbsp; Terhitung mulai tanggal tersebut dalam lajur 11, kepadanya diberikan pensiun pokok sebulan</span><br><span class="indentx"> sebesar tersebut dalam lajur 12 Keputusan ini. </span> <br><br>
              <span class="indentx"><b> A. &nbsp; PENERIMA PENSIUN</b></span>
              <div class=WordSection1>
                <!-- <p class=MsoNormal style='margin-left:0.65pt'><span lang=EN-US>&nbsp;</span></p> -->
                <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
                style='border-collapse:collapse;margin-left:100.75pt;margin-right:6.75pt'>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>1.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border:solid black 1.0pt;
                  border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>NAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border:solid black 1.0pt;
                  border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>JOKO<span style='letter-spacing:-.2pt'> </span>KUSMANTO,<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.2pt'>A.Md</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>2.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt;letter-spacing:-.25pt'>NIP</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt'>196404171985031009<span style='letter-spacing:-.05pt'>
                  </span>/<span style='letter-spacing:-.05pt'> </span><span style='letter-spacing:
                  -.1pt'>500075891</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>3.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>TANGGAL<span style='letter-spacing:-.2pt'> </span><span
                  style='letter-spacing:-.1pt'>LAHIR</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>17 APRIL<span style='letter-spacing:-.1pt'> </span><span
                  style='letter-spacing:-.2pt'>1964</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>4.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt;letter-spacing:-.1pt'>JABATAN</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PARAMEDIK<span style='letter-spacing:-.3pt'> </span>VETERINER<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.1pt'>PENYELIA</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>5.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>UNIT<span style='letter-spacing:-.15pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> TERAKHIR</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>DINAS<span style='letter-spacing:-.35pt'> </span>PETERNAKAN<span
                  style='letter-spacing:-.35pt'> </span>DAN<span style='letter-spacing:-.35pt'>
                  </span><span style='letter-spacing:-.1pt'>KESWAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-top:4.4pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>6.</span></p>
                  </td>
                  <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:8.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.1pt'>PANGKAT/GOL. RUANG</span></p>
                  </td>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>LAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PENATA<span style='letter-spacing:-.35pt'> </span>TINGKAT<span
                  style='letter-spacing:-.3pt'> </span>I/III/d/01-10-<span style='letter-spacing:
                  -.2pt'>2013</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.2pt'>BARU</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.1pt'>PEMBINA/IV/a/01-04-</span><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>2022</span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>7.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>MASA<span style='letter-spacing:-.1pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> GOLONGAN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>33 TAHUN<span style='letter-spacing:-.1pt'> </span>6<span
                  style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>8.</span></p>
                  </td>
                  <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:5.45pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt'>GAJI<span style='letter-spacing:-.25pt'> </span><span
                  style='letter-spacing:-.1pt'>POKOK</span></span></p>
                  </td>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.2pt'>LAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>4.797.000</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>BARU</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>5.000.000</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>9.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>MASA<span style='letter-spacing:-.1pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> PENSIUN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>38 TAHUN<span style='letter-spacing:-.1pt'> </span>7<span
                  style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>10</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>BERHENTI<span style='letter-spacing:-.1pt'> </span>AKHIR<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>APRIL<span style='letter-spacing:-.15pt'> </span><span
                  style='letter-spacing:-.2pt'>2022</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>11</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>PENSIUN<span style='letter-spacing:-.1pt'> </span><span
                  style='letter-spacing:-.25pt'>TMT</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>1<span style='letter-spacing:-.15pt'> </span>MEI
                  <span style='letter-spacing:-.2pt'>2022</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>12</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PENSIUN<span style='letter-spacing:-.1pt'> POKOK</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>3.750.000</span></span></p>
                  </td>
                </tr>
                </table>
                <p class=MsoNormal>&nbsp;</p>
                </div>
            </td>
            
          </tr>

        <!-- TABEL
          <tr>
            <td>
              <p style="text-align:left; font-size:10pt"> Mengingat </span>&ensp;&emsp;&emsp; : 
              <span style="text-align: justify">1. &nbsp; Pasal 4 ayat (1) Undang-Undang Dasar Republik Indonesia Tahun 1945; </span><br>
              <span class="indent">2. &nbsp; Undang-Undang Nomor 11 Tahun 1969 tentang Pensiun Pegawai dan Pensiun Janda/Duda </span> <span class="indents">  Pegawai; </span> <br>
              <span class="indent">3. &nbsp; Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara; </span> <br>
              <span class="indent">4. &nbsp; Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji PNS jo Peraturan</span> <span class="indents"> Pemerintah Nomor 15 Tahun 2019; </span> <br>
              <span class="indent">5. &nbsp; Peraturan Pemerintah Nomor 18 Tahun 2019 tentang Penetapan Pensiun Pokok Pensiunan </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">6. &nbsp; Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil; </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">7. &nbsp; Peraturan Badan Kepegawaian Negara Nomor 2 Tahun 2018 tentang Pedoman Pemberian </span> <span class="indents"> Pertimbangan Teknis Pensiun Pegawai Negeri Sipil dan Pensiu Janda/Duda Pegawai Negeri Sipil.</span> <br>
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p style="text-align:left; font-size:10pt"> Memperhatikan </span>&ensp; : <span style="text-align: justify">Pertimbangan Teknis Kepala Badan Kepegawaian Negara/Kepala Kantor Regional Badan Kepegawaian </span><br>
              <span class="indent"> Negara Nomor PH-23300002684 Tanggal 16-11-2021.</span><br>
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p style="text-align:center; font-size:10pt">MEMUTUSKAN :</p>
              <p style="text-align:left; font-size:10pt"> Menetapkan </span>&emsp;&emsp; : 
              <span style="text-align: justify">(1) &nbsp; Memberikan kenaikan pangkat pengabdian kepada Pegawai Negeri Sipil yang namanya tersebut</span><br><span class="indentx"> dalam lajur 1 dari dan menjadi sebagaimana tersebut dalam lajur 6 dengan gaji pokok dari dan </span> <br><span class="indentx"> menjadi sebagaimana tersebut dalam lajur 8 Keputusan ini. </span> <br>
              <span class="indent">(2) &nbsp; Memberhentikan dengan hormat sebagai Pegawai Negeri Sipil yang namanya tersebut dalam lajur</span><br><span class="indentx"> 1 pada akhir bulan tersebut pada lajur 10 Keputusan ini, disertai ucapan terima kasih atas jasa- </span> <br><span class="indentx">jasa selama bekerja pada Pemerintah Republik Indonesia. </span> <br>
              <span class="indent">(3) &nbsp; Terhitung mulai tanggal tersebut dalam lajur 11, kepadanya diberikan pensiun pokok sebulan</span><br><span class="indentx"> sebesar tersebut dalam lajur 12 Keputusan ini. </span> <br><br>
              <span class="indentx"><b> A. &nbsp; PENERIMA PENSIUN</b></span>
              <table border="1" class="indentx" width="70%">
                <tr>
                  <th>a</th>
                  <th>b</th>
                  <th></th>
                </tr>
                <tr>
                  <td>
                    <p style="text-align:left; font-size:10pt">1</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">NAMA</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">NAMA PEGAWAI</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="text-align:left; font-size:10pt">2</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">NIP</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">196404171985031009 / 500075891</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="text-align:left; font-size:10pt">3</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">TANGGAL LAHIR</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">19 MAret 1967</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="text-align:left; font-size:10pt">4</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">JABATAN</p>
                  </td>
                  <td>
                    <p style="text-align:left; font-size:10pt">PARAMEDIK VETERINER PENYELIA</p>
                  </td>
                </tr>
              </table>
              </p>
            </td>
          </tr>
        Table -->
        </table>
      </div>
      <div class="column" >
        <table border="1" width="100%">
          <tr>
            <th style="text-align:left; font-size:10pt">&ensp;&emsp;&emsp;&ensp;&emsp;&emsp;&ensp;&emsp;&emsp;B. KELUARGA PENERIMA PENSIUN</th>
          </tr>
          <tr>
            <td>
              <p style="text-align:left; font-size:8pt" class="indent"> 1. ISTERI/SUAMI<br>
                
              </p>
              <div class=WordSection1>

                <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
                style='border-collapse:collapse;border:none; margin-top:10px; margin-left:110px;' >
               <tr>
                <td width=31 valign=top style='width:23.15pt;border:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>NO</span></p>
                </td>
                <td width=186 valign=top style='width:139.4pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>NAMA</span></p>
                </td>
                <td width=139 valign=top style='width:104.5pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>TGL LAHIR</span></p>
                </td>
                <td width=119 valign=top style='width:89.25pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>TGL PERKAWINAN</span></p>
                </td>
                <td width=91 valign=top style='width:68.45pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>KET</span></p>
                </td>
              </tr>
              <tr style='height:18.8pt'>
                <td width=31 valign=top style='width:23.15pt;border:solid windowtext 1.0pt;
                border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>1.</span></p>
                </td>
                <td width=186 valign=top style='width:139.4pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>SUKESIH</span></p>
                </td>
                <td width=139 valign=top style='width:104.5pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>09-02-1968</span></p>
                </td>
                <td width=119 valign=top style='width:89.25pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>26-07-2001</span></p>
                </td>
                <td width=91 valign=top style='width:68.45pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <!-- <p class=MsoNormal><span style='font-size:8.0pt'>&nbsp;</span></p> -->
                </td>
              </tr>
                </table>

                <!-- <p class=MsoNormal>&nbsp;</p> -->
                <p style="text-align:left; font-size:8pt" class="indent"> 2. ANAK<br>
                
              </p>
              <div class=WordSection1>

                <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
                style='border-collapse:collapse;border:none; margin-top:10px; margin-left:110px;' >
               <tr>
                <td width=31 valign=top style='width:23.15pt;border:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>NO</span></p>
                </td>
                <td width=186 valign=top style='width:139.4pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>NAMA</span></p>
                </td>
                <td width=139 valign=top style='width:104.5pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>TGL LAHIR</span></p>
                </td>
                <td width=119 valign=top style='width:89.25pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>TGL PERKAWINAN</span></p>
                </td>
                <td width=91 valign=top style='width:68.45pt;border:solid windowtext 1.0pt;
                border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:8.0pt'>KET</span></p>
                </td>
              </tr>
              <tr style='height:18.8pt'>
                <td width=31 valign=top style='width:23.15pt;border:solid windowtext 1.0pt;
                border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>1.</span></p>
                </td>
                <td width=186 valign=top style='width:139.4pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>SUKESIH</span></p>
                </td>
                <td width=139 valign=top style='width:104.5pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>09-02-1968</span></p>
                </td>
                <td width=119 valign=top style='width:89.25pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>26-07-2001</span></p>
                </td>
                <td width=91 valign=top style='width:68.45pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                padding:0cm 5.4pt 0cm 5.4pt;height:18.8pt'>
                <p class=MsoNormal><span style='font-size:8.0pt'>&nbsp;</span></p>
                </td>
              </tr>
                </table>

                <p class=MsoNormal>&nbsp;</p>

                </div>
                </div>
                
            </td>
            
          </tr>
         
          <tr>
            <td>
              <p style="text-align:left; font-size:10pt"> Menimbang </span>&emsp;&emsp; : <span style="text-align: justify">bahwa Pegawai Negeri Sipil yang namanya tercantum dalam keputusan ini telah mencapai batas usia </span><br><span class="indent">pensiun dan telah memenuhi syarat untuk diberikan kenaikan pangkat pengabdian dan diberhentikan </span> <span class="indent">  dengan hormat sebagai Pegawai Negeri Sipil dengan hak pensiun. </span> 
              </p>
              <p style="text-align:left; font-size:10pt"> Mengingat </span>&ensp;&emsp;&emsp; : 
              <span style="text-align: justify">1. &nbsp; Pasal 4 ayat (1) Undang-Undang Dasar Republik Indonesia Tahun 1945; </span><br>
              <span class="indent">2. &nbsp; Undang-Undang Nomor 11 Tahun 1969 tentang Pensiun Pegawai dan Pensiun Janda/Duda </span> <span class="indents">  Pegawai; </span> <br>
              <span class="indent">3. &nbsp; Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara; </span> <br>
              <span class="indent">4. &nbsp; Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji PNS jo Peraturan</span> <span class="indents"> Pemerintah Nomor 15 Tahun 2019; </span> <br>
              <span class="indent">5. &nbsp; Peraturan Pemerintah Nomor 18 Tahun 2019 tentang Penetapan Pensiun Pokok Pensiunan </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">6. &nbsp; Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil; </span> <span class="indents"> PNS dan Janda/Dudanya;</span> <br>
              <span class="indent">7. &nbsp; Peraturan Badan Kepegawaian Negara Nomor 2 Tahun 2018 tentang Pedoman Pemberian </span> <span class="indents"> Pertimbangan Teknis Pensiun Pegawai Negeri Sipil dan Pensiu Janda/Duda Pegawai Negeri Sipil.</span> <br>
              </p>
              <p style="text-align:left; font-size:10pt"> Memperhatikan </span>&ensp; : <span style="text-align: justify">Pertimbangan Teknis Kepala Badan Kepegawaian Negara/Kepala Kantor Regional Badan Kepegawaian </span><br>
              <span class="indent"> Negara Nomor PH-23300002684 Tanggal 16-11-2021.</span><br>
              </p>
              <p style="text-align:center; font-size:10pt">MEMUTUSKAN :</p>
              <p style="text-align:left; font-size:10pt"> Menetapkan </span>&emsp;&emsp; : 
              <span style="text-align: justify">(1) &nbsp; Memberikan kenaikan pangkat pengabdian kepada Pegawai Negeri Sipil yang namanya tersebut</span><br><span class="indentx"> dalam lajur 1 dari dan menjadi sebagaimana tersebut dalam lajur 6 dengan gaji pokok dari dan </span> <br><span class="indentx"> menjadi sebagaimana tersebut dalam lajur 8 Keputusan ini. </span> <br>
              <span class="indent">(2) &nbsp; Memberhentikan dengan hormat sebagai Pegawai Negeri Sipil yang namanya tersebut dalam lajur</span><br><span class="indentx"> 1 pada akhir bulan tersebut pada lajur 10 Keputusan ini, disertai ucapan terima kasih atas jasa- </span> <br><span class="indentx">jasa selama bekerja pada Pemerintah Republik Indonesia. </span> <br>
              <span class="indent">(3) &nbsp; Terhitung mulai tanggal tersebut dalam lajur 11, kepadanya diberikan pensiun pokok sebulan</span><br><span class="indentx"> sebesar tersebut dalam lajur 12 Keputusan ini. </span> <br><br>
              <span class="indentx"><b> A. &nbsp; PENERIMA PENSIUN</b></span>
              <div class=WordSection1>
                <!-- <p class=MsoNormal style='margin-left:0.65pt'><span lang=EN-US>&nbsp;</span></p> -->
                <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
                style='border-collapse:collapse;margin-left:100.75pt;margin-right:6.75pt'>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>1.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border:solid black 1.0pt;
                  border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>NAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border:solid black 1.0pt;
                  border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>JOKO<span style='letter-spacing:-.2pt'> </span>KUSMANTO,<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.2pt'>A.Md</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>2.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt;letter-spacing:-.25pt'>NIP</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt'>196404171985031009<span style='letter-spacing:-.05pt'>
                  </span>/<span style='letter-spacing:-.05pt'> </span><span style='letter-spacing:
                  -.1pt'>500075891</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>3.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>TANGGAL<span style='letter-spacing:-.2pt'> </span><span
                  style='letter-spacing:-.1pt'>LAHIR</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>17 APRIL<span style='letter-spacing:-.1pt'> </span><span
                  style='letter-spacing:-.2pt'>1964</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>4.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.4pt'><span lang=IN
                  style='font-size:7.0pt;letter-spacing:-.1pt'>JABATAN</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PARAMEDIK<span style='letter-spacing:-.3pt'> </span>VETERINER<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.1pt'>PENYELIA</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.15pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>5.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>UNIT<span style='letter-spacing:-.15pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> TERAKHIR</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.15pt'>
                  <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>DINAS<span style='letter-spacing:-.35pt'> </span>PETERNAKAN<span
                  style='letter-spacing:-.35pt'> </span>DAN<span style='letter-spacing:-.35pt'>
                  </span><span style='letter-spacing:-.1pt'>KESWAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.3pt'>
                  <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-top:4.4pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>6.</span></p>
                  </td>
                  <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:8.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.1pt'>PANGKAT/GOL. RUANG</span></p>
                  </td>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.3pt'>
                  <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>LAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.3pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PENATA<span style='letter-spacing:-.35pt'> </span>TINGKAT<span
                  style='letter-spacing:-.3pt'> </span>I/III/d/01-10-<span style='letter-spacing:
                  -.2pt'>2013</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.2pt'>BARU</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.1pt'>PEMBINA/IV/a/01-04-</span><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>2022</span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>7.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>MASA<span style='letter-spacing:-.1pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> GOLONGAN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>33 TAHUN<span style='letter-spacing:-.1pt'> </span>6<span
                  style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>8.</span></p>
                  </td>
                  <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;margin-bottom:
                  0cm;margin-left:5.45pt;margin-bottom:.0001pt;line-height:normal'><span
                  lang=IN style='font-size:7.0pt'>GAJI<span style='letter-spacing:-.25pt'> </span><span
                  style='letter-spacing:-.1pt'>POKOK</span></span></p>
                  </td>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.2pt'>LAMA</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>4.797.000</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
                  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 0cm 0cm 0cm;
                  height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.2pt'>BARU</span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>5.000.000</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>9.</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>MASA<span style='letter-spacing:-.1pt'> </span>KERJA<span
                  style='letter-spacing:-.1pt'> PENSIUN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>38 TAHUN<span style='letter-spacing:-.1pt'> </span>7<span
                  style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>10</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>BERHENTI<span style='letter-spacing:-.1pt'> </span>AKHIR<span
                  style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>APRIL<span style='letter-spacing:-.15pt'> </span><span
                  style='letter-spacing:-.2pt'>2022</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.2pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right;line-height:7.2pt'><span lang=IN style='font-size:7.0pt;letter-spacing:
                  -.25pt'>11</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>PENSIUN<span style='letter-spacing:-.1pt'> </span><span
                  style='letter-spacing:-.25pt'>TMT</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.2pt'>
                  <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt'><span
                  lang=IN style='font-size:7.0pt'>1<span style='letter-spacing:-.15pt'> </span>MEI
                  <span style='letter-spacing:-.2pt'>2022</span></span></p>
                  </td>
                </tr>
                <tr style='height:8.25pt'>
                  <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
                  border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
                  right'><span lang=IN style='font-size:7.0pt;letter-spacing:-.25pt'>12</span></p>
                  </td>
                  <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
                  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>PENSIUN<span style='letter-spacing:-.1pt'> POKOK</span></span></p>
                  </td>
                  <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
                  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                  padding:0cm 0cm 0cm 0cm;height:8.25pt'>
                  <p class=TableParagraph style='margin-left:5.45pt'><span lang=IN
                  style='font-size:7.0pt'>Rp<span style='letter-spacing:-.05pt'> </span><span
                  style='letter-spacing:-.1pt'>3.750.000</span></span></p>
                  </td>
                </tr>
                </table>
                <p class=MsoNormal>&nbsp;</p>
                </div>
            </td>
            
          </tr>

        
        </table>
      </div>
    </div>
    <!-- Write HTML just like a web page -->
    
    
  </section>

</body>

</html>
 