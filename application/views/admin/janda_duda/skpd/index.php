<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Purna Tugas Janda Duda</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Purna Tugas Janda Duda </li>
					</ol>
				</div>
			</div>
			
			
		</div>
	    <!-- /.container-fluid -->
	</section>
	<section class="content">
	<div class="container-fluid">
		<!-- SELECT2 EXAMPLE -->
		<div class="card card-default">
		<!-- <div class="card-header">
			<h3 class="card-title">Proses Purna Tugas Janda Duda  <?php echo date_indo('2017-10-5');?></h3>
			
			<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse">
			<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove">
			<i class="fas fa-times"></i>
			</button>
			</div>
		</div> -->
		<!-- /.card-header -->
		<!-- <div class="card-body">
			<form method="get" action="<?php echo base_url("Jd/")?>">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Tahun Pensiun</label>
							<select class="form-control" name="thpensiun" id="thpensiun" style="width: 100%;">
								<option value="">--Select--</option>
								<?php foreach ($tahun as $key => $th) { ?>
									<option value="<?= $th->thpensiun?>" ><?= $th->thpensiun ?></option>
								<?php } ?>
							</select>
							<input type="hidden" name="A_01" class="form-control not-dark" id="w_skpd" value="<?= $this->session->userdata('A_01')?>">
						</div>
						
					</div>
					<div class="col-md-4">
						<label>TMT Pensiun</label>
							<select class="form-control select2" name="tmtpensiun"  id="tmtpensiun" style="width: 100%;">
							<option value="">--Select--</option>
							</select>
						
					</div>
					
				</div>
				
				<input type="submit" class="btn btn-primary" id="show" value="Proses">
				<form method="get" action="<?php echo base_url("Jd")?>">
				<input type="submit" class="btn btn-secondary" value="Reset">
				</form>
			</form>
		</div> -->
		
		</div>
	</div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.card-body -->
    <!-- /.content -->
	<!-- Main content -->
	<section class="content" > 
		<div class="container-fluid"  >
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							
								<h3 class="card-title">Data Tabel Purna Tugas Janda Duda</h3><br>
								<a id="kirim" class="btn btn-primary btn-xs"><i class="fas fa-plus"></i>Tambah Ajuan</a><br><br>
								
									<form action="<?= site_url('Jd/ajuan_bkd')?>" id="blast" method="post" enctype="multipart/form-data"> 
										<div class="form-group">
											<label>NIP</label>
											<input type="text" name="title[]" class="form-control" id="nip" placeholder="Masukkan NIP YBS" >
											</div>
										<div class="form-group">
											<label>Nama</label>
											<input type="text" name="B_03[]" class="form-control" placeholder="Nama YBS"  readonly>
										</div>
                                        <div class="form-group">
											<label>Aksi</label>
											<select class="form-control selectsch_items" name="cashbill" id="action" required>
                                                <option value="">--Option--</option>
                                                <option value="red">Simpan Draft</option>
                                                <option value="green">Kirim BKD</option>
                                                <option value="blue">Kirim PNS/YBS</option>
                                            </select>
										</div>
                                        <div class="form-group" id="nosk">
											<label>Nomer SK</label>
											<input type="text" name="nomer_surat" class="form-control" placeholder="Nomer Surat " >
										</div>
										<div class="form-group">
											<input type="hidden" name="A_01[]" class="form-control" placeholder="Nama YBS"  readonly>
                                            
										</div>
											<input type="hidden" class="form-control" name="diusulkan_skpd[]" value="1">
                                            <input type="hidden" class="form-control" name="diusulkan_bkd[]" value="1">
											<input type="hidden" class="form-control" name="created_at[]" >
											<input type="hidden" class="form-control" name="B_02[]" value="">
											<input type="hidden" class="form-control" name="nama[]" value="">
											<input type="hidden" name="last_position[]" value="2">
											<!-- <input type="hidden" name="A_01[]" value="<?= $this->session->userdata('A_01')?>"> -->
											<input type="hidden" name="status_ajuan[]" value="3">
											<input type="hidden" name="jenis_ajuan[]" value="2">

										<button type="submit" id="sendbkd" class="btn btn-success btn-sm form-control green box" onclick="blastbkd(event)"><i class="fas fa-arrow-circle-right">&nbsp;</i>Kirim BKD &ensp;&ensp;</button>
										
									</form><br>
									<form action="<?= site_url('Jd/save_draft')?>" id="draftsave" method="post" enctype="multipart/form-data"> 
									
										<input type="hidden" class="form-control" name="diusulkan_bkd[]" value="1">
										<input type="hidden" class="form-control" name="created_at[]">
										<input type="hidden" name="nipns[]" class="form-control"  placeholder="Masukkan NIP YBS"  readonly>
										<input type="hidden" class="form-control" name="B_02[]" value="<?= $data['B_02B'] ?>">
										<input type="hidden" class="form-control" name="namapns[]" value="">
										<input type="hidden" name="last_position[]" value="2">
										<input type="hidden" name="nmskpd[]" value="<?= $this->session->userdata('A_01')?>">
										<input type="hidden" name="status_ajuan[]" value="3">
										<input type="hidden" name="jenis_ajuan[]" value="2">
										<button  type="submit" id="save" class="btn btn-primary btn-sm form-control red box" onclick="savedraft(event)"><i class="fas fa-save"> &nbsp;</i>Simpan Draft</button>
										<!-- <a type="submit" class="btn btn-primary btn-xs" ><i class="fas fa-save">&nbsp;</i>Simpan Draft</a> -->
										<!-- <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save">&nbsp;</i>Simpan Draft</button> -->
									</form><br>
                                    <form action="<?= site_url('Jd/blastpns')?>" id="sendybs" method="post" enctype="multipart/form-data"> 
									
										<input type="hidden" class="form-control" name="diusulkan_bkd[]" value="1">
                                        <input type="hidden" class="form-control" name="diusulkan_skpd[]" value="1">
										<input type="hidden" class="form-control" name="created_at[]">
										<input type="hidden" name="nipns[]" class="form-control"  placeholder="Masukkan NIP YBS"  readonly>
										<input type="hidden" class="form-control" name="B_02[]" value="<?= $data['B_02B'] ?>">
										<input type="hidden" class="form-control" name="namapns[]" value="">
										<input type="hidden" name="last_position[]" value="0">
										<input type="hidden" name="nmskpd[]" value="<?= $this->session->userdata('A_01')?>">
										<input type="hidden" name="status_ajuan[]" value="1">
										<input type="hidden" name="jenis_ajuan[]" value="2">
										<button  type="submit" id="save" class="btn btn-primary btn-sm form-control blue box" onclick="pnssend(event)"><i class="fas fa-save"> &nbsp;</i>Kirim PNS/YBS</button>
										<!-- <a type="submit" class="btn btn-primary btn-xs" ><i class="fas fa-save">&nbsp;</i>Simpan Draft</a> -->
										<!-- <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save">&nbsp;</i>Simpan Draft</button> -->
									</form>
								
							<!-- <a data-toggle="modal" data-target="#modal-add" data-popup="tooltip" class="btn btn-primary btn-xs" data-placement="top"><i class="fas fa-plus">&nbsp;</i>Tambah Ajuan</a><br> -->
						</div>
						<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Jd/blast_skpd')?>" id="kirimbkd" method="post">
							<?php 
							// $id_waktu_skpd = $this->uri->segment('2');
							$id_waktu_skpd = $_GET['id_waktu_skpd'];
							// echo json_encode($id_waktu_skpd);
							?>
							
							<table  id="example" class="table table-bordered table-striped main-table responsive" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
							 			<th>Nama</th>
										<th>Jabatan</th>
										<th>Gol & Ruang</th>
										<th>Tanggal Lahir</th>
										<th>SKPD</th>
										<th>Status</th>
										<!-- <th>Waktu</th> -->
										<th><input id="checkAlls" type="checkbox" /></th>
                                        <th>Action</th>
									</tr>
								</thead> 
								<tbody >
									
									    <?php $no = 1;
										foreach ($row as $data ) {
											
												
									?>
									<tr>
										
										<input type="hidden" name="diusulkan_skpd[]" value="1">
										<input type="hidden" name="created_at[]" >
										<input type="hidden" name="A_01[]" value="<?= $data['A_01']?>">
										<td><?=$no++?> </td>
										<td><input type="hidden" name="B_02[]" value="<?= $data['B_02B'] ?>"><?= $data['B_02B'] ?></td>
										<td><input type="hidden" name="B_03[]" value="<?= $data['B_03'] ?>"><?= $data['B_03'] ?></td>
										<td><?= $data['I_JB'];?></td>
										<td><?= $data['pangkat']?> & ruang : (<?= $data['gol']?>)</td>
										<td><?= $data['B_05'] ?></td>
										<td><?= $data['skpd']?></td>
										<!-- <td><input type="hidden" name="tmt[]" value="<?= $data['tmtpensiun']?>"><?= $data['tmtpensiun']?></td>
										<td><input type="hidden" name="id_waktu_skpd[]" value="<?php if ($id_waktu_skpd > 0) { echo $data['wskpd'];} else { echo"0";}?> ">
										<?php if ($id_waktu_skpd > 0) { echo $data['wskpd'].' Hari';} else { echo"Tidak dikunci";}?>
										</td> -->
										<input type="hidden" name="due_date_skpd[]" value="<?=$data['due_date_skpd']?>">
                                       <td>
                                            <?php
                                            if($data['kelengkapan_data'] == 0) { 

													echo '
														<button type="button" class="btn btn-outline-danger btn-block btn-xs" data-placement="top"  data-toggle="tooltip" title="Kelengkapan persyaratan YBS" ><i class="fa fa-book" ></i> Data Belum lengkap</button>
														
													';
												} else { 
													echo'
													<button type="button" class="btn btn-outline-success btn-block btn-xs"><i class="fa fa-book"></i> Berkas lengkap</button>
													
													';
												}
												?>
                                       </td> 
										<td><input type="checkbox" name="B_02B[]" value="<?= $data['B_02B'] ?>"<?php if($data['last_position'] == 2 ){echo'checked disabled';} ?>></td>
										<td>
											<form action="" method="post">
											<a href="<?= site_url('Jd/edit_jd_skpd/'. $data['B_02B'])?>" class="btn btn-success btn-xs text-white">
											<i class="fas fa-eye "></i>
											</a>
											<input type="hidden" name="id" value="<?= $data['B_02B'] ?>">
											
											</button>
											</form> 
										</td>
									</tr>
									<?php } 
                    						?> 
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
							 			<th>Nama</th>
										<th>Jabatan</th>
										<th>Gol & Ruang</th>
										<th>Tanggal Lahir</th>
										<th>SKPD</th>
                                        <th>Status</th>
										<!-- <th>TMT</th>
										<th>Waktu</th> -->
										<th></th>
                                        <th>Action</th>
									</tr>	
								</tfoot>
							</table>
							<!-- < href="" class="btn btn-success btn-sm" type="submit"><i class="fas fa-arrow-circle-right">&nbsp;</i>Blast Info ke SKPD</> -->
							 <div class="row">
								<!-- /.col -->
								<div class="col-md-6">
									<div class="form-group">
										<label><small><strong> Kirim Nominatif</strong></small></label><br>
										 <button type="submit" class="btn btn-success btn-sm" onclick="submitResult(event)"><i class="fas fa-arrow-circle-right">&nbsp;</i>Kirim Ajuan ke BKD</button>
									</div>
									<!-- /.form-group -->
								</div>
								</div>
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
    
</div>

<script>
    $(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>

<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>
<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
    // initialising dt table
    $(document).ready(function() {
        

        $('#example').DataTable({
            
            
            // Definition of filter to display
            
            
        } );
    } );

</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#w_skpd").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#waktu_skpd").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_waktu_skpd"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        A_01: $("#w_skpd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#waktu_skpd").html(response.l_waktu_skpd).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#w_skpd").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#thpensiun").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_tahun"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        A_01: $("#w_skpd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#thpensiun").html(response.l_tahun).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();
            $("#thpensiun").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#tmtpensiun").hide(); // Sembunyikan dulu combobox kota nya
				// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Jd/list_tmt"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
					 	A_01: $("#w_skpd").val(),
                        thpensiun: $("#thpensiun").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya
                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#tmtpensiun").html(response.l_tmt).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>


<script>
 function submitResult(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Kirim info ke BKD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim !'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("kirimbkd").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>
<script>
 function savedraft(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Simpan sebagai draft?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Simpan !'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("draftsave").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>

<script>
 function pnssend(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Kirim Ke YBS?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, kirim !'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("sendybs").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>

<script>
 function blastbkd(e) {
    e.preventDefault();
    Swal.fire({
        title: 'Kirim info ke BKD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim !'
    }).then((result) => {
        if (result.isConfirmed) {
          
            document.getElementById("blast").submit();
        }
    })
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if (isset($message)) { ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php 	} ?>
</script>

<link rel="stylesheet" href="<?php echo base_url().'assets/dist/js/bootstrap.js'?>">
<script src="<?php echo base_url().'assets/dist/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/dist/js/jquery-ui.js'?>" type="text/javascript"></script>
<!-- <script type="text/javascript">
		$(document).ready(function(){
			 $('#nip').on('input',function(){
                
                var B_02B=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('jd/get_nip')?>",
                    dataType : "JSON",
                    data : {B_02B: B_02B},
                    cache:false,
                    success: function(data){
                        $.each(data,function(B_02B, B_03){
                            $('[name="B_03"]').val(data.B_03);
                           
                            
                        });
                        
                    }
                });
                return false;
           });

		});
	</script> -->

<script type="text/javascript">
	$(document).ready(function(){

		$('#nip').autocomplete({
			source: "<?php echo site_url('Jd/get_autocomplete');?>",
	
			select: function (event, ui) {
				$('[name="title"]').val(ui.item.label); 
				$('[name="B_03[]"]').val(ui.item.B_03); 
				$('[name="B_02[]"]').val(ui.item.B_02); 
				$('[name="A_01[]"]').val(ui.item.A_01); 
				$('[name="nipns[]"]').val(ui.item.label); 
				$('[name="namapns[]"]').val(ui.item.B_03); 
				$('[name="nmskpd[]"]').val(ui.item.A_01); 
			}
		});

	});
</script>

<script>
$(document).ready(function(){
	$("#blast").hide();
	$("#draftsave").hide();
  $("#kirim").click(function(){
	$("#draftsave").toggle();
    $("#blast").toggle();
  });
});
</script>
<script>
const el = document.getElementById('action');

const box = document.getElementById('nosk');

el.addEventListener('change', function handleChange(event) {
  if (event.target.value === 'green') {
    box.style.display = 'block';
  } else {
    box.style.display = 'none';
  }
});
</script>
