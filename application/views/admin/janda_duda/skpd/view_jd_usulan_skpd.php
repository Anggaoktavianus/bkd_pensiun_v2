<style>
  select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
 
</style>
 <?php
    $bkd = "kirim_bkd";
    $pns = "kirim_pns";
    // $datat = $bkd.''.$pns;
    // echo $datat;
    ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun Janda/Duda - SKPD </h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Calon Pensiun Janda/Duda</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			
		</div>
	<!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Pensiun Janda/Duda</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body" id="box">
							<form action="<?= site_url('Bup/kirim_usulan_skpd')?>" id="kirim_usulan" method="post">
                                <table  id="examples" class="table table-bordered table-striped responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Pangkat/Jabatan</th>
                                            <th>SKPD</th>
                                            <th>Posisi Ajuan</th>
                                            <th>No Surat Pengantar</th>
                                            <!-- <th>Usulan <input id="checkAlls" type="checkbox" /></th> -->
                                            <th>BTL <input id="checkAlls" type="checkbox" /></th>
                                            <th>Action</th>

                                        </tr>
                                    </thead> 
                                    <tbody> 
                                        <?php $no = 1;
                                            foreach ($row as $data ) {
                                        ?>
                                
                                        <tr>
                                            
                                            <input type="hidden" name="last_position" value="2">
                                            <td><?=$no++?></td>
                                            <td><?= $data['B_02B'] ?></td>
                                            <td><?= $data['nama'] ?></td>
                                            <td><?= $data['F_PK']?> <?= $data['pangkat']?> <?= $data['I_JB']?></td>
                                            <td><?= $data['nmskpd']?></td>
                                            <td><?php if($data['last_position'] == 1){echo'SKPD';} elseif($data['last_position']==2){ echo"BKD"; } elseif($data['last_position']==3){ echo"BKN"; } elseif($data['last_position']==4){ echo"SKPD"; }else {echo"PNS";} ?></td>
                                            <td> <?= $data['nomer_surat']?></td>
                                            <!-- <td><input type="checkbox" name="B_02B[]" id="<?= $data['B_02B'] ?>" onchange="billingFunction()" value="<?= $data['B_02B'] ?>"<?php if($data['last_position'] == 2 || $data['last_position'] == 3 || $data['last_position'] == 0){echo'checked disabled';} ?>></td> -->
                                             <td><input type="checkbox" name="B_02B[]" id="<?= $data['B_02B'] ?>" onchange="billingFunction()" value="<?= $data['B_02B'] ?>"<?php if($data['last_position'] == 1 || $data['last_position'] == 0 || $data['last_position'] == 4){echo'checked disabled';} ?>></td>
                                            <td>
                                                <form action="" method="post">
                                                <a href="<?= site_url('Jd/edit_jd_skpd/'. $data['B_02B'])?>" class="btn btn-success btn-xs text-white">
                                                <i class="fas fa-eye "></i>
                                                </a>
                                                <input type="hidden" name="id" value="<?= $data['B_02B'] ?>">
                                                
                                                </button>
                                                
                                                </form>
                                                
                                            </td>
                                        </tr> 
                                        <?php
                                        } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Pangkat/Jabatan</th>
                                            <th>SKPD</th>
                                            <th>Posisi Ajuan</th>
                                            <th>No Surat Pengantar</th>
                                             <!-- <th>Usulan</th> -->
                                            <th>BTL</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    
                                </table>
                                <div class="card-body">
                                    <div class="margin">
                                        <div class="btn-group">
                                           
                                            <!-- <div class="form-group">
                                                <label><small><strong>Input Nomer Surat Pengantar </strong> </small></label><br>
                                                <?php
                                                    if ($bkd == null) {
                                                       echo '<input type="text" id="no_surat"  name="nomer_surat" class="form-control no_surat" placeholder="Input Nomer Surat Pengantar" >';
                                                    }else{
                                                        echo '<input type="text" id="no_surat"  name="nomer_surat" class="form-control no_surat" placeholder="Input Nomer Surat Pengantar" required>';
                                                    }
                                                ?>
                                                
                                            </div> -->
                                        </div>
                                        <div class="btn-group">
                                            <!-- <div class="form-group">
                                                <label><small><strong> Action</strong></small></label><br>
                                                <button type="submit" id="" name="<?=$bkd?>"  value="<?=$bkd?>" class="btn btn-success"><i class="fas fa-arrow-circle-up">&nbsp;</i>Kirim Ke BKD</button>
                                            </div> -->
                                        </div>
                                        <div class="btn-group">
                                        
                                        <div class="form-group">
                                            <form action="<?= site_url('Jd/revisi_skpd'); ?>" id="kirim_pns" method="post" enctype="multipart/form-data">
                                            <label><small><strong> Action</strong></small></label><br>
                                            <button type="submit"  name="<?=$pns?>" value="<?=$pns?>" onclick="submitResult(event)" class="btn btn-info"><i class="fas fa-arrow-circle-right">&nbsp;</i>Batal Kirim</button>
                                            
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                           		 </div>
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>

  <!-- /.content-wrapper -->
<script>
        $("#checkAlls").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script type="text/javascript">
  function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox' ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }
</script>
<script>
    // initialising dt table
    $(document).ready(function() {

        $('#examples').DataTable({
            
        
            // Definition of filter to display
            
            filterDropDown: {
                columns: [
                    {
                        idx: 3
                    },
                    {
                        idx: 5
                    }
                ],
                bootstrap: true,
                // responsive: true
            }
        } );
    } );

</script>

<!-- KIRIM USULAN KE BKD/PNS -->
<script>
$(document).ready(function(){
  $("#kirimbkd").click(function(event){
  event.preventDefault()

   
        Swal.fire({
        title: 'Kirim Usulan ke BKD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Kirim'
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById("kirim_usulan").submit("");
        }
    })
  });
});

 function submitResult(e) {
    e.preventDefault();
   
        Swal.fire({
        title: 'Batal Kirim Usulan ke BKD?',
        text: "Setelah dikirim Admin tidak dapat merubah data lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Batal Kirim'
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById("kirim_usulan").submit("");
        }
    })
    
    
  }
  <?php 
    $message = $this->session->flashdata('pesan');
    $info = $this->session->flashdata('info');
      if($info == 'success'){  ?>

      $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'success',
        })
      });
  <?php } elseif ($info == 'danger') { ?> 
    $(function() {
        Swal.fire({
            title: '<?=$info?>',
            text: "<?=$message?>",
            icon: 'error',
        })
      });
<?php } ?>
</script>
<!-- END -->



<script>
    function billingFunction() {
       
        if (document.getElementById('<?= $row['B_02B'] ?>').checked) {
                document.getElementById("no_surat").value = document.getElementById('<?= $row['id_no_surat_pengantar_skpd']?>').value;
        }
        else {
        document.getElementById("no_surat").value = "";
        }

        }
</script>

