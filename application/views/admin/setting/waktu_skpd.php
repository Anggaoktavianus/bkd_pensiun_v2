<style>
  .main-table {
  /* border-collapse: collapse; */
  cursor: pointer;
  /* margin: 20px; */
}
tr.ui-sortable-handle > td:first-child{
    /* width: 50px; */
    /* background-color: lightgreen; */
}

.multipleRows tr > td {
    border: none;
    padding-left: 0px;
}
td {
	cursor: pointer;
}

.editor{
	display: none;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Setting Waktu BKD</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Waktu BKD </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>	<!-- /.content -->
  <!-- Main content -->
    <section class="content" > 
		<div class="container-fluid"  >
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Waktu BKD</h3><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div><br>
						<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<form action="<?= site_url('Bup/blast_bkd')?>" method="post">
							<table  id="example" class="table table-bordered table-striped main-table" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>SKPD</th>
							 			<th>Waktu SKPD</th>
										<th>Waktu USER</th>
										<th>Action</th>
									</tr>
								</thead> 
								<tbody >
									 <?php $no = 1;
										foreach ($setting as $key => $data ) {
											?>
									<tr>
										
										<td><?=$no++?></td>
										<td><?= $data->nskpd?></td>
										<td><?=$data->waktu_skpd?> /Hari</td>
										<td><?=$data->waktu_pns?> /Hari</td>
										<td>
											<div class="tooltip-demo">
												<a data-toggle="modal" data-target="#modal-edit<?=$data->id;?>" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-pencil-alt"></i></a>
												<!-- <a href="<?php echo site_url('Bup/hapus_ahli_waris/'); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data ');" class="btn btn-danger btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a> -->
											</div>
										</td>
										
										
									</tr>
                    							<?php }?> 
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>SKPD</th>
							 			<th>Waktu SKPD</th>
										<th>Waktu USER</th>
										<th>Action</th>
									</tr>	
								</tfoot>
							</table>
							<!-- < href="" class="btn btn-success btn-sm" type="submit"><i class="fas fa-arrow-circle-right">&nbsp;</i>Blast Info ke SKPD</> -->
							 
							</form>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
    <!-- /.content -->
    <!-- Modal Add Anak -->
    
</div>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
      <form action="<?= site_url('Bup/insert_setting_bkd'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Data Waktu SKPD</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label class='col-md-12'>Waktu SKPD</label>
                <div class='col-md-12'><input type="number" name="waktu_skpd" min="0" max="7" id='myInput' onclick='handleMyInput(event)' autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan jumlah hari" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Waktu User</label>
                <div class='col-md-12'><input type="number" name="waktu_pns" min="0" max="7" id='myInput' onclick='handleMyInput(event)' autocomplete="off" value="waktu_pns" placeholder="Masukkan jumlah hari" required class="form-control" ></div>
              </div>
          </div>
          
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<?php $no=0; foreach($row as $data): $no++; ?>
  <div class="modal fade" id="modal-edit<?=$data->id;?>">
    <div class="modal-dialog">
      <form action="<?= site_url('Bup/edit_setting_bkd'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Waktu SKPD</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <input type="hidden" readonly value="<?=$data->id;?>" name="id" class="form-control" >
              <div class="form-group">
                <label class='col-md-12'>Waktu SKPD</label>
                <div class='col-md-12'><input type="number" name="waktu_skpd" min="0" max="7" id='myInput' onclick='handleMyInput(event)' autocomplete="off" value="<?=$data->waktu_skpd;?>" required placeholder="Masukkan Modal" class="form-control" readonly ></div>
              </div>
	      <div class="form-group">
                <label class='col-md-12'>Waktu User</label>
                <div class='col-md-12'><input type="number" name="waktu_pns" min="0" max="7" id='myInput' onclick='handleMyInput(event)' autocomplete="off" value="<?=$data->waktu_pns;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
          </div>        
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <?php endforeach; ?>

<script>
// initialising dt table
$(document).ready(function() {

	$('#example').DataTable({
		
      
		// Definition of filter to display
		
		filterDropDown: {
			
			bootstrap: true
		}
	} );
} );
</script>
<script>
	function handleMyInput(event){
  const { value, max } = event.target
  if(value >= max) alert('Maksimal 7 Hari');
}
</script>




