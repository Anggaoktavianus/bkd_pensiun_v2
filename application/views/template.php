<?php
    $role_pensiun = $this->session->userdata('role_pensiun');

    // echo  $role_pensiun;
    // echo  $this->session->userdata('kolok');
    // die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BKD Pensiun</title>

  <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/dist/img/jateng.png')?>">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.2.4/css/fixedColumns.dataTables.min.css">
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="<?= base_url('assets/plugins/jqvmap/jqvmap.min.css')?>"> -->
  <!-- datatables -->
  
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
  <link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
  <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css')?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css')?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css')?>">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/jquery-ui.css'?>">
  <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
  <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.min.js')?>"></script>
  <link rel="stylesheet" href="<?= base_url('assets/plugins/sweetalert2/sweetalert2.min.css')?>">
  
  <!-- <?= $map['js'];?> -->

  
  
</head>
<body class="hold-transition sidebar-blue sidebar-mini layout-fixed " >
  <div class="wrapper">
    <!-- Preloader -->
    <!-- <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__shake" src="<?= base_url('assets/dist/img/AdminLTELogo.png')?>" alt="AdminLTELogo" height="60" width="60">
    </div> -->
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?= base_url('dashboard')?>" class="nav-link">Home</a>
        </li>
      </ul>
      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a class="nav-link" data-widget="navbar-search" href="#" role="button">
            <i class="fas fa-search"></i>
          </a>
          <div class="navbar-search-block">
            <form class="form-inline">
              <div class="input-group input-group-sm">
                <!-- <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"> -->
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                  <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>
        <!-- Notif adminprov -->
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Notifications</span>
            <div class="dropdown-divider"></div>
            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#test1">Open Modal 1 </button> -->
            <div class="dropdown-divider"></div>
          </div>
        </li>
        <!-- Notif adminKab -->
        
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-user"></i>
            <span class="badge badge-warning navbar-badge"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Profile</span>
            <div class="dropdown-divider"></div>
            <a href="" class="dropdown-item">
              <i class="fas fa-user mr-2"></i> <?= $this->session->userdata('B_02B')?>
              <span class="float-right text-muted text-sm"></span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?=site_url('login/logout')?>" class="dropdown-item">
              <i class="fas fa-power-off mr-2"></i> Logout
              <span class="float-right text-muted text-sm"></span>
            </a>
            <div class="dropdown-divider"></div>
            
          </div>
        </li>
        
      </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="<?= base_url('dashboard')?>" class="brand-link">
        <img src="<?= base_url('assets/dist/img/jateng.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Pensiun</span>
      </a>

      <?php

        $nipPns = $this->session->userdata('B_02B');
        $urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/'.$nipPns;


      ?>
     
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex"> 
          <!-- <div class="image">
            <img src="http://simpeg.bkd.jatengprov.go.id/eps/showpic/%27.$nipPns" class="img-circle elevation-2" alt="User Image">
          </div> -->
          <div class="image">
            <img src="<?= $urlFoto ?>" class="img-circle elevation-2" alt="User Image">
            
          </div>
          <div class="info">
            
            <a href="" class="d-block" style=""> <p style="font-size:14px;"><?= $this->session->userdata('B_03')?> </p></a>
           
          </div>
        </div>
        <!-- SidebarSearch Form -->
        <div class="form-inline">
          <!-- <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div> -->
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="<?= site_url('dashboard')?>" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p >
                  Dashboard<?= $this->session->userdata('jenis_ajuan')?>
                  <i class="right fas fa-angle"></i>
                </p>
              </a>
            </li>
            
            <!-- <li class="nav-item">
              <a href="<?= site_url('user')?>" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  Management User
                </p>
              </a>
            </li> -->
          <?php if($role_pensiun == 1){ ?>

            <!-- BUP -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-book"></i>
              <p>
                BUP (BKD)
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('bup/index/?A_01=&id_waktu_skpd=0') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP By Query</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_bkd')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Diusulkan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_diajukan_skpd') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Diajukan SKPD</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_acc') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Acc</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    BUP BKN
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fas fa-circle"></i>
                      <p>
                        BUP BKN YOGYA
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bup_ajuan_bkn')?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP Diajukan BKN</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bkn_acc') ?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP Disetujui BKN</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bkn_tolak')?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP BTL</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fas fa-circle"></i>
                      <p>
                        BUP BKN JKT
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bup_ajuan_bkn_jkt')?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP Diajukan BKN</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bkn_acc_jkt') ?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP Disetujui BKN</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="<?= site_url('bup/bkn_tolak_jkt')?>" class="nav-link">
                          <i class="far fa-dot-circle nav-icon"></i>
                          <p>BUP BTL</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li> -->
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_by_sk')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Diajukan BKN</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('bup/bkn_acc') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Disetujui BKN</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('bup/bkn_tolak')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP BTL</p>
                </a>
              </li>
            </ul>
          </li>
            <!-- END BUP -->

            <!-- JANDA DUDA -->
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-book"></i>
              <p>
                JANDA/DUDA (BKD)
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <!-- <li class="nav-item">
                <a href="<?= site_url('jd/index/?A_01=&id_waktu_skpd=0') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D By Query</p>
                </a>
              </li> -->
             
              <li class="nav-item">
                <a href="<?= site_url('jd/jd_diajukan_skpd') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Diajukan SKPD</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('jd/jd_acc') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Acc</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('jd/jd_by_sk')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Diajukan BKN</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('jd/bkn_acc') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Disetujui BKN</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('jd/bkn_tolak')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D BTL</p>
                </a>
              </li>
            </ul>
          </li>
            <!-- END JANDA DUDA -->
          <?php } ?>

          <?php if($role_pensiun == 2){ ?>
            <!-- BUP -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-book"></i>
              <p>
                BUP (SKPD)
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
             
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_skpd')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Dari BKD</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="<?= site_url('bup/bup_diusulkan_skpd') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP Diusulkan</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- END BUP -->
          <!-- JANDA DUDA -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-book"></i>
              <p>
                Janda/Duda (SKPD)
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
             <li class="nav-item">
                <a href="<?= site_url('jd/index/') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Ajuan Pensiun</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="<?= site_url('Jd/jd_skpd')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Dari BKD</p>
                </a>
              </li> -->
               <li class="nav-item">
                <a href="<?= site_url('Jd/jd_diusulkan_skpd') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>J/D Diusulkan</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- END JANDA DUDA -->
          <?php } ?>

          <?php if($role_pensiun == 1 || $role_pensiun == 2 || $role_pensiun == 3){ ?>
           <!-- <li class="nav-item">
            <a href="<?= site_url('bup/bup_user/'.$this->session->userdata('B_02B'))?>" class="nav-link">
              <i class="nav-icon fas fas fa-user"></i>
              <p>
                
                USER PNS 
              
              </p>
            </a>
            
          </li> -->
          <li class="nav-item">
            
            <a href="<?= site_url('user/'.$this->session->userdata('B_02B'))?>" class="nav-link">
              <i class="nav-icon fas fas fa-book"></i>
              <p>
                USER PNS
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
            
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('bup/bup_user/'.$this->session->userdata('B_02B'))?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BUP</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="<?= site_url('bup/bup_user/'.$this->session->userdata('B_02B'))?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>USER</p>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="<?= site_url('jd/jd_user/'.$this->session->userdata('B_02B'))?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Janda/Duda</p>
                </a>
              </li>
             
            </ul> -->
          </li>
          <?php } ?>

          <?php if($role_pensiun == 1 || $role_pensiun == 2 ){ ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-cog"></i>
              <p>
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <?php if($role_pensiun == 1){ ?>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('bup/setting_bkd')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Setting Waktu</p>
                </a>
              </li>
            </ul>
            <?php } ?>
            <?php if($role_pensiun == 2){ ?>
              <?php $id = $this->session->userdata('A_01');?>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('bup/setting_skpd')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Setting Waktu</p>
                </a>
              </li>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
  </div>
<!-- jQuery -->
<script src="<?= base_url('assets/plugins/jquery/jquery.min.js')?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
<?php echo $contents ?>
<!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="">All Rights Reserved by Badan Kepegawaian Daerah Provinsi Jawa Tengah</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Version:</b>1.0 Beta
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/plugins/chart.js/Chart.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets/plugins/sparklines/sparkline.js')?>"></script>
<!-- JQVMap -->
<!-- <script src="<?= base_url('assets/plugins/jqvmap/jquery.vmap.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')?>"></script> -->
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets/plugins/jquery-knob/jquery.knob.min.js')?>"></script>
<!-- daterangepicker -->
<script src="<?= base_url('assets/plugins/moment/moment.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>
<!-- Summernote -->
<script src="<?= base_url('assets/plugins/summernote/summernote-bs4.min.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- datatables -->
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')?>"></script>

<script src="<?= base_url('assets/plugins/jszip/jszip.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/pdfmake.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/vfs_fonts.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.html5.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.print.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.colVis.min.js')?>"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/dist/js/adminlte.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/dist/js/demo.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?= base_url('assets/dist/js/pages/dashboard.js')?>"></script> -->
<script src="<?= base_url('assets/dist/js/filterDropDown.js')?>"></script>

	




<!-- Page specific script -->


<script>

  /** add active class and stay opened when selected */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.nav-sidebar a').filter(function() {
    return this.href == url;
}).addClass('active');

// for treeview
$('ul.nav-treeview a').filter(function() {
    return this.href == url;
}).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>
 


</body>
</html>

