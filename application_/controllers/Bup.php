<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bup extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()

		{
		parent::__construct();
		not_login();
		$this->load->helper(array('form', 'url','download'));
		$this->load->model('M_bup');
		$this->load->library('form_validation');
		$this->dbdefault = $this->load->database('default', TRUE);
		$this->dbeps = $this->load->database('eps', TRUE);
		$this->load->library('cetak_pdf');
		$this->load->library('upload');
		 $this->load->helper('apiclient');
		 $this->load->helper(array('form', 'url'));


		// Load zip library
		$this->load->library('zip');		
		
		}
	
	//END
	
	// INDEX BKD
	public function index()
		{
		// $skpd = $this->input->post('A_01');
		//  $row = $this->M_bup->get($id);
		
		//  $data = array(
		// 	'row' => $row,
			
		//  );
		// $this->template->load('template', 'admin/bup/index',$data); //dari tabel EPS

		$skpd=$this->input->get('A_01');
		// $status=$this->input->get('status');
		$data['skpd'] = $this->M_bup->get_filter_skpd();
		// $data['waktu_skpd'] = $this->M_bup->get_list_waktu_skpd();
		$data['row'] = $this->M_bup->get($skpd)->result_array();
		
		$this->template->load('template', 'admin/bup/index',$data); //dari tabel EPS
		}
		
	// END 

	// BUP DIUSULKAN BKD
	public function bup_bkd()
		{
		$row = $this->M_bup->get_usul();
		
		$data = array(
			'row' => $row,
		);
		$this->template->load('template', 'admin/bup/view_bup',$data); //dari tabel pensiun pngajuan
		}
	//END
	
	// BLAST BUP BKD KE SKPD
	public function blast_bkd()
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

			$id = $this->input->post('B_02B');
			$ids = $this->input->post('B_02B');
			$blast_bkd = $this->input->post('diusulkan_bkd');
			// $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
			// $B_03 = $this->input->post('B_03');
			$kunci = $this->input->post('kunci_nominatif');
			$A_01 = $this->input->post('A_01');
			$wskpd = $this->input->post('id_waktu_skpd');
			$tanggalSekarang = date('Y-m-d H:i:s');
			$tanggalDuedate = date("Y-m-d H:i:s", strtotime($tanggalSekarang.' + '.$wskpd.' Days'));
			echo "tanggal sekarang :".$tanggalSekarang."<br>";
			echo "tanggal overdue :".$tanggalDuedate;
			// $AK_TMT = $this->input->post('AK_TMT');
			if ($this->form_validation->run() == FALSE) {

			//  $row = $this->M_bup->get();
			//  $dinas = $this->M_bup->get_skpd();
			//  $data = array(
			// 	'row' => $row,
			// 	'dinas' => $dinas,
			//  );
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
			//  $this->template->load('template', 'admin/bup/index',$data);
			// redirect('bup');
			redirect_back();

			}else {
				
			// $this->M_bup->insert_history_acc();
			for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				'B_02B' => $id[$i],
				'diusulkan_bkd' => $blast_bkd[$i],
				'id_waktu_skpd' => $wskpd[$i],
				'due_date_skpd' => $tanggalDuedate[$i],
				'created_at' => date('Y:m:d H:i:s'),
				'A_01' => $A_01[$i],
				'last_position' => 1,
				'status_ajuan' => 0,
				// 'AK_TMT' => $AK_TMT[$i],
			);
			$this->db->insert('pensiun_pengajuan',$data);
			
			
			}
			

			// echo json_encode($sql);
			
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');

			redirect_back();
			//  return redirect('bup');
			}
		}
	//END
	
	// BLAST BUP SKPD KE USER
	public function blast_skpd()
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');
			$id = $this->input->post('B_02B');
			$blast_skpd = $this->input->post('diusulkan_skpd');
			$ids = $this->input->post('B_02B');
			$wskpd = $this->input->post('id_waktu_pns');
			if ($this->form_validation->run() == FALSE) {

		
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
			//  $this->template->load('template', 'admin/bup/index',$data);
			redirect('bup/bup_skpd');

			}elseif ($wskpd > 0) {

			for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				'id_waktu_pns' => $wskpd[$i],
				'diusulkan_skpd' => 1,
				'last_position' => 0,
				'status_ajuan ' => 1
				
			);
			$this->db->where('B_02B', $id[$i]); 
			$this->db->update('pensiun_pengajuan',$data);
				

			}
			for ($x=0; $x < sizeof($ids); $x++) 
			{ 
			$data = array(
				'id_pengajuan' => $ids[$x],
				'nip_acc' => $this->session->userdata('B_02B'),
				'status_acc'=>0,
				// 'jabatan_acc'=>,
				'created_at' => date('Y:m:d H:i:s'),
				
			);
			$this->db->insert('pensiun_pengajuan_acc',$data);
			
			
			}

			// echo json_encode($data);
			
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');

			return redirect('bup/bup_skpd');
			}else {

			for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				// 'B_02B' => $id[$i],
				'diusulkan_skpd' => 1,
				'last_position' => 0,
				
			);
			$this->db->where('B_02B', $id[$i]); 
			$this->db->update('pensiun_pengajuan',$data);
				

			}
			for ($x=0; $x < sizeof($ids); $x++) 
			{ 
			$data = array(
				'id_pengajuan' => $ids[$x],
				'nip_acc' => $this->session->userdata('B_02B'),
				'status_acc'=>0,
				// 'jabatan_acc'=>,
				'created_at' => date('Y:m:d H:i:s'),
				
			);
			$this->db->insert('pensiun_pengajuan_acc',$data);
			
			
			}

			// echo json_encode($data);
			
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');

			return redirect('bup/bup_skpd');
			}
		}
	//END
	// KIRIM USULAN DARI SKPD KE BKD
	public function kirim_usulan_skpd()
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

			$idx = $this->input->post('B_02B');
			

			if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
			//  $this->template->load('template', 'admin/bup/index',$data);
			redirect_back();

			}else {

			$this->M_bup->insert_surat_pengantar();

			$id_surat = $this->M_bup->get_id_surat();
			foreach ($id_surat as $data) {
			 $data->id ; // Tambahkan tag option ke variabel $lists
			}
			$no_surat_pengantar = $data->id;

			for ($i=0; $i < sizeof($idx); $i++) 
			{ 
			$data = array(
				'B_02B' =>$idx[$i],
				'id_no_surat_pengantar_skpd' => $no_surat_pengantar,
				'last_position' => 2,
				'status_ajuan' => 3,
				
			);
			$this->db->where('B_02B', $idx[$i]); 
			$this->db->update('pensiun_pengajuan',$data);

				

			}
			
			

			// echo json_encode($data);
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');

			redirect_back();
			}
		}
	// END

	// KIRIM USULAN DARI USER KE SKPD
	public function kirim_usulan_user()
		{
			$this->form_validation->set_rules('B_02B', 'B_02B', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal merubah data.');
			redirect_back();
		
			}else{
			$data=array(
				"last_position"=>4,
				"status_ajuan" => 2
				
			);
			$this->db->where('B_02B', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan',$data);
			$data=array(
				"status_acc"=>0,
				
				
			);
			$this->db->where('id_pengajuan', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			// $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');
			
			redirect_back();
			
			
			}	
		}
	// END
	// REVISI USULAN DARI SKPD KE PNS
	public function revisi_skpd()
		{
			$this->form_validation->set_rules('B_02B', 'B_02B', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal merubah data.');
			redirect_back();
		
			}else{
			$keterangan = $this->input->post('keterangan');
			$data=array(
				"last_position"=>0,
				"status_ajuan" =>4,
				"keterangan" =>$keterangan,
				
			);
			$this->db->where('B_02B', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan',$data);

			$data=array(
				"status_acc"=>2,
				"nip_acc"=>$this->session->userdata('B_02B'),
				
			);
			$this->db->where('id_pengajuan', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			// $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');
			
			redirect_back();
			
			
			}	
		}
	// END
	// REVISI USULAN DARI BKD KE SKPD
	public function revisi_bkd()
		{
			$this->form_validation->set_rules('B_02B', 'B_02B', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal merubah data.');
			redirect_back();
		
			}else{
			$data=array(
				"last_position"=>1,
				"status_ajuan"=>0,
				
			);
			$this->db->where('B_02B', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan',$data);

			$data=array(
				"status_acc"=>2,
				
				"nip_acc"=>$this->session->userdata('B_02B'),
				
			);
			$this->db->where('id_pengajuan', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			// $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');
			
			redirect_back();
			
			
			}	
		}
	// END
	// ACC USULAN DARI BKD KE SKPD
	public function acc_bkd()
		{
			$this->form_validation->set_rules('B_02B', 'B_02B', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal merubah data.');
			redirect_back();
		
			}else{
			$data=array(
				"last_position"=>3,
				"status_ajuan"=>5
				
			);
			$this->db->where('B_02B', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan',$data);

			$data=array(
				"status_acc"=>1,
				"nip_acc"=>$this->session->userdata('B_02B'),
				
			);
			$this->db->where('id_pengajuan', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			// $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');
			
			redirect_back();
			
			
			}	
		}
	// END
	//EDIT INDEX BUP BKD
	public function edit_index_bup()
		{
			$row = $this->M_bup->get_usul();	
			$data = array(
				
				'row' => $row,
			);
			$this->template->load('template', 'admin/bup/edit_bup_bkd',$data);
			
		}

	
	//END
	
	//DELETE PENGAJUAN
	public function del($id)
		{
			
			$this->db->where('B_02B', $id);
			$this->db->delete('pensiun_pengajuan');
			redirect_back();
		}
    	//END

	//ACTION EDIT BUP BKD
	public function edit_bup_bkd($id)
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim');
			$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			// $idk = $this->input->post('b.B_02');
			if ($this->form_validation->run() == FALSE) {
				
				$query = $this->M_bup->getall($id);
				if ($query->num_rows() > 0) {
					$row = $query->row();
					$sutri = $this->M_bup->get_suami_istri($id);
					$anak = $this->M_bup->get_anak($id);
					$provinsi = $this->M_bup->get_provinsi();
					$kabkota = $this->M_bup->get_kab();
					$keca = $this->M_bup->get_keca();
					$desa = $this->M_bup->get_kelu();
					$status_anak = $this->M_bup->get_status_anak();
					$status_sutri = $this->M_bup->get_status_sutri();
					$status_acc = $this->M_bup->get_status_pengajuan($id);
					$data = array(
						'row' => $row,
						'sutri' => $sutri,
						'anak' => $anak,
						'provinsi' => $provinsi,
						'kabkota' => $kabkota,
						'keca' => $keca,
						'desa' => $desa,
						'status_anak' => $status_anak,
						'status_sutri' => $status_sutri,
						'status_acc' => $status_acc,

					);
					// $nip = $id;
					// $kodes = $this->input->post('kodes'); //sample => 10,11,13,14
					$kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
					$token = '!BKDoyee123';

					$params = array(
						'token' => $token,
						'kodes' => $kodes,
						'nip' => $id
						);

					$res = ApiClientPost($params);
					$result = json_decode($res, TRUE);
					$data['efilenya'] = $result['data'];  
					// echo json_encode($data);
					$this->template->load('template', 'admin/bup/edit_bup_bkd',$data); //dari tabel pensiun pngajuan
				} else {
					echo "<script>alert ('Data tidak ditemukan');";
					redirect_back();
				}

			// redirect('user/add_index');

		# code...
			} else {
			
			// $post = $this->input->post(null, TRUE);
			$this->M_bup->edit();
			if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
			redirect_back();
			}
			echo "<script>window.location ='" . site_url('Bup/bup_bkd') . "' ; </script>";
			}
			
		}
	//END

	// INDEX BUP SKPD
	public function bup_skpd()
		{
			$id = $this->input->post('A_01');
			$row = $this->M_bup->get_usul_by_bkd($id);
			$pns = $this->M_bup->get_waktu_pns_by_bkd($id);
			$data = array(
				'row' => $row,
				'pns' => $pns,
			);
			// echo json_encode($row);
			
			$this->template->load('template', 'admin/bup/view_bup_skpd',$data); //dari tabel pensiun pngajuan
		}

	public function bup_diusulkan_skpd()
		{
			$id = $this->input->get('A_01');
			
			
			$data['row'] = $this->M_bup->get_usul_by_id($id)->result_array();
			
			$this->template->load('template', 'admin/bup/view_bup_usulan_skpd',$data); //dari tabel pensiun pngajuan
		}
	//END

	//EDIT BUP SKPD
	public function edit_bup_skpd($id)
		{
			
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim');
			$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			// $idk = $this->input->post('b.B_02');
			if ($this->form_validation->run() == FALSE) {
				$query = $this->M_bup->getall($id);
				if ($query->num_rows() > 0) {
					$row = $query->row();
					$sutri = $this->M_bup->get_suami_istri($id);
					$anak = $this->M_bup->get_anak($id);
					$provinsi = $this->M_bup->get_provinsi();
					$kabkota = $this->M_bup->get_kab();
					$keca = $this->M_bup->get_keca();
					$desa = $this->M_bup->get_kelu();
					$status_anak = $this->M_bup->get_status_anak();
					$status_sutri = $this->M_bup->get_status_sutri();
					$status_acc = $this->M_bup->get_status_pengajuan($id);
					$data = array(
						'row' => $row,
						'sutri' => $sutri,
						'anak' => $anak,
						'provinsi' => $provinsi,
						'kabkota' => $kabkota,
						'keca' => $keca,
						'desa' => $desa,
						'status_anak' => $status_anak,
						'status_sutri' => $status_sutri,
						'status_acc' => $status_acc,

					);
					$kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
					$token = '!BKDoyee123';

					$params = array(
						'token' => $token,
						'kodes' => $kodes,
						'nip' => $id
						);

					$res = ApiClientPost($params);
					$result = json_decode($res, TRUE);
					$data['efilenya'] = $result['data'];  
					// echo json_encode($res);
					$this->template->load('template', 'admin/bup/edit_bup_skpd',$data); //dari tabel pensiun pngajuan
				} else {
					echo "<script>alert ('Data tidak ditemukan');";
					redirect_back();
				}

			// redirect('user/add_index');

		# code...
			} else {
			// $post = $this->input->post(null, TRUE);
			$this->M_bup->edit();
			if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
			redirect_back();
			}
			echo "<script>window.location ='" . site_url('Bup/bup_skpd') . "' ; </script>";
			}
			//dari tabel pensiun pngajuan
		}
	//END

	//EDIT BUP SKPD
	public function edit_bup_bkn($id)
		{
			
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim');
			$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			// $idk = $this->input->post('b.B_02');
			if ($this->form_validation->run() == FALSE) {
				$query = $this->M_bup->getall($id);
				if ($query->num_rows() > 0) {
					$row = $query->row();
					$sutri = $this->M_bup->get_suami_istri($id);
					$anak = $this->M_bup->get_anak($id);
					$provinsi = $this->M_bup->get_provinsi();
					$kabkota = $this->M_bup->get_kab();
					$keca = $this->M_bup->get_keca();
					$desa = $this->M_bup->get_kelu();
					$status_anak = $this->M_bup->get_status_anak();
					$status_sutri = $this->M_bup->get_status_sutri();
					$status_acc = $this->M_bup->get_status_pengajuan($id);
					$data = array(
						'row' => $row,
						'sutri' => $sutri,
						'anak' => $anak,
						'provinsi' => $provinsi,
						'kabkota' => $kabkota,
						'keca' => $keca,
						'desa' => $desa,
						'status_anak' => $status_anak,
						'status_sutri' => $status_sutri,
						'status_acc' => $status_acc,

					);
					$kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
					$token = '!BKDoyee123';

					$params = array(
						'token' => $token,
						'kodes' => $kodes,
						'nip' => $id
						);

					$res = ApiClientPost($params);
					$result = json_decode($res, TRUE);
					$data['efilenya'] = $result['data'];  
					// echo json_encode($res);
					$this->template->load('template', 'admin/bup/edit_bup_bkn',$data); //dari tabel pensiun pngajuan
				} else {
					echo "<script>alert ('Data tidak ditemukan');";
					redirect_back();
				}

			// redirect('user/add_index');

		# code...
			} else {
			// $post = $this->input->post(null, TRUE);
			$this->M_bup->edit();
			if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
			redirect_back();
			}
			echo "<script>window.location ='" . site_url('Bup/bup_skpd') . "' ; </script>";
			}
			//dari tabel pensiun pngajuan
		}
	//END

	//BUP USER 
	public function bup_user($id)
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim');
			$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			// $idk = $this->input->post('b.B_02');
			$nip = $this->session->userdata('B_02B');
			$sql = $this->db->query("SELECT * FROM pensiun_pengajuan WHERE B_02B = '$nip' AND diusulkan_bkd = 1 AND diusulkan_skpd = 1 ");
			$query= $sql->num_rows();
			if ($query > 0) {
				if ($this->form_validation->run() == FALSE) {
					$query = $this->M_bup->getall($id);
					if ($query->num_rows() > 0) {
						$row = $query->row();
						$sutri = $this->M_bup->get_suami_istri($id);
						$anak = $this->M_bup->get_anak($id);
						$provinsi = $this->M_bup->get_provinsi();
						$kabkota = $this->M_bup->get_kab();
						$keca = $this->M_bup->get_keca();
						$desa = $this->M_bup->get_kelu();
						$status_anak = $this->M_bup->get_status_anak();
						$status_sutri = $this->M_bup->get_status_sutri();
						$status_acc = $this->M_bup->get_status_pengajuan($id);
						$data = array(
							'row' => $row,
							'sutri' => $sutri,
							'anak' => $anak,
							'provinsi' => $provinsi,
							'kabkota' => $kabkota,
							'keca' => $keca,
							'desa' => $desa,
							'status_anak' => $status_anak,
							'status_sutri' => $status_sutri,
							'status_acc' => $status_acc,

						);
						$kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
						$token = '!BKDoyee123';

						$params = array(
							'token' => $token,
							'kodes' => $kodes,
							'nip' => $id
							);

						$res = ApiClientPost($params);
						$result = json_decode($res, TRUE);
						$data['efilenya'] = $result['data'];  
						// echo json_encode($data);
						$this->template->load('template', 'admin/bup/view_bup_user',$data); //dari tabel pensiun pngajuan
					} else {
						echo "<script>alert ('Data tidak ditemukan');";
						echo "window.location ='" . site_url('Bup/bup_user') . "' ; </scrip>";
					}

				// redirect('user/add_index');
				} else {
				// $post = $this->input->post(null, TRUE);
				$this->M_bup->edit();
					if($this->db->affected_rows() > 0){
					$this->session->set_flashdata('info', 'success');
					$this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
					redirect_back();
				}
				echo "<script>window.location ='" . site_url('Bup/bup_user') . "' ; </script>";
				}
			//dari tabel pensiun pngajuan
		}else{
			$this->template->load('template', 'admin/validate');
		}
		}

		public function updatesort()
		{
		$sortable = $this->input->post('sortable');
		// echo json_encode($sortable);
		$this->M_bup->updatesort_sutri($sortable);
		$this->session->set_flashdata('message', '<strong>Success!</strong> Menu position update.');

			
		}

		public function updatesort_anak()
		{
			$anak = $this->input->post('anak');
			// echo json_encode($sortable);
			$this->M_bup->updatesort_anak($anak);
			$this->session->set_flashdata('message', '<strong>Success!</strong> Menu position update.');

			
		}
	//END

// CRUD TABLE AHLIWARIS
    	// ADD ANAK
	public function insert_anak()
		{
			$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Ditambahkan");
			redirect_back();
		
			}else{
			$data=array(
				"KF_01" => $_POST['KF_01'],
				"KF_02" => $_POST['KF_02'],
				"KF_03" => $_POST['KF_03'],
				"KF_04" => $_POST['KF_04'],
				"KF_05" => $_POST['KF_05'],
				"KF_08" => $_POST['KF_08'],
				"KF_09" => $_POST['KF_09'],
				"KF_10" => $_POST['KF_10'],
				"keterangan"=>$_POST['keterangan'],
				"status"=>$_POST['status'],
				'updated_at' => date('Y-m-d H:i:s')
			);
			
			// $this->dbeps->insert('MASTKEL1',$data);
			$this->db->insert('MASTKEL1',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			
			redirect_back();
			
			
			}	
		}
	//END

	//EDIT ANAK
	public function edit_anak ()
		{
			$this->form_validation->set_rules('ID', 'ID', 'required');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect_back();
		
			}else{
			$data=array(
				"KF_03" => $_POST['KF_03'],
				"KF_04" => $_POST['KF_04'],
				"KF_05" => $_POST['KF_05'],
				"KF_08" => $_POST['KF_08'],
				"KF_09" => $_POST['KF_09'],
				"KF_10" => $_POST['KF_10'],
				"keterangan"=>$_POST['keterangan'],
				"status"=>$_POST['status'],
				'updated_at' => date('Y-m-d H:i:s')
			);
			// $this->dbeps->where('ID', $_POST['ID']);
			// $this->dbeps->update('MASTKEL1',$data);
			$this->db->where('ID', $_POST['ID']);
			$this->db->update('MASTKEL1',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			//      $id= $this->input->post('B_02B');
			// 	redirect ('Bup/edit_bup_bkd/',$id);
			redirect_back();
			
			
			}	
		}
	//END

	// ADD Sutri
	public function insert_sutri()
		{
			$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Ditambahkan");
			redirect_back();
		
			}else{
			$data=array(
				"KF_01" => $_POST['KF_01'],
				"KF_02" => $_POST['KF_02'],
				"KF_03" => $_POST['KF_03'],
				"KF_04" => $_POST['KF_04'],
				"KF_05" => $_POST['KF_05'],
				"KF_06" => $_POST['KF_06'],
				"KF_09" => $_POST['KF_09'],
				"KF_10" => $_POST['KF_10'],
				"keterangan"=>$_POST['keterangan'],
				"status"=>$_POST['status'],
				'created_at' => date('Y-m-d H:i:s')
			);
			
			// $this->dbeps->insert('MASTKEL1',$data);
			$this->db->insert('MASTKEL1',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			
			redirect_back();
			
			
			}	
		}
	//END

	//EDIT SUTRI
	public function edit_sutri ()
		{
			$this->form_validation->set_rules('ID', 'ID', 'required');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect_back();
		
			}else{
			$data=array(
				// "KF_01" => $_POST['KF_01'],
				// "KF_02" => $_POST['KF_02'],
				"KF_03" => $_POST['KF_03'],
				"KF_04" => $_POST['KF_04'],
				"KF_05" => $_POST['KF_05'],
				"KF_06" => $_POST['KF_06'],
				"KF_09" => $_POST['KF_09'],
				"KF_10" => $_POST['KF_10'],
				"keterangan"=>$_POST['keterangan'],
				"status"=>$_POST['status'],
				'updated_at' => date('Y-m-d H:i:s')
			);
			// $this->dbeps->where('ID', $_POST['ID']);
			// $this->dbeps->update('MASTKEL1',$data);
			$this->db->where('ID', $_POST['ID']);
			$this->db->update('MASTKEL1',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			//      $id= $this->input->post('B_02B');
			// 	redirect ('Bup/edit_bup_bkd/',$id);
			redirect_back();
			
			
			}	
		}
	//END
	
	//DELETE AHLIWARIS
	public function hapus_ahli_waris($id)
		{
			$data = array(
			'deleted_at' => date('Y-m-d H:i:s')
			);
			// $this->dbeps->where('ID', $id);
			// $this->dbeps->update('MASTKEL1',$data);
			$this->db->where('ID', $id);
			$this->db->update('MASTKEL1',$data);
			redirect_back();
		}
	//END

// END OF CRUD TABLE AHLIWARIS


// CHAINED DROPDOWN
   	// GET KOTA
	function list_kota()
		{
			$id_provinsi = $this->input->post('id');

			$sub = $this->M_bup->get_kota($id_provinsi);

			// Buat variabel untuk menampung tag-tag option nya
			// Set defaultnya dengan tag option Pilih
			$lists = "<option value='00'>Pilih</option>";

			foreach ($sub as $data) {
			$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
			}

			$callback = array('list_kota' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

			echo json_encode($callback); // konversi varibael $callback menjadi JSON
		}
	//END

	//GET KECAMATAN
	function list_kecamatan()
		{
			$id_kota = $this->input->post('id');


			$sub = $this->M_bup->get_kecamatan($id_kota);

			// Buat variabel untuk menampung tag-tag option nya
			// Set defaultnya dengan tag option Pilih
			$lists = "<option value='00'>Pilih</option>";

			foreach ($sub as $data) {
			$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
			}

			$callback = array('list_kecamatan' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

			echo json_encode($callback); // konversi varibael $callback menjadi JSON
		}
	//END

	
		
	// GET KELURAHAN
	function list_kelurahan()
		{
			$id_kecamatan = $this->input->post('id');


			$sub = $this->M_bup->get_kelurahan($id_kecamatan);

			// Buat variabel untuk menampung tag-tag option nya
			// Set defaultnya dengan tag option Pilih
			$lists = "<option value='00'>Pilih</option>";

			foreach ($sub as $data) {
			$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
			}

			$callback = array('list_kelurahan' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

			echo json_encode($callback); // konversi varibael $callback menjadi JSON
		}
	//END
//END OF CHAINED DROPDOWN

//SETTING WAKTU
	//BKD
	public function setting_bkd()
		{
		
			
			$row = $this->M_bup->get_setting();
			$data = array(
				'row' => $row,
			);
			$this->template->load('template', 'admin/setting/waktu_bkd',$data); //dari tabel EPS
		}
	public function insert_setting_bkd()
		{
		
				$this->form_validation->set_rules('waktu_skpd', 'waktu_skpd', 'required');
				if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Ditambahkan");
				redirect_back();
			
				}else{
				$data=array(
					"waktu_skpd" => $_POST['waktu_skpd'],
					"waktu_pns" => $_POST['waktu_pns'],
					
				);
				
				$this->db->insert('pensiun_waktu',$data);
				$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
				
				redirect_back();
				
				
				}	
		}

	public function edit_setting_bkd()
		{
				$this->form_validation->set_rules('id', 'id', 'required');
				$this->form_validation->set_rules('waktu_skpd', 'waktu_skpd', 'required');
				if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Di Edit");
				redirect_back();
			
				}else{
				$data=array(
					"waktu_skpd" => $_POST['waktu_skpd'],
					"waktu_pns" => $_POST['waktu_pns'],
				);
				$this->db->where('id', $_POST['id']);
				$this->db->update('pensiun_waktu',$data);
				$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
				//      $id= $this->input->post('B_02B');
				// 	redirect ('Bup/edit_bup_bkd/',$id);
				redirect_back();
				
				
				}	
		}
	function list_waktu_skpd()
		{
			$skpd = $this->input->post('A_01');

			$sub = $this->M_bup->get_list_skpd($skpd);

			// Buat variabel untuk menampung tag-tag option nya
			// Set defaultnya dengan tag option Pilih
			$lists = "<option value='0'>Pilih</option>";

			foreach ($sub as $data) {
			$lists .= "<option value='" . $data->waktu_skpd . "'>" . $data->waktu_skpd . " Hari</option>"; // Tambahkan tag option ke variabel $lists
			}

			$callback = array('l_waktu_skpd' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

			echo json_encode($callback); // konversi varibael $callback menjadi JSON
		}

	//END
	//SKPD
	public function setting_skpd()
		{
			$id = $this->session->userdata('A_01');
			$row = $this->M_bup->get_setting($id);
			$setting = $this->M_bup->get_setting_skpd($id);
			$data = array(
				'row' => $row,
				'setting' =>$setting
			);
			// echo json_encode($data);
			$this->template->load('template', 'admin/setting/waktu_skpd',$data); //dari tabel EPS
		}
	public function insert_setting_skpd()
		{
		
				$this->form_validation->set_rules('A_01', 'A_01', 'required');
				if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Ditambahkan");
				redirect_back();
			
				}else{
				$data=array(
					"waktu_skpd" => $_POST['waktu_skpd'],
					"waktu_bkd" => $_POST['waktu_bkd'],
					
				);
				
				$this->db->insert('pensiun_waktu',$data);
				$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
				
				redirect_back();
				
				
				}	
		}
		//END
//END
	function expired()
		{
		$bulan = $this->input->post('sewa');
		$tanggalSekarang = date('d-m-Y');
		$tanggalDuedate = date("d-m-Y", strtotime($tanggalSekarang.' + '.$bulan.' Days'));
		echo "tanggal sekarang :".$tanggalSekarang."<br>";
		echo "tanggal overdue :".$tanggalDuedate;
		}

	function cetak_sk_($id)

		{
		
			$pdf = new FPDF('L', 'mm','Legal');

			$pdf->AddPage();
			// Tampilkan logo pada sudut kiri atas pada 300 DPI
			$query = $this->M_bup->getall($id)->result();
			$no=1;
			foreach ($query as $data){

			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(220,4,'B. KELUARGA PENERIMA PENSIUN',0,0,'R');
			$pdf->Cell(20,7,'',0,1);
			$pdf->Cell(10,7,'',0,1);
			$pdf->Cell(100,4,'KEPUTUSAN GUBERNUR JAWA TENGAH',0,1,'R');
			$pdf->Cell(98,4,'NOMOR : 882/9325/015/23300/AZ/12/21',0,1,'R');
			$pdf->Cell(120,4,'TENTANG PEMBERIAN KENAIKAN PANGKAT PENGABDIAN, PEMBERHENTIAN DAN',0,1,'R');
			$pdf->Cell(123,4,'PEMBERIAN PENSIUN PEGAWAI NEGERI SIPIL YANG MENCAPAI BATAS USIA PENSIUN',0,1,'R');
			$pdf->Cell(100,3,'',0,1,'R');
			$pdf->Cell(100,4,'DENGAN RAHMAT TUHAN YANG MAHA ESA',0,1,'R');
			$pdf->Cell(87,4,'GUBERNUR JAWA TENGAH',0,1,'R');
			$pdf->Cell(100,3,'',0,1,'R');
			// 0 adalah nilai dari lebar yang digunakan. dengan nilai 0 artinya bahwa kita tidak mengatur lebar cell tersebut.
			// 7 nilai tersebut menandakan tinggi cell.
			// ”CONTOH DAFTAR MAHASISWA’ merupakan kalimat yang akan dicetak didalam cell tersebut.
			// 0 Merupakan nilai untuk pembuatan border cell. dengan mengguakan 0 artinya bahwa border tidak digunakan. Sementara jika ingin menggunakan border masukan nilai 1.
			// 1 Menujukan kemana posisi akan berpindah setelah pembuatan cell selesai 1 menujukan perpindahan posisi pada awal baris berikutnya. Jika tidak ingin berpindah baris gunakan nilai 0.
			// ‘C’ Merupakan jenis untuk perataan teks didalam cell. ‘C’ digunakan untuk rata tengah. Sementara’ L’ digunakan untuk rata kiri dan ‘R’ untuk rata kanan.
			$pdf->SetFont('Arial','',8);

			$pdf->Cell(5,4,'Menimbang',0,0);
			$pdf->Cell(17,4,'',0,0);
			$pdf->Cell(3,4,':',0,0);
			$pdf->Cell(5,4,'bahwa Pegawai Negeri Sipil yang namanya tercantum dalam keputusan ini telah mencapai batas usia ',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'pensiun dan telah memenuhi syarat untuk diberikan kenaikan pangkat pengabdian dan diberhentikan  ',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'dengan hormat sebagai Pegawai Negeri Sipil dengan hak pensiun. ',0,1,'L');
			$pdf->Cell(5,4,'',0,1);

			$pdf->Cell(5,4,'Mengingat',0,0);
			$pdf->Cell(17,4,'',0,0);
			$pdf->Cell(3,4,':',0,0);
			$pdf->Cell(5,4,'1. Pasal 4 ayat (1) Undang-Undang Dasar Republik Indonesia Tahun 1945;',0,1);
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'2. Undang-Undang Nomor 11 Tahun 1969 tentang Pensiun Pegawai dan Pensiun Janda/Duda Pegawai;',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'4. Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara;',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'4. Peraturan Pemerintah Nomor 7 Tahun 1977 tentang Peraturan Gaji PNS jo Peraturan Pemerintah ',0,1,'L');
			$pdf->Cell(28,4,'',0,0);
			$pdf->Cell(5,4,'Nomor 15 Tahun 2019;',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'5. Peraturan Pemerintah Nomor 18 Tahun 2019 tentang Penetapan Pensiun Pokok Pensiunan PNS ',0,1,'L');
			$pdf->Cell(28,4,'',0,0);
			$pdf->Cell(5,4,'dan Janda/Dudanya;',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'6. Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil;',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'7. Peraturan Badan Kepegawaian Negara Nomor 2 Tahun 2018 tentang Pedoman Pemberian',0,1,'L');
			$pdf->Cell(28,4,'',0,0);
			$pdf->Cell(5,4,'Pertimbangan Teknis Pensiun Pegawai Negeri Sipil dan Pensiun Janda/Duda Pegawai Negeri',0,1,'L');
			$pdf->Cell(28,4,'',0,0);
			$pdf->Cell(5,4,'Sipil',0,1,'L');

			$pdf->Cell(5,4,'Memperhatikan',0,0);
			$pdf->Cell(17,4,'',0,0);
			$pdf->Cell(3,4,':',0,0);
			$pdf->Cell(5,4,'Pertimbangan Teknis Kepala Badan Kepegawaian Negara/Kepala Kantor Regional Badan Kepegawaian ',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'Negara Nomor PH-23300002684 Tanggal 16-11-2021.',0,1,'L');
			$pdf->Cell(5,4,'',0,1);
			$pdf->Cell(60,4,'',0,0);
			$pdf->Cell(10,4,'MEMUTUSKAN :',0,0);
			$pdf->Cell(17,6,'',0,1);
			$pdf->Cell(5,4,'Memperhatikan',0,0);
			$pdf->Cell(17,4,'',0,0);
			$pdf->Cell(3,4,':',0,0);
			$pdf->Cell(10,4,'',0,1);
			$pdf->Cell(5,4,'KESATU',0,0);
			$pdf->Cell(17,4,'',0,0);
			$pdf->Cell(3,4,':',0,0);
			$pdf->Cell(5,4,'(1)  Memberikan kenaikan pangkat pengabdian kepada Pegawai Negeri Sipil yang namanya tersebut ',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(5,4,'dalam lajur 1 dari dan menjadi sebagaimana tersebut dalam lajur 6 dengan gaji pokok dari dan',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(5,4,'menjadi sebagaimana tersebut dalam lajur 8 Keputusan ini.',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'(2)  Memberhentikan dengan hormat sebagai Pegawai Negeri Sipil yang namanya tersebut dalam lajur',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(5,4,'1 pada akhir bulan tersebut pada lajur 10 Keputusan ini, disertai ucapan terima kasih atas jasa-jasa',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(5,4,'selama bekerja pada Pemerintah Republik Indonesia.',0,1,'L');
			$pdf->Cell(25,4,'',0,0);
			$pdf->Cell(5,4,'(3) Terhitung mulai tanggal tersebut dalam lajur 11, kepadanya diberikan pensiun pokok sebulan sebesar',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(5,4,'tersebut dalam lajur 12 Keputusan ini.',0,1,'L');
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(10,4,'',0,1);
			

			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(30,4,'',0,0);
			$pdf->Cell(30,7,'A. PENERIMA PENSIUN',0,1,'L');
			$pdf->Cell(10,7,'',0,1);

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(5,7,'1.',0,0,);
			$pdf->Cell(10,7,'KETERANGAN PRIBADI',0,0);
			$pdf->Cell(140,7,'',0,0);
		
			$pdf->Cell(0,7,'B. ANAK KANDUNG',0,1);

			$pdf->SetFont('Arial','',8);

			$pdf->Cell(5,3,'A.',0,0,);
			$pdf->Cell(10,3,'NAMA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->nama,0,0);
			$pdf->Cell(95,3,'',0,0);

			// TABEL ANAK KANDUNG
			$pdf->Cell(7,4,'NO',1,0,'C');
			$pdf->Cell(40,4,'NAMA',1,0,'C');
			$pdf->Cell(30,4,'TGL LAHIR',1,0,'C');
			$pdf->Cell(40,4,'NAMA AYAH/IBU',1,0,'C');
			$pdf->Cell(40,4,'KETERANGAN',1,0,'C');
			$pdf->Cell(5,4,'',0,1);
			// END

			$pdf->Cell(5,3,'B.',0,0,);
			$pdf->Cell(10,3,'NIP',0,);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->B_02B,0,0);
			$pdf->Cell(95,3,'',0,0);

			// TABEL ANAK KANDUNG
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(7,4,$no,1,0);
			$pdf->Cell(40,4,'M.ROFI S',1,0);
			$pdf->Cell(30,4,'1996-08-13',1,0);
			$pdf->Cell(40,4,$data->nama,1,0);
			$pdf->Cell(40,4,'Anak Kandung',1,0);
			$pdf->Cell(5,3,'',0,1);
			// END

			$pdf->Cell(5,3,'C.',0,0,);
			$pdf->Cell(10,3,'TEMPAT/TGL LAHIR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->B_04.','.$data->tl,0,1);
			$pdf->Cell(5,3,'D.',0,0,);
			$pdf->Cell(10,3,'JABATAN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->I_JB,0,1);
			$pdf->Cell(5,3,'E.',0,0,);
			$pdf->Cell(10,3,'PANGKAT/GOL.RUANG/TMT',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->pangkat.','.$data->F_PK,0,1);
			$pdf->Cell(5,3,'F.',0,0,);
			$pdf->Cell(10,3,'GAJI POKOK TERAKHIR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'3,491,500',0,1);
			$pdf->Cell(5,3,'H.',0,0,);
			$pdf->Cell(10,3,'MASA KERJA GOLONGAN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'21 TAHUN',0,1);
			$pdf->Cell(5,3,'I.',0,0,);
			$pdf->Cell(10,3,'MASA KERJA PENSIUN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'28 TAHUN',0,1);
			$pdf->Cell(5,3,'J.',0,0,);
			$pdf->Cell(10,3,'PENINJAUAN MASA KERJA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'__ TAHUN',0,1);
			$pdf->Cell(5,3,'K.',0,0,);
			$pdf->Cell(10,3,'PENDIDIKAN DASAR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'',0,1);
			$pdf->Cell(5,3,'L.',0,0,);
			$pdf->Cell(10,3,'PENGANGKATAN PERTAMA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'',0,1);
			$pdf->Cell(10,3,'',0,1);

			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(5,3,'2.',0,0,);
			$pdf->Cell(10,3,'KETERANGAN KELUARGA',0,0);
			$pdf->Cell(140,3,'',0,0);
			$pdf->Cell(10,3,'3. ALAMAT SESUDAH PENSIUN',0,0);
			$pdf->Cell(10,4,'',0,1);
			$pdf->Cell(5,3,'A.',0,0,);
			$pdf->Cell(10,3,'NAMA SUAMI / ISTERI',0,0);
			$pdf->Cell(140,3,'',0,0);
			$pdf->Cell(10,3,$data->alamat,0,0);
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(155,7,'',0,0);
			$pdf->Cell(10,3,'4. DENGAN DPCP INI DIBUAT DENGAN SEBENARNYA DIPERGUNAKAN SEBAGAIMANA MESTINYA',0,0);
			$pdf->Cell(10,3,'',0,1);
			

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(8,4,'No',1,0,'C');
			
			$pdf->Cell(50,4,'Nama',1,0,'C');
			$pdf->Cell(35,4,'Tgl Lahir',1,0,'C');
			$pdf->Cell(35,4,'Tgl Menikah',1,1,'C');

			$pdf->SetFont('Arial','',8);
			
			
			$pdf->Cell(8,4,$no,1,0);
			$pdf->Cell(50,4,$data->namas,1,0);
			$pdf->Cell(35,4,$data->tgl_lahir,1,0);
			$pdf->Cell(35,4,$data->KF_06,1,1);
			
			
			$no++;

			$pdf->SetFont('Arial','',10);
			
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(10,3,'',0,1);
			$pdf->Cell(0,5,'MENGETAHUI',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'SEMARANG,    OKTOBER 2018',0,1);
			$pdf->Cell(0,5,'KASUBAG UMUM DAN KEPEGAWAIAN',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'PNS YANG BERSANGKUTAN',0,1);
			$pdf->Image('assets/qr.png',20,158,25);
			$pdf->Image('assets/qr.png',235,158,25);
			$pdf->Cell(10,25,'',0,1);
			
			$pdf->Cell(0,5,' AMINURDIN, S.STP',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,$data->nama,0,1);
			$pdf->Cell(0,5,' NIP. 197710211997031001',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'NIP.'.$data->B_02B,0,1);
			
			}
			$pdf->Output();
		}

	function cetak($id)

		{
		
			$pdf = new FPDF('L', 'mm','Legal');

			$pdf->AddPage();
			// Tampilkan logo pada sudut kiri atas pada 300 DPI
			$query = $this->M_bup->getall($id)->result();
			$anak = $this->M_bup->get_anak($id);
			$no=1;
			foreach ($query as $data){

			$pdf->SetFont('Arial','B',10);
			$pdf->Image('assets/garuda.png',160,1,-1900,'C');
			$pdf->Cell(10,7,'',0,1);
			$pdf->Cell(10,7,'',0,1);
			$pdf->Cell(0,7,'BADAN ADMINISTRASI KEPEGAWAIAN NEGARA',0,1,'C');
			$pdf->Cell(10,7,'',0,1);
			// 0 adalah nilai dari lebar yang digunakan. dengan nilai 0 artinya bahwa kita tidak mengatur lebar cell tersebut.
			// 7 nilai tersebut menandakan tinggi cell.
			// ”CONTOH DAFTAR MAHASISWA’ merupakan kalimat yang akan dicetak didalam cell tersebut.
			// 0 Merupakan nilai untuk pembuatan border cell. dengan mengguakan 0 artinya bahwa border tidak digunakan. Sementara jika ingin menggunakan border masukan nilai 1.
			// 1 Menujukan kemana posisi akan berpindah setelah pembuatan cell selesai 1 menujukan perpindahan posisi pada awal baris berikutnya. Jika tidak ingin berpindah baris gunakan nilai 0.
			// ‘C’ Merupakan jenis untuk perataan teks didalam cell. ‘C’ digunakan untuk rata tengah. Sementara’ L’ digunakan untuk rata kiri dan ‘R’ untuk rata kanan.
			$pdf->SetFont('Arial','',8);

			$pdf->Cell(5,3,'INSTANSI',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'PEMERINTAH',0,1);

			$pdf->Cell(5,3,'PROVINSI',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'JAWA TENGAH',0,1);

			$pdf->Cell(5,3,'KOTA',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'SEMARANG',0,1);

			$pdf->Cell(5,3,'UNIT KERJA',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->nmskpd,0,1);

			$pdf->Cell(5,3,'PEMBAYARAN',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'KASDA',0,1);

			$pdf->Cell(5,3,'BUP',0,0);
			$pdf->Cell(28,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'-',0,1);

			$pdf->Cell(10,2,'',0,1);

			$pdf->SetFont('Arial','B',10);

			$pdf->Cell(0,7,'DATA PERORANGAN CALON PENERIMA PENSIUN (DPCP) PEGAWAI NEGERI SIPIL',0,1,'C');
			$pdf->Cell(10,7,'',0,1);

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(5,7,'1.',0,0,);
			$pdf->Cell(10,7,'KETERANGAN PRIBADI',0,0);
			$pdf->Cell(140,7,'',0,0);
		
			$pdf->Cell(0,7,'B. ANAK KANDUNG',0,1);

			$pdf->SetFont('Arial','',8);

			$pdf->Cell(5,3,'A.',0,0,);
			$pdf->Cell(10,3,'NAMA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->nama,0,0);
			$pdf->Cell(95,3,'',0,0);

			// TABEL ANAK KANDUNG
			$pdf->Cell(7,4,'NO',1,0,'C');
			$pdf->Cell(40,4,'NAMA',1,0,'C');
			$pdf->Cell(30,4,'TGL LAHIR',1,0,'C');
			$pdf->Cell(40,4,'NAMA AYAH/IBU',1,0,'C');
			$pdf->Cell(40,4,'KETERANGAN',1,0,'C');
			$pdf->Cell(5,4,'',0,1);
			// END

			$pdf->Cell(5,3,'B.',0,0,);
			$pdf->Cell(10,3,'NIP',0,);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->B_02B,0,0);
			$pdf->Cell(95,3,'',0,0);
			

			// TABEL ANAK KANDUNG
			
			// $nox=1;
			foreach ($anak as $x){
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(7,4,$no ++,1,0);
			$pdf->Cell(40,4,$x->KF_04,1,0);
			$pdf->Cell(30,4,'1996-08-13',1,0);
			$pdf->Cell(40,4,$x->nama,1,0);
			$pdf->Cell(40,4,'Anak Kandung',1,0);
			
			$pdf->Cell(5,3,'',0,1);
			
			// END
			}
			$pdf->Cell(5,3,'C.',0,0,);
			$pdf->Cell(10,3,'TEMPAT/TGL LAHIR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->B_04.','.$data->tl,0,1);
			$pdf->Cell(5,3,'D.',0,0,);
			$pdf->Cell(10,3,'JABATAN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->I_JB,0,1);
			$pdf->Cell(5,3,'E.',0,0,);
			$pdf->Cell(10,3,'PANGKAT/GOL.RUANG/TMT',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,$data->pangkat.','.$data->F_PK,0,1);
			$pdf->Cell(5,3,'F.',0,0,);
			$pdf->Cell(10,3,'GAJI POKOK TERAKHIR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'3,491,500',0,1);
			$pdf->Cell(5,3,'H.',0,0,);
			$pdf->Cell(10,3,'MASA KERJA GOLONGAN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'21 TAHUN',0,1);
			$pdf->Cell(5,3,'I.',0,0,);
			$pdf->Cell(10,3,'MASA KERJA PENSIUN',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'28 TAHUN',0,1);
			$pdf->Cell(5,3,'J.',0,0,);
			$pdf->Cell(10,3,'PENINJAUAN MASA KERJA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'__ TAHUN',0,1);
			$pdf->Cell(5,3,'K.',0,0,);
			$pdf->Cell(10,3,'PENDIDIKAN DASAR',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'',0,1);
			$pdf->Cell(5,3,'L.',0,0,);
			$pdf->Cell(10,3,'PENGANGKATAN PERTAMA',0,0);
			$pdf->Cell(35,3,'',0,0);
			$pdf->Cell(5,3,':',0,0);
			$pdf->Cell(5,3,'',0,1);
			$pdf->Cell(10,3,'',0,1);

			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(5,3,'2.',0,0,);
			$pdf->Cell(10,3,'KETERANGAN KELUARGA',0,0);
			$pdf->Cell(140,3,'',0,0);
			$pdf->Cell(10,3,'3. ALAMAT SESUDAH PENSIUN',0,0);
			$pdf->Cell(10,4,'',0,1);
			$pdf->Cell(5,3,'A.',0,0,);
			$pdf->Cell(10,3,'NAMA SUAMI / ISTERI',0,0);
			$pdf->Cell(140,3,'',0,0);
			$pdf->Cell(10,3,$data->alamat,0,0);
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(155,7,'',0,0);
			$pdf->Cell(10,3,'4. DENGAN DPCP INI DIBUAT DENGAN SEBENARNYA DIPERGUNAKAN SEBAGAIMANA MESTINYA',0,0);
			$pdf->Cell(10,3,'',0,1);
			

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(8,4,'No',1,0,'C');
			
			$pdf->Cell(50,4,'Nama',1,0,'C');
			$pdf->Cell(35,4,'Tgl Lahir',1,0,'C');
			$pdf->Cell(35,4,'Tgl Menikah',1,1,'C');

			$pdf->SetFont('Arial','',8);
			
			
			$pdf->Cell(8,4,$no,1,0);
			$pdf->Cell(50,4,$data->namas,1,0);
			$pdf->Cell(35,4,$data->tgl_lahir,1,0);
			$pdf->Cell(35,4,$data->KF_06,1,1);
			
			
			$no++;

			$pdf->SetFont('Arial','',10);
			
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(10,5,'',0,1);
			$pdf->Cell(10,3,'',0,1);
			$pdf->Cell(0,5,'MENGETAHUI',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'SEMARANG,    OKTOBER 2018',0,1);
			$pdf->Cell(0,5,'KASUBAG UMUM DAN KEPEGAWAIAN',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'PNS YANG BERSANGKUTAN',0,1);
			$pdf->Image('assets/qr.png',20,158,25);
			$pdf->Image('assets/qr.png',235,158,25);
			$pdf->Cell(10,25,'',0,1);
			
			$pdf->Cell(0,5,' AMINURDIN, S.STP',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,$data->nama,0,1);
			$pdf->Cell(0,5,' NIP. 197710211997031001',0,0);
			$pdf->Cell(-120,5,'',0,0);
			$pdf->Cell(0,5,'NIP.'.$data->B_02B,0,1);
			
			}
			$pdf->Output();
		}
	public function bup_diajukan_skpd()
		{
		
			
			
			
			$data['row'] = $this->M_bup->get_ajuan_skpd()->result_array();
			
			$this->template->load('template', 'admin/bup/view_bup_diajukan_skpd',$data); 	
		}

	public function edit_bup_diajukan_skpd($id)
		{
			
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim');
			$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			// $idk = $this->input->post('b.B_02');
			if ($this->form_validation->run() == FALSE) {
				$query = $this->M_bup->getall($id);
				if ($query->num_rows() > 0) {
					$row = $query->row();
					$sutri = $this->M_bup->get_suami_istri($id);
					$anak = $this->M_bup->get_anak($id);
					$provinsi = $this->M_bup->get_provinsi();
					$kabkota = $this->M_bup->get_kab();
					$keca = $this->M_bup->get_keca();
					$desa = $this->M_bup->get_kelu();
					$status_anak = $this->M_bup->get_status_anak();
					$status_sutri = $this->M_bup->get_status_sutri();
					$status_acc = $this->M_bup->get_status_pengajuan($id);
					$data = array(
						'row' => $row,
						'sutri' => $sutri,
						'anak' => $anak,
						'provinsi' => $provinsi,
						'kabkota' => $kabkota,
						'keca' => $keca,
						'desa' => $desa,
						'status_anak' => $status_anak,
						'status_sutri' => $status_sutri,
						'status_acc' => $status_acc,

					);
					$kodes = '28_7,04_12,02,03,150_1,150_2,04_11,10,11,23_3,30_14,31,16,44_2,44_1,28_8';
					$token = '!BKDoyee123';

					$params = array(
						'token' => $token,
						'kodes' => $kodes,
						'nip' => $id
						);

					$res = ApiClientPost($params);
					$result = json_decode($res, TRUE);
					$data['efilenya'] = $result['data'];  
					// echo json_encode($data);
					$this->template->load('template', 'admin/bup/edit_bup_diajukan_skpd',$data); //dari tabel pensiun pngajuan
				} else {
					echo "<script>alert ('Data tidak ditemukan');";
					redirect_back();
				}

			// redirect('user/add_index');

		# code...
			} else {
			// $post = $this->input->post(null, TRUE);
			$this->M_bup->edit_ajuan_skpd();
			if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil merubah data Calon BUP.');
			redirect_back();
			}
			echo "<script>window.location ='" . site_url('Bup/bup_skpd') . "' ; </script>";
			}
			//dari tabel pensiun pngajuan
		}
	//END
	function cetak_sk()
	{
		$this->load->view('admin/bup/print_sk');
	}

	public function acc_ajuan_skpd()
		{
			$this->form_validation->set_rules('B_02B', 'B_02B', 'required');
			if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal merubah data.');
			redirect_back();
		
			}else{
			$data=array(
				"last_position"=>2,
				"status_ajuan"=>5,
			);
			$this->db->where('B_02B', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan',$data);

			$data=array(
				"status_acc"=>1,
				"nip_acc"=>$this->session->userdata('B_02B'),
				
			);
			$this->db->where('id_pengajuan', $_POST['B_02B']);
			$this->db->update('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			// $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data');
			
			redirect_back();
			
			
			}	
		}

		public function bup_acc()
		{
			$data['row'] = $this->M_bup->get_bup_acc()->result_array();
			
			$this->template->load('template', 'admin/bup/view_bup_acc',$data); 
		}

		// Create zip
    public function createzip(){

	
     // Read file from path
     if($this->input->post('but_createzip1') != NULL){

	
        // File path
        // $filepath1 = FCPATH.'/uploads/image1.jpg';
        // $filepath2 = FCPATH.'/uploads/document/users.csv';
        // $filepath1 = FCPATH.;
        $filepath2 = FCPATH.'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf';

        // Add file
        $this->zip->read_file($filepath2);
        // $this->zip->read_file($filepath2);
	// echo json_encode($filepath1);

        // Download
        $filename = "backup.zip";
        $this->zip->download($filename);
	     
	

     }

     // Read files from directory
     if($this->input->post('but_createzip2') != NULL){
	
        // File name
        $filename = "backup.zip";
        // Directory path (uploads directory stored in project root)
        $path = 'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf';

        // Add directory to zip
	// echo json_encode($path);
        $this->zip->read_dir($path);

        // Save the zip file to archivefiles directory
        $this->zip->archive(FCPATH.'/archivefiles/'.$filename);

        // Download
	// echo json_encode($filename);
        $this->zip->download($filename);
	

     // Load view
     $this->template->load('template', 'admin/bup/view_bup_acc'); 
  }
}


public function lakukan_download($filename = NULL){
	$filename = '1';

		// force_download($file);
		 // load download helder
		$this->load->helper('download');
		// read file contents
		// $data = file_get_contents('http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf'.$filename);
		$data = 'http://efile.bkd.jatengprov.go.id/assets/efile/1966/196608261987022002/196608261987022002_02.pdf'.$filename;
		
		force_download($filename, $data);

	}

// BLAST ACC BKD KE BKN
	public function ajuan_bkn()
		{
			
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

			$id = $this->input->post('B_02B');
			$ids = $this->input->post('B_02B');
			$blast_bkd = $this->input->post('diusulkan_bkd');
			// $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
			// $B_03 = $this->input->post('B_03');
			$kunci = $this->input->post('kunci_nominatif');
			$A_01 = $this->input->post('A_01');
			$wskpd = $this->input->post('id_waktu_skpd');
			$tanggalSekarang = date('Y-m-d H:i:s');
			$tanggalDuedate = date("Y-m-d H:i:s", strtotime($tanggalSekarang.' + '.$wskpd.' Days'));
			echo "tanggal sekarang :".$tanggalSekarang."<br>";
			echo "tanggal overdue :".$tanggalDuedate;
			// $AK_TMT = $this->input->post('AK_TMT');
			if ($this->form_validation->run() == FALSE) {
			// 	echo "d";
			// die();
			//  $row = $this->M_bup->get();
			//  $dinas = $this->M_bup->get_skpd();
			//  $data = array(
			// 	'row' => $row,
			// 	'dinas' => $dinas,
			//  );
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
			//  $this->template->load('template', 'admin/bup/index',$data);
			// redirect('bup');
			redirect_back();

			}else {
				
			// $this->M_bup->insert_history_acc();
			
			for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				// 'B_02B' => $id[$i],
				'last_position' => 3,
				
			);
			$this->db->where('B_02B', $id[$i]); 
			$this->db->update('pensiun_pengajuan',$data);
				

			}
			for ($x=0; $x < sizeof($ids); $x++) 
			{ 
			$data = array(
				'id_pengajuan' => $ids[$x],
				'nip_acc' => $this->session->userdata('B_02B'),
				'status_acc'=>0,
				// 'jabatan_acc'=>,
				'created_at' => date('Y:m:d H:i:s'),
				
			);
			
			$this->db->insert('pensiun_pengajuan_acc',$data);
			
			
			}
			
			 $yourfile = "/asstes/nip.zip";
			$file_name = 'all.zip';
			header("Content-Type: application/zip");
			header("Content-Disposition: attachment; filename=$file_name");
			header("Content-Length: " . filesize($yourfile));
			readfile($yourfile);

			// echo json_encode($sql);
			
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');

			// redirect_back_alt();
			// $this->template->load('template', 'admin/bup/view_bup_acc'); 
			}
		}
	//END
	public function bup_ajuan_bkn()
		{
			$data = array(
				'page' => 'acc',
				'pages' => 'tolak',
				
			);
			$data['row'] = $this->M_bup->get_bup_bkn()->result_array();
			
			
			$this->template->load('template', 'admin/bup/view_bup_bkn',$data); 
		}
		// BLAST ACC BKD KE BKN
	public function acc_tolak_bkn()
		{
			$this->form_validation->set_rules('B_02B', 'NIP', 'trim|is_unique[pensiun_pengajuan.B_02B]');

			$id = $this->input->post('B_02B');
			$ids = $this->input->post('B_02B');
			$blast_bkd = $this->input->post('diusulkan_bkd');
			// $ids = $this->input->post('B_02'); //here i am getting student id from the checkbox
			// $B_03 = $this->input->post('B_03');
			$kunci = $this->input->post('kunci_nominatif');
			$A_01 = $this->input->post('A_01');
			$wskpd = $this->input->post('id_waktu_skpd');
			$tanggalSekarang = date('Y-m-d H:i:s');
			$tanggalDuedate = date("Y-m-d H:i:s", strtotime($tanggalSekarang.' + '.$wskpd.' Days'));
			echo "tanggal sekarang :".$tanggalSekarang."<br>";
			echo "tanggal overdue :".$tanggalDuedate;
			// $AK_TMT = $this->input->post('AK_TMT');
			if ($this->form_validation->run() == FALSE) {

			//  $row = $this->M_bup->get();
			//  $dinas = $this->M_bup->get_skpd();
			//  $data = array(
			// 	'row' => $row,
			// 	'dinas' => $dinas,
			//  );
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Gagal menambahkan data.');
			//  $this->template->load('template', 'admin/bup/index',$data);
			// redirect('bup');
			redirect_back();

			}else {
				
			// $this->M_bup->insert_history_acc();
			if (isset($_POST['acc'])) {
				for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				// 'B_02B' => $id[$i],
				'last_position' => 3,
				
			);
			$this->db->where('B_02B', $id[$i]); 
			$this->db->update('pensiun_pengajuan',$data);
				
			// echo json_encode($data);
			}
			for ($x=0; $x < sizeof($ids); $x++) 
			{ 
			$data = array(
				'id_pengajuan' => $ids[$x],
				'nip_acc' => $this->session->userdata('B_02B'),
				'status_acc'=>3,
				// 'jabatan_acc'=>,
				'created_at' => date('Y:m:d H:i:s'),
				
			);
			
			$this->db->insert('pensiun_pengajuan_acc',$data);
			
			// echo json_encode($data);
			}
				
			}elseif (isset($_POST['tolak'])) {
				for ($i=0; $i < sizeof($id); $i++) 
			{ 
			$data = array(
				// 'B_02B' => $id[$i],
				'last_position' => 3,
				
			);
			$this->db->where('B_02B', $id[$i]); 
			$this->db->update('pensiun_pengajuan',$data);
				
			// echo json_encode($data);
			}
			for ($x=0; $x < sizeof($ids); $x++) 
			{ 
			$data = array(
				'id_pengajuan' => $ids[$x],
				'nip_acc' => $this->session->userdata('B_02B'),
				'status_acc'=>4,
				// 'jabatan_acc'=>,
				'created_at' => date('Y:m:d H:i:s'),
				
			);
			
			$this->db->insert('pensiun_pengajuan_acc',$data);
			// echo json_encode($data);
			
			}
			}else{
			redirect_back();
				
			}
			

			// echo json_encode($sql);
			
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');

			redirect_back();
			//  return redirect('bup');
			}
		}
	//END

	public function bkn_acc()
		{
			$data['row'] = $this->M_bup->get_bkn_acc()->result_array();
			
			$this->template->load('template', 'admin/bup/view_bkn_acc',$data); 
		}

	public function bkn_tolak()
		{
			$data['row'] = $this->M_bup->get_bkn_tolak()->result_array();
			
			$this->template->load('template', 'admin/bup/view_bup_tms',$data); 
		}

	// UPLOAD SK
	function upload_sk(){
        $config['upload_path'] = './views/sk/'; //path folder
        $config['allowed_types'] = 'pdf|docx'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
        $this->upload->initialize($config);
        if(!empty($_FILES['sk']['name'])){
 
            if ($this->upload->do_upload('sk')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./views/sk/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './views/sk/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $file=$gbr['file_name'];
		$id = $this->input->post('id', true);
		$status = $this->input->post('status_bkn');
		// $id=$this->input->post('id');
                // $nosk=$this->input->post('no_sk');
                // $this->M_bup->simpan_upload($file,$nosk,$id);
                // echo "Image berhasil diupload";
		 
		 $nosk = $this->input->post('no_sk', true);
			$data = array(
			// 'B_02B'          =>$id,
			'file_sk'         =>$file,
			'no_sk'          =>$nosk,
			'status_bkn'	=>$status
				
			);
			$this->db->where('B_02B', $id); 
			$this->db->update('pensiun_pengajuan',$data); 
		redirect_back();
		// echo json_encode($data);

            }
                      
        }else{
            echo "Image yang diupload kosong";
        }
                 
    }

    function download($id)
	{
		$data = $this->db->get_where('pensiun_pengajuan',['B_02B'=>$id])->row();
		force_download('views/sk/'.$data->file_sk,NULL);
	}
}

//Cetak SK


