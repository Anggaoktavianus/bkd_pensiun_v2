<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		// not_login();
        $this->load->model('M_login');
	    $this->load->library('form_validation');
		
	}


    public function index() 
    {
        is_login();
        $this->load->view('admin/login');
       
    }

    public function action()
    {
		$nip = $this->input->post('nip', TRUE);
        	$password = $this->input->post('password', TRUE);
		//ini untuk dummy yg di cek cm NIP nya aja
		$cek = $this->M_login->cekLoginDummy($nip);
		// $cek = $this->M_login->cekLoginBlast($nip);

		// ini yang asli
		// $cek = $this->M_login->cek_login_sinaga($nip, $password);
		
		if($cek != null){

			$dataPns = getDataMastfip($cek->nip);
 
			$A_01   = $dataPns->A_01;
			$B_09   = $dataPns->B_09;
			$I_5A   = $dataPns->I_5A;
			$B_02B  = $dataPns->B_02B;
			$kolok = $dataPns->A_01.$dataPns->A_02.$dataPns->A_03.$dataPns->A_04.$dataPns->A_05;
			$admin_bkd = 'B200303000'; //SUB BIDANG PEMINDAHAN DAN PEMBERHENTIAN

			$umpeg = cari_kode_umpeg($A_01);

			if($kolok == $umpeg){
				$role_pensiun = 2; //Umpeg OPD
			}else if($kolok == $admin_bkd){
				$role_pensiun = 1; //Admin BKD SUB BIDANG PEMINDAHAN DAN PEMBERHENTIAN
			}else{
				$role_pensiun = 3; //Umum
			}

			$session = array(
				'id'    => $cek->id,
				'A_01'  => $A_01,
				'B_02B'	=> $B_02B,
				'role'  => $cek->role,
				'kolok' => $kolok,
				'role_pensiun' => $role_pensiun
			);
			$this->session->set_userdata($session);
			$messge = array(
				'status'    => 'success',
				'type'      => 'success',
				'message'   => 'Selamat Datang Kembali di Aplikasi Pensiun'
			);
			$this->session->set_flashdata($messge);
			redirect('dashboard');
		} else {
			// pass salah
			// echo 'salah';
			$messge = array(
				'status'    => 'error',
				'type'      => 'danger',
				'message'   => 'periksa kembali username dan password anda'
			);

			$this->session->set_flashdata($messge);
			redirect('login');
		}
	}

	public function login_proses() 
	{
		
        $this->form_validation->set_rules('nip', 'NIP', 'trim|required|min_length[3]|max_length[45]');
        // $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_length[12]');
	$nip = $this->input->post('nip', TRUE);
	$blast_bkd = $this->input->post('diusulkan_bkd', TRUE);
        $password = $this->input->post('password', TRUE);
	$cek = $this->M_login->cekLoginDummy($nip);
	$blast = $this->M_login->cekLoginBlast($blast_bkd);
        if ($this->form_validation->run() == TRUE) {
                
            if($cek != null) { //cek email terdaftar
            
                if($cek != null) {
                    //    $this->input->post('alamat',$almt->alamat);
                        
                      	$A_01   = getDataMastfip($cek->nip)->A_01;
			$B_09   = getDataMastfip($cek->nip)->B_09;
			$I_5A   = getDataMastfip($cek->nip)->I_5A;
			$B_02B  = getDataMastfip($cek->nip)->B_02B;

			$session = array(
				'id'    => $cek->id,
				'A_01'  => $A_01,
				'B_02B'	=> $B_02B,
				'role'  => $cek->role,
			);
			$this->session->set_userdata($session);
			$messge = array(
				'status'    => 'success',
				'type'      => 'success',
				'message'   => 'Selamat Datang Kembali di Aplikasi Pensiun'
			);
			$this->session->set_flashdata($messge);
			redirect('dashboard');

                } else if ($blast = null ) {
				$A_01   = getDataMastfip($cek->nip)->A_01;
			$B_09   = getDataMastfip($cek->nip)->B_09;
			$I_5A   = getDataMastfip($cek->nip)->I_5A;
			$B_02B  = getDataMastfip($cek->nip)->B_02B;

			$session = array(
				'id'    => $blast->id,
				'A_01'  => $A_01,
				'B_02B'	=> $B_02B,
				'role'  => $blast->role,
				'diusulkan_bkd' => $blast->diusulkan_bkd,
				'diusulkan_skpd' => $blast->diusulkan_skpd,
			);
			$this->session->set_userdata($session);
			$messge = array(
				'status'    => 'success',
				'type'      => 'success',
				'message'   => 'Selamat Datang Kembali di Aplikasi Pensiun'
			);
			$this->session->set_flashdata($messge);
			$this->load->view('admin/validate');
		} else{
                   $messge = array(
				'status'    => 'error',
				'type'      => 'danger',
				'message'   => 'periksa kembali username dan password anda'
			);

			$this->session->set_flashdata($messge);
			redirect('login');
                }
                

            } else { 
                // jika status belum aktif!
                if($blast = null) {
                    //    $this->input->post('alamat',$almt->alamat);
                        
                      	$A_01   = getDataMastfip($cek->nip)->A_01;
			$B_09   = getDataMastfip($cek->nip)->B_09;
			$I_5A   = getDataMastfip($cek->nip)->I_5A;
			$B_02B  = getDataMastfip($cek->nip)->B_02B;

			$session = array(
				'id'    => $blast->id,
				'A_01'  => $A_01,
				'B_02B'	=> $B_02B,
				'role'  => $blast->role,
				'diusulkan_bkd' => $blast->diusulkan_bkd,
				'diusulkan_skpd' => $blast->diusulkan_skpd,
			);
			$this->session->set_userdata($session);
			$messge = array(
				'status'    => 'success',
				'type'      => 'success',
				'message'   => 'Selamat Datang Kembali di Aplikasi Pensiun'
			);
			$this->session->set_flashdata($messge);
			$this->load->view('admin/validate');

                } else{
                   $messge = array(
				'status'    => 'error',
				'type'      => 'danger',
				'message'   => 'periksa kembali username dan password anda'
			);

			$this->session->set_flashdata($messge);
			redirect('login');
                }
            }

        } else { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('pesan', 'error');
            redirect('login/','refresh');
        }

	}

    public function logout(){
        if(isset($_SESSION['B_02B'])) {

			$array_items = array(
				'id',
				'A_01',
				'B_02B',
				'role',
				'kembali_url'
			);
			$this->session->unset_userdata($array_items);

			redirect('/');
	} else {
			redirect('/');
	}
    }
    

    

    
}
