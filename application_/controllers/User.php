<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('M_user');
		$this->load->library('form_validation');

        }

	public function index()
		{	
			$row  = $this->M_user->get();	
			$data = array(
				'row' => $row,
			);
			$this->template->load('template', 'admin/user/v_user', $data);
		}

	public function add_index()
	{
		$kabkota = $this->M_user->get_kab();	
		$data = array(
			'kabkota' => $kabkota,
		);
		$this->template->load('template', 'admin/user/add_user', $data);
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|is_unique[pensiun_admin.email]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules(
		'password2',
		'Konfirmasi password',
		'trim|required|matches[password]',
		array('matches' => '{field} tidak sesuai dengan password')
		);
		$this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->M_user->get_kab();
		
		$data = array(
			'kabkota' => $kabkota,
			
		);
		$this->template->load('template', 'admin/user/add_user', $data);
		} else {
		 if($this->M_user->m_register()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('user','refresh');
		}
		echo "<script>window.location ='" . site_url('user') . "' ; </script>";
		}
	}

	public function edit_index()
	{
		$kabkota = $this->M_user->get_kab();
		$role = $this->M_user->get_role();
		$kabkota = $this->M_user->get_kab();	
		$data = array(
			'kabkota' => $kabkota,
			'role' => $role,
		);
		$this->template->load('template', 'admin/user/edit_user', $data);
		
	}
	public function edit($id)
	{

		$this->form_validation->set_rules('email', 'email', 'min_length[3]|callback_email_check');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		if ($this->input->post('password')) {
		$this->form_validation->set_rules('password', 'Password', 'min_length[5]');
		$this->form_validation->set_rules(
			'password2',
			'Konfirmasi password',
			'matches[password]',
			array('matches' => '{field} tidak sesuai dengan password')
		);
		}
		if ($this->input->post('password2')) {
		$this->form_validation->set_rules(
			'password2',
			'Konfirmasi password',
			'matches[password]',
			array('matches' => '{field} tidak sesuai dengan password')
		);
		}
		$this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->M_user->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->M_user->get_kab();
				$role = $this->M_user->get_role();
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'role' => $role,

				);
		
				$this->template->load('template', 'admin/user/edit_user', $data);
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}

            	// redirect('user/add_index');

            # code...
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->M_user->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data user.');
           	redirect('user','refresh');
		}
		echo "<script>window.location ='" . site_url('user') . "' ; </script>";
		}
	}
	 

	public function profile()
	{	$id = $this->input->post('id');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
		$kabkota = $this->M_user->get_kab();
		$query = $this->M_user->getalls($id);	
		$row = $query->row();	
		$user = $this->db->get_where('pensiun_admin',['email' => $this->session->userdata('email')])->row_array(); 	
		$data = array(
			'kabkota' => $kabkota,
			'user' => $user,
			'row' => $row,
		);

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template', 'admin/profile', $data);
		}else {
			$name 		= $this->input->post('nama');
			$email 		= $this->input->post('email');
			$kabkota 	= $this->input->post('kabkota');
			$kecamatan 	= $this->input->post('kecamatan');
			$desa 		= $this->input->post('desa');

			$this->db->set('nama',$name);
			$this->db->set('kabkota_id',$kabkota);
			$this->db->set('kecamatan_id',$kecamatan);
			$this->db->set('kelurahan_id',$desa);
			// $this->db->set('email',$email);

			$this->db->where('email', $email);
			$this->db->update('pensiun_admin');

		 $this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data user.');
           		redirect('user/profile','refresh');
		}
		
	}

	public function changepassword()
	{
		// $this->form_validation->set_rules('email', 'email', 'min_length[3]|callback_email_check');
		$this->form_validation->set_rules('password', 'Old Password', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[5]|matches[newpassword2]');
		$this->form_validation->set_rules('newpassword2', 'Confirm New Password', 'trim|required|min_length[5]|matches[newpassword]');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
		
		
		$user = $this->db->get_where('pensiun_admin',['email' => $this->session->userdata('email')])->row_array(); 	
		$data = array(
			'user' => $user,
		);

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template', 'admin/profile', $data);
		}else {
			$passwordlama = $this->input->post('password');
			$passwordbaru = $this->input->post('newpassword');
			if (!password_verify($passwordlama,$user['password'])) {
				 $this->session->set_flashdata('info', 'danger');
          			 $this->session->set_flashdata('pesan', 'Password lama salah');
           			redirect('user/profile','refresh');
			}else {
				if ($passwordbaru == $passwordlama) {
				$this->session->set_flashdata('info', 'danger');
          			 $this->session->set_flashdata('pesan', 'Password baru tidak boleh sama dengan password lama');
           			redirect('user/profile','refresh');
				}else {
					$passwordhash = get_hash($passwordbaru);
					$this->db->set('password',$passwordhash);
					$this->db->where('email',$this->session->userdata('email'));
					$this->db->update('pensiun_admin');
					 $this->session->set_flashdata('info', 'success');
          			 $this->session->set_flashdata('pesan', 'Password berhasil diubah');
				   redirect('user/profile','refresh');
				}
			}
		}
		
	}
	

	function email_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM pensiun_admin WHERE email= '$post[email]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('email_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->M_user->del($id);

		$row = $this->M_user->get($id);

		if ($row) {
		$this->M_user->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('user'));
		}
	}

	public function reset($id)
	{
	
		$row = $this->M_user->reset($id);
		if ($row) {
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Reset Password Berhasil');
			redirect(site_url('user'));
		} else {
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Reset Password Gagal');
			redirect(site_url('user'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */