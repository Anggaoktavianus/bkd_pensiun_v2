<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_bup extends CI_Model
{

    public $dbeps = NULL;
    public $dbdefault = NULL;

    public function __construct(){
		parent::__construct();

        $this->dbdefault = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }
     
        

    public function get()
    {
       
        //  $sql =
        //     "SELECT a.*, b.nm as skpd 
        //     FROM MASTFIP08 a 
        //     INNER JOIN TABLOK08 b ON b.kd = a.A_01
        //     WHERE b.aktif = '1' AND a.I_JB NOT IN ('LOKASI PENSIUN') 
        //     AND a.A_01 IS NOT NULL ORDER BY a.A_01 ASC
        //     LIMIT 100
				
        //     ";
            $sql =
            "SELECT a.*, b.nm as skpd, c.B_02B as np
            FROM MASTFIP08 a 
            INNER JOIN TABLOK08 b ON b.kd = a.A_01
            LEFT JOIN pensiun_pengajuan c ON c.B_02B = a.B_02B
            WHERE b.aktif = '1' AND a.I_JB NOT IN ('LOKASI PENSIUN') 
            AND a.A_01 IS NOT NULL ORDER BY a.A_01 ASC
            LIMIT 100
				
            ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        // # code...
        
    }

    public function get_usul()
    {
         $sql =
            "SELECT a.*, b.B_03 as nama, c.nm as nmskpd
            FROM pensiun_pengajuan a
            INNER JOIN MASTFIP08 b ON b.B_02B = a.B_02B 
            INNER JOIN TABLOK08 c ON c.kd = b.A_01 
            WHERE a.deleted_at IS NULL ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getall($id = null)
    {
        $this->db->select('a.*, b.B_03 as nama, b.B_05 as tl, c.nm as nmskpd, b.B_12 as alamat');
        $this->db->from('pensiun_pengajuan a');
        $this->db->join('MASTFIP08 b', 'b.B_02B = a.B_02B ' );
        $this->db->join('TABLOK08 c', 'c.kd = b.A_01 ' );
        // $this->db->where('deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.B_02B', $id );
            
        }
        $query = $this->db->get();
        return $query;
    }

    public function get_skpd()
    {
       
         $sql =
            "SELECT a.*
            FROM  TABLOK08 a 
            WHERE a.aktif = '1' AND a.kd IS NOT NULL ORDER BY a.kd ASC";

        $query = $this->dbeps->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }


    public function get_ahliwaris()
    {
        //     ";
            $sql =
            "SELECT * FROM pensiun_ahliwaris ORDER BY sort ASC
				
            ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        // # code...
        
        
    }
    public function get_suami_istri($id)
    {
            
        $sql = "SELECT a.B_02, b.B_03 as nama,b.B_02, b.B_05 as tl, c.*,TIMESTAMPDIFF(YEAR, c.KF_05, CURDATE()) as umur, d.ket as status
        FROM pensiun_pengajuan a
        INNER JOIN MASTFIP08 b ON b.B_02B = a.B_02B 
        LEFT JOIN MASTKEL1 c ON c.KF_01 = b.B_02
        LEFT JOIN pensiun_sutri d ON d.id = c.status
        WHERE b.B_02B = '$id' AND c.KF_02 = 1 ORDER BY c.KF_03 ASC";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
    public function get_anak($id)
    {
       
            
        $sql = "SELECT  a.B_02, b.B_03 as nama,b.B_02, b.B_05 as tl, c.*,TIMESTAMPDIFF(YEAR, c.KF_05, CURDATE()) as umur, d.ket as status
        FROM pensiun_pengajuan a
        INNER JOIN MASTFIP08 b ON b.B_02B = a.B_02B 
        LEFT JOIN MASTKEL1 c ON c.KF_01 = b.B_02
        LEFT JOIN pensiun_anak d ON d.id = c.status
        WHERE b.B_02B = '$id' AND c.KF_02 = 2 ORDER BY c.KF_03 ASC";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function updatesort_anak( $data = array())
    {
        
        $i=1;
		foreach($data as $k => $v){
		$sql = "Update MASTKEL1 SET KF_03=".$i." WHERE ID=".$v;
		$query = $this->db->query($sql);
			$i++;
		}
        
        
    }

    public function updatesort_sutri( $data = array())
    {
        
        $i=1;
		foreach($data as $k => $v){
		$sql = "Update MASTKEL1 SET KF_03=".$i." WHERE ID=".$v;
		$query = $this->db->query($sql);
			$i++;
		}
        
        
    }

    // Update BUP pada menu BKD
    public function edit() {
        $id = $this->input->post('B_02B', true);
         
        $data = array(
                      'nama' =>$this->input->post('B_03'),
                    //   'email'=>$this->input->post('email'),
                    //   'kabkota_id'=>$this->input->post('kabkota_id'),
                    //   'kecamatan_id'=>$this->input->post('kecamatan_id'),
                    //   'kelurahan_id'=>$this->input->post('kelurahan_id'),
                    //    'role_id'=>$this->input->post('role_id'),
                    //    'status'=>$this->input->post('status'),
                       'update_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('B_02B', $id); 
    
        $this->db->update('pensiun_pengajuan',$data); 

	}

    // status anak

    public function get_status_anak()
	{
      $sql =
            	" SELECT * FROM `pensiun_anak`";

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
		$data = $query->result();
		} else {
		$data = array();
		}

		return $data;
	}
    // Update anak pada menu BKD
    public function edit_anak() {
        $id = $this->input->post('ID', true);
         
        $data = array(
                      'keterangan' =>$this->input->post('keterangan'),
                    
                       'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('ID', $id); 
    
        $this->db->update('MASTKEL1',$data); 

	}
    

    public function get_provinsi()
	{
      $sql =
            	" SELECT * FROM `pensiun_provinsi`";

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
		$data = $query->result();
		} else {
		$data = array();
		}

		return $data;
	}

	public function get_kota($id_provinsi)
	{
       // Tampilkan semua data kota berdasarkan id provinsi
        $result = $this->db->query("SELECT * FROM pensiun_kota WHERE province_id ='$id_provinsi' ORDER BY name ASC ")->result();

        return $result;
    
	}
	
	public function get_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM pensiun_kecamatan WHERE regency_id ='$id_kota' ORDER BY name ASC ")->result();

        return $result;
	}

	public function get_kelurahan($id_kelurahan)
	{
		$result = $this->db->query("SELECT * FROM pensiun_desa WHERE district_id ='$id_kelurahan' ORDER BY name ASC ")->result();

        return $result;
	}

    

}
