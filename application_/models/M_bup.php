<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_bup extends CI_Model
{  

    public $dbeps = NULL;
    public $dbdefault = NULL;

    public function __construct(){
		parent::__construct();

        $this->dbdefault = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
       
    }

    
     
    function get_ ()
        {         
            $this->db->select('eps.a.*, eps.b.nm as skpd, bkd_pensiun.c.B_02B as np, eps.d.NAMAY as pangkat ');
            $this->db->from('eps.MASTFIP08 a');
            $this->db->join('eps.TABLOK08 b', 'eps.b.kd = eps.a.A_01', 'inner');
            $this->db->join('bkd_pensiun.pensiun_pengajuan c', 'bkd_pensiun.c.B_02B = eps.a.B_02B','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.a.F_03','inner');
            $this->db->where('eps.b.aktif', '1');
            $this->db->order_by('eps.a.A_01', 'asc');
            $this->db->limit(100);
                
                
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        }

        //SELECT B_02B,
        // IF (IF ((YEAR(CURDATE())-YEAR(B_05)) - (RIGHT(CURDATE(),5)<RIGHT(B_05,5)) >= 60,65,IF ((YEAR(CURDATE())-YEAR(B_05)) - (RIGHT(CURDATE(),5)<RIGHT(B_05,5)) >= 58,60,58)) - (YEAR(CURDATE())-YEAR(B_05)) - (RIGHT(CURDATE(),5)<RIGHT(B_05,5)) = 1,1,0) maupens
        // FROM MASTFIP08 m
        // WHERE m.A_01 = 'B2';
    function get ($id)
        {
                  
            $this->db->select('eps.a.*, eps.b.nm as skpd, bkd_pensiun.c.B_02B as np,bkd_pensiun.f.A_01 kdskpd,bkd_pensiun.f.waktu_skpd wskpd,bkd_pensiun.c.due_date_skpd, eps.d.NAMAY as pangkat,bkd_pensiun.c.diusulkan_bkd, IF (IF ((YEAR(CURDATE())-YEAR(eps.a.B_05)) - (RIGHT(CURDATE(),5)<RIGHT(eps.a.B_05,5)) >= 60,65,IF ((YEAR(CURDATE())-YEAR(eps.a.B_05)) - (RIGHT(CURDATE(),5)<RIGHT(eps.a.B_05,5)) >= 58,60,58)) - (YEAR(CURDATE())-YEAR(eps.a.B_05)) - (RIGHT(CURDATE(),5)<RIGHT(eps.a.B_05,5)) = 1,1,0) maupens ');
            $this->db->from('eps.MASTFIP08 a');
            $this->db->join('eps.TABLOK08 b', 'eps.b.kd = eps.a.A_01', 'inner');
            $this->db->join('bkd_pensiun.pensiun_pengajuan c', 'bkd_pensiun.c.B_02B = eps.a.B_02B','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.a.F_03','inner');
            $this->db->join('bkd_pensiun.pensiun_waktu f', 'bkd_pensiun.f.A_01 = eps.a.A_01','inner');
            $this->db->where('eps.b.aktif', '1');
            $this->db->where('eps.a.A_01', $id);
            // $this->db->where('bkd_pensiun.c.deleted_at', null);
            $this->db->order_by('eps.a.A_01', 'asc');
            return $this->db->get();
                
        // $query = $this->db->get();
        // if ($query->num_rows() > 0) {
        //     $data = $query->result();
        // } else {
        //     $data = array();
        // }

        // return $data;
        }
       

        // Get usulan dari bkd
        function get_skpd ()
        {         
            $this->db->select('eps.a.*, eps.b.nm as skpd, bkd_pensiun.c.B_02B as np, eps.d.NAMAY as pangkat ');
            $this->db->from('eps.MASTFIP08 a');
            $this->db->join('eps.TABLOK08 b', 'eps.b.kd = eps.a.A_01', 'inner');
            $this->db->join('bkd_pensiun.pensiun_pengajuan c', 'bkd_pensiun.c.B_02B = eps.a.B_02B','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.a.F_03','inner');
            $this->db->where('eps.b.aktif', '1');
            $this->db->order_by('eps.a.A_01', 'asc');
            $this->db->limit(100);
                
                
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        }
        

    public function get_usul()
        {
        
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'inner');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','inner');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','inner');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }

    public function get_usul_by_bkd()
        {
            
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.A_01 ',$this->session->userdata('A_01'));

                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }

     public function get_usul_by_id()
        {
        
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            $this->db->where('bkd_pensiun.a.A_01 ',$this->session->userdata('A_01'));

            return $this->db->get();
            
        }

    public function getall($id = null)
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.b.B_05 as tl,eps.b.B_04, eps.c.nm as nmskpd, eps.b.B_12 as alamat, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB, eps.b.B_03A, eps.b.B_03B, eps.b.B_04, eps.b.A_01, eps.b.B_02, eps.e.status, eps.e.KF_06,bkd_pensiun.g.ket, eps.e.KF_04 as namas, eps.e.KF_05 as tgl_lahir');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'inner');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','inner');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','inner');
            $this->db->join('eps.MASTKEL1 e', 'eps.e.KF_01 = eps.b.B_02','inner');
            $this->db->join('bkd_pensiun.pensiun_status_anak g', 'bkd_pensiun.g.id = eps.e.status','left');
            $this->db->join('bkd_pensiun.pensiun_status_sutri h', 'bkd_pensiun.h.id = eps.e.status','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->group_by('bkd_pensiun.a.B_02B');
            if ($id != null) {
                $this->db->where('bkd_pensiun.a.B_02B', $id );
                
            }
            $query = $this->db->get();
            return $query;
        }

    public function get_ahliwaris()
        {
                $sql =
                "SELECT bkd_pensiun.a.B_02, eps.b.B_03 as nama, eps.b.B_02, b.B_05 as tl, eps.c.*,TIMESTAMPDIFF(YEAR, c.KF_05, CURDATE()) as umur, bkd_pensiun.d.ket
            FROM bkd_pensiun.pensiun_pengajuan a
            INNER JOIN eps.MASTFIP08 b ON eps.b.B_02B = bkd_pensiun.a.B_02B 
            LEFT JOIN eps.MASTKEL1 c ON eps.c.KF_01 = eps.b.B_02
            LEFT JOIN bkd_pensiun.pensiun_status_anak d ON bkd_pensiun.d.id = eps.c.status
            LEFT JOIN bkd_pensiun.pensiun_status_sutri e ON bkd_pensiun.e.id = eps.c.status
            WHERE KF_01 = 197806032009011011  ORDER BY eps.c.KF_03 ASC ";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
            // # code...
            
            
        }

    public function get_suami_istri($id)
        {
                
            // $sql = "SELECT bkd_pensiun.a.B_02, eps.b.B_03 as nama, eps.b.B_02, b.B_05 as tl, eps.c.*,TIMESTAMPDIFF(YEAR, c.KF_05, CURDATE()) as umur, bkd_pensiun.d.ket
            // FROM bkd_pensiun.pensiun_pengajuan a
            // INNER JOIN eps.MASTFIP08 b ON eps.b.B_02B = bkd_pensiun.a.B_02B 
            // LEFT JOIN eps.MASTKEL1 c ON eps.c.KF_01 = eps.b.B_02
            // LEFT JOIN bkd_pensiun.pensiun_status_sutri d ON bkd_pensiun.d.id = eps.c.status
            // WHERE eps.b.B_02B = '$id' AND eps.c.KF_02 = 1 AND eps.c.deleted_at IS NULL ORDER BY eps.c.KF_03 ASC";
            $sql = "SELECT bkd_pensiun.a.B_02, eps.b.B_03 as nama, eps.b.B_02, b.B_05 as tl, bkd_pensiun.c.*,TIMESTAMPDIFF(YEAR, bkd_pensiun.c.KF_05, CURDATE()) as umur, bkd_pensiun.d.ket
            FROM bkd_pensiun.pensiun_pengajuan a
            INNER JOIN eps.MASTFIP08 b ON eps.b.B_02B = bkd_pensiun.a.B_02B 
            LEFT JOIN bkd_pensiun.MASTKEL1 c ON bkd_pensiun.c.KF_01 = eps.b.B_02
            LEFT JOIN bkd_pensiun.pensiun_status_sutri d ON bkd_pensiun.d.id = bkd_pensiun.c.status
            WHERE eps.b.B_02B = '$id' AND bkd_pensiun.c.KF_02 = 1 AND bkd_pensiun.c.deleted_at IS NULL ORDER BY bkd_pensiun.c.KF_03 ASC";


            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }
    public function get_anak($id)
        {
        
                
            // $sql = "SELECT bkd_pensiun.a.B_02, eps.b.B_03 as nama, eps.b.B_02, b.B_05 as tl, eps.c.*,TIMESTAMPDIFF(YEAR, c.KF_05, CURDATE()) as umur, bkd_pensiun.d.ket
            // FROM bkd_pensiun.pensiun_pengajuan a
            // INNER JOIN eps.MASTFIP08 b ON eps.b.B_02B = bkd_pensiun.a.B_02B 
            // LEFT JOIN eps.MASTKEL1 c ON eps.c.KF_01 = eps.b.B_02
            // LEFT JOIN bkd_pensiun.pensiun_status_anak d ON bkd_pensiun.d.id = eps.c.status
            // WHERE eps.b.B_02B = '$id' AND eps.c.KF_02 = 2 AND eps.c.deleted_at IS NULL ORDER BY eps.c.KF_03 ASC";\

            $sql = "SELECT bkd_pensiun.a.B_02, eps.b.B_03 as nama, eps.b.B_02, b.B_05 as tl, bkd_pensiun.c.*,TIMESTAMPDIFF(YEAR, bkd_pensiun.c.KF_05, CURDATE()) as umur, bkd_pensiun.d.ket
            FROM bkd_pensiun.pensiun_pengajuan a
            INNER JOIN eps.MASTFIP08 b ON eps.b.B_02B = bkd_pensiun.a.B_02B 
            LEFT JOIN bkd_pensiun.MASTKEL1 c ON bkd_pensiun.c.KF_01 = eps.b.B_02
            LEFT JOIN bkd_pensiun.pensiun_status_sutri d ON bkd_pensiun.d.id = bkd_pensiun.c.status
            WHERE eps.b.B_02B = '$id' AND bkd_pensiun.c.KF_02 = 2 AND bkd_pensiun.c.deleted_at IS NULL ORDER BY bkd_pensiun.c.KF_03 ASC";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }

    public function updatesort_anak( $data = array())
        {
            
            $i=1;
            foreach($data as $k => $v){
            $sql = "Update MASTKEL1 SET KF_03=".$i." WHERE ID=".$v;
            $query = $this->dbeps->query($sql);
                $i++;
            }
            
            
        }

    public function updatesort_sutri( $data = array())
        {
            
            $i=1;
            foreach($data as $k => $v){
            $sql = "Update MASTKEL1 SET KF_03=".$i." WHERE ID=".$v;
            $query = $this->dbeps->query($sql);
                $i++;
            }
            
            
        }

    // Update BUP pada menu BKD
    public function edit() {
        $id = $this->input->post('B_02B', true);
         
        $data = array(
            'A_01'          =>$this->input->post('skpd'),
            'B_02'          =>$this->input->post('nip_lama'),
            'B_03A'         =>$this->input->post('gelar_dpn'),
            'B_03'          =>$this->input->post('nama'),
            'B_03B'         =>$this->input->post('gelar_blkg'),
            'B_04'          =>$this->input->post('t_lahir'),
            'B_05'          =>$this->input->post('tl'),
            'B_12'          =>$this->input->post('alamat'),
            // 'RT'            =>$this->input->post('rt'),
            'keterangan'    =>$this->input->post('keterangan'),
            'provinsi_id'   =>$this->input->post('provinsi_id'),
            'kabkota_id'    =>$this->input->post('kabkota_id'),
            'kecamatan_id'  =>$this->input->post('kecamatan_id'),
            'kelurahan_id'  =>$this->input->post('kelurahan_id'),
            'updated_at'    =>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('B_02B', $id); 
    
        $this->db->update('pensiun_pengajuan',$data); 

	}
    
    // Update anak pada menu BKD

    public function get_provinsi()
        {
        $sql =
                    " SELECT * FROM `pensiun_provinsi`";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
            $data = $query->result();
            } else {
            $data = array();
            }

            return $data;
        }

	public function get_kota($id_provinsi)
	    {
        // Tampilkan semua data kota berdasarkan id provinsi
            $result = $this->db->query("SELECT * FROM pensiun_kota WHERE province_id ='$id_provinsi' ORDER BY name ASC ")->result();

            return $result;
        
        }
	
	public function get_kecamatan($id_kota)
        {
            $result = $this->db->query("SELECT * FROM pensiun_kecamatan WHERE regency_id ='$id_kota' ORDER BY name ASC ")->result();

            return $result;
        }

	public function get_kelurahan($id_kelurahan)
        {
            $result = $this->db->query("SELECT * FROM pensiun_desa WHERE district_id ='$id_kelurahan' ORDER BY name ASC ")->result();

            return $result;
        }

    // MENGAMBIL DATA KAB-DESA

    public function get_kab()
        {
            
            $sql =
                "SELECT * FROM `pensiun_kota` WHERE `province_id` = '33'";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }
    
    public function get_keca()
        {
            
            $sql =
                " SELECT a.*, c.province_id as provinsi 
                FROM pensiun_kecamatan a 
                INNER JOIN pensiun_kota c ON c.id = a.regency_id
                WHERE c.province_id = '33'";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
            # code...
        }

    public function get_kelu()
        {
            $sql =
                " SELECT a.*, b.regency_id as kel, c.province_id as provinsi 
                FROM pensiun_desa a 
                INNER JOIN pensiun_kecamatan b ON b.id = a.district_id
                INNER JOIN pensiun_kota c ON c.id = b.regency_id
                WHERE c.province_id = '33'";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
            # code...
        }

    // STATUS ANAK ISTRI
    public function get_status_anak()
        {
            $sql = "SELECT * FROM pensiun_status_anak ";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }
    // STATUS ANAK ISTRI
    public function get_status_sutri()
        {
            $sql = "SELECT * FROM pensiun_status_sutri ";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }

    // FILTER SKPD
    public function get_filter_skpd()
        {
            $sql = "SELECT * FROM TABLOKB08 WHERE aktif = 1 GROUP BY A_01 ";
            $query = $this->dbeps->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }

    public function get_setting()
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.A_01,eps.b.NALOK as nskpd');
            $this->db->from('bkd_pensiun.pensiun_waktu a');
            $this->db->join('eps.TABLOKB08 b', 'eps.b.A_01 = bkd_pensiun.a.A_01','left');
            $this->db->where('eps.b.aktif', '1');
            $this->db->group_by('eps.b.A_01');
            
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }

    public function get_setting_skpd()
        {
        $id = $this->session->userdata('A_01');
                
            $sql = "SELECT bkd_pensiun.a.*, eps.b.A_01 , eps.b.NALOK as nskpd
            FROM bkd_pensiun.pensiun_waktu a
            INNER JOIN eps.TABLOKB08 b ON eps.b.A_01 = bkd_pensiun.a.A_01 
            WHERE bkd_pensiun.a.A_01 = '$id' AND eps.b.aktif = 1 GROUP BY eps.b.A_01";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;
        }

    public function get_list_skpd($skpd)//untuk kunci nominatif pada admin bkd
        {
        // Tampilkan semua data kota berdasarkan id provinsi
            $result = $this->db->query("SELECT bkd_pensiun.a.* , eps.b.NALOK as nmskpd
            FROM bkd_pensiun.pensiun_waktu a 
            INNER JOIN eps.TABLOKB08 b ON eps.b.A_01 = bkd_pensiun.a.A_01
            WHERE bkd_pensiun.a.A_01 ='$skpd' GROUP BY A_01 
            ")->result();
            

            return $result;
        
        }
    
    public function get_waktu_skpd($id = null)
        {
            $this->db->select('a.*');
            $this->db->from('pensiun_waktu a');
            // $this->db->where('deleted_at IS NULL');TABPKT
            if ($id != null) {
                $this->db->where('a.waktu_skpd', $id );
                
            }
            $query = $this->db->get();
            return $query;
        }

    public function get_id_surat()
        {
            $sql = "SELECT * FROM pensiun_no_surat_pengantar ORDER BY id DESC LIMIT 1 ";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }

     public function insert_surat_pengantar()
        {
            $data = array('nomer_surat' =>$this->input->post('nomer_surat'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'created_by'=>$this->session->userdata('B_02B'),
                        
                    );

            return $this->db->insert('pensiun_no_surat_pengantar',$data);

        }

     public function get_status_pengajuan($id)
        {
            $sql = "SELECT a.*, b.`B_02B`
            FROM pensiun_pengajuan_acc a 
            INNER JOIN pensiun_pengajuan b ON b.B_02B = a.id_pengajuan 
            WHERE b.B_02B=$id ";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }

    public function get_waktu_pns_by_bkd()
        {
            $id = $this->session->userdata('A_01');
            $sql = "SELECT a.*
            FROM pensiun_waktu a 
            WHERE a.A_01='$id' ";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }

            return $data;

        }

    public function get_ajuan_skpd()
        {
        
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->join('bkd_pensiun.pensiun_pengajuan_acc f', 'bkd_pensiun.f.id_pengajuan = bkd_pensiun.a.B_02B  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            $this->db->where('bkd_pensiun.a.last_position ',2);
            $this->db->where('bkd_pensiun.f.status_acc ',0);

            return $this->db->get();
            
        }

     public function edit_ajuan_skpd() {
        $id = $this->input->post('id_pengajuan', true);
         
        $data = array(
            'status_acc'          =>1,
            'status_ajuan'          =>3
            // 'B_02'         =>$this->input->post('nip_lama'),
            // 'B_03A'         =>$this->input->post('gelar_dpn'),
            // 'B_03'          =>$this->input->post('nama'),
            // 'B_03B'         =>$this->input->post('gelar_blkg'),
            // 'B_04'          =>$this->input->post('t_lahir'),
            // 'B_05'          =>$this->input->post('tl'),
            // 'B_12'          =>$this->input->post('alamat'),
            // // 'RT'            =>$this->input->post('rt'),
            // // 'RW'            =>$this->input->post('rw'),
            // 'provinsi_id'   =>$this->input->post('provinsi_id'),
            // 'kabkota_id'    =>$this->input->post('kabkota_id'),
            // 'kecamatan_id'  =>$this->input->post('kecamatan_id'),
            // 'kelurahan_id'  =>$this->input->post('kelurahan_id'),
            // 'updated_at'    =>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id_pengajuan', $id); 
    
        $this->db->update('pensiun_pengajuan_acc',$data); 

	}

    public function get_bup_acc()
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat, bkd_pensiun.f.status_acc');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_pengajuan_acc f', 'bkd_pensiun.f.id_pengajuan = bkd_pensiun.a.B_02B','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            // $this->db->where('bkd_pensiun.a.last_position ',2);
            $this->db->where('bkd_pensiun.f.status_acc',1);
             $this->db->where('bkd_pensiun.a.last_position',2);

            return $this->db->get();
            
        }

    public function get_bup_bkn()
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat, bkd_pensiun.f.status_acc');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_pengajuan_acc f', 'bkd_pensiun.f.id_pengajuan = bkd_pensiun.a.B_02B','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            // $this->db->where('bkd_pensiun.a.last_position ',2);
            $this->db->where('bkd_pensiun.f.status_acc',2);
             $this->db->where('bkd_pensiun.a.last_position',3);

            return $this->db->get();
            
        }

         public function get_bkn_acc()
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat, bkd_pensiun.f.status_acc');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_pengajuan_acc f', 'bkd_pensiun.f.id_pengajuan = bkd_pensiun.a.B_02B','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            // $this->db->where('bkd_pensiun.a.last_position ',2);
            $this->db->where('bkd_pensiun.f.status_acc',3);
             $this->db->where('bkd_pensiun.a.last_position',3);

            return $this->db->get();
            
        }

         public function get_bkn_tolak()
        {
            $this->db->select('bkd_pensiun.a.*, eps.b.B_03 as nama, eps.c.nm as nmskpd, eps.b.F_TMT, eps.b.F_PK, eps.d.NAMAY as pangkat, eps.b.I_JB ,bkd_pensiun.e.nomer_surat, bkd_pensiun.f.status_acc');
            $this->db->from('bkd_pensiun.pensiun_pengajuan a');
            $this->db->join('eps.MASTFIP08 b', 'eps.b.B_02B = bkd_pensiun.a.B_02B', 'left');
            $this->db->join('eps.TABLOK08 c', 'eps.c.kd = eps.b.A_01','left');
            $this->db->join('eps.TABPKT d', 'eps.d.KODE = eps.b.F_03','left');
            $this->db->join('bkd_pensiun.pensiun_pengajuan_acc f', 'bkd_pensiun.f.id_pengajuan = bkd_pensiun.a.B_02B','left');
            $this->db->join('bkd_pensiun.pensiun_no_surat_pengantar e', 'bkd_pensiun.e.id = bkd_pensiun.a.id_no_surat_pengantar_skpd  ','left');
            $this->db->where('bkd_pensiun.a.deleted_at ', null);
            $this->db->where('bkd_pensiun.a.diusulkan_skpd IS NOT NULL');
            // $this->db->where('bkd_pensiun.a.last_position ',2);
            $this->db->where('bkd_pensiun.f.status_acc',4);
             $this->db->where('bkd_pensiun.a.last_position',3);
             $this->db->group_by('bkd_pensiun.a.B_02B','DESC');

            return $this->db->get();
            
        }
    function simpan_upload($file,$nosk){
        // $id = $this->M_bup->getall($id);
        // $id = $this->input->post('B_02B');
        // $hasil=$this->db->query("UPDATE pensiun_pengajuan
        // SET file_sk = '$file', no_sk = '$nosk', ...
        // WHERE B_02B = '$id' AND deleted_at IS NULL ");
        // return $hasil;

         $id = $this->input->post('B_02B', true);
         
        $data = array(
            'file_sk'          =>$file,
            'no_sk'          =>$nosk,
            
                      
                    );
         $this->db->where('B_02B', $id); 
    
        $this->db->update('pensiun_pengajuan',$data); 
    }



}
