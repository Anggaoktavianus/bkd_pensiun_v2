<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{

    public $dbeps = NULL;
    public $dbdefault = NULL;

    public function __construct(){
		parent::__construct();

        $this->dbdefault = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function cek_login_sinaga($nip, $password) {

        $md5_pass = md5($password);

        $sql = 
        "SELECT *
        FROM USER_NEW
        WHERE username = ?
        AND password = ?";

        $query_cek = $this->dbeps->query($sql, array($nip, $md5_pass));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();

        } else {
            $data = null;
        }

        return $data;
    }

    public function cekLoginDummy($nip){
        $sql = 
        "SELECT *
        FROM USER_NEW
        WHERE username = ?";

        $query_cek = $this->dbeps->query($sql, array($nip));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function cekLoginBlast($nip){
        $sql = 
        "SELECT eps.a.*, bkd_pensiun.b.*
        FROM eps.USER_NEW a 
        LEFT JOIN bkd_pensiun.pensiun_pengajuan b ON bkd_pensiun.b.B_02B = eps.a.nip
        WHERE eps.a.username = ? AND eps.a.username NOT IN  (bkd_pensiun.b.B_02B)";

        $query_cek = $this->dbeps->query($sql, array($nip));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }
}