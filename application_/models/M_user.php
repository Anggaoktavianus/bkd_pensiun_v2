<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

    
    public function getall($id = null)
    {
        $this->db->from('pensiun_admin');
        
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
    public function get($id = null)
    {
        $this->db->select('pensiun_admin.*, pensiun_role.nama as role');
        $this->db->from('pensiun_admin');
        // $this->db->join('pensiun_kota', 'pensiun_kota.id = pensiun_admin.kabkota_id ' );
        $this->db->join('pensiun_role', 'pensiun_role.id = pensiun_admin.role_id ' );
        $this->db->where('deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('pensiun_admin.id', $id );
            
        }
        $query = $this->db->get();
        return $query;
    }

    public function get_kab()
    {
        // $this->db->select('pensiun_admin.*, pensiun_kota.name as kabkota');
        // 
         $sql =
            " SELECT * FROM `pensiun_kota` WHERE `province_id` = '33' ORDER BY name ASC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_kec($id_kota)
    {
       // Tampilkan semua data kota berdasarkan id provinsi
        $result = $this->db->query("SELECT * FROM pensiun_kecamatan WHERE regency_id ='$id_kota' ORDER BY name ASC ")->result();

        return $result;
    }

    public function get_kel($id_kec)
    {
       $result = $this->db->query("SELECT * FROM pensiun_desa WHERE district_id ='$id_kec' ORDER BY name ASC ")->result();

        return $result;

    }

   

    public function get_role()
    {
        // $this->db->select('pensiun_admin.*, pensiun_kota.name as kabkota');

        // 
         $sql =
            " SELECT * FROM `pensiun_role` WHERE `id` ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
   
    public function m_register() {

        $data = array('nama' =>$this->input->post('nama'),
                      'email'=>$this->input->post('email'),
                      'kabkota_id'=>$this->input->post('kabkota_id'),
                      'kecamatan_id'=>$this->input->post('kecamatan_id'),
                      'kelurahan_id'=>$this->input->post('kelurahan_id'),
                       'role_id'=>$this->input->post('role_id'),
                       'status'=>$this->input->post('status'),
                       'created_at'=>date('Y-m-d H:i:s'),
                      'password'=>get_hash($this->input->post('password')));
                       

        return $this->db->insert('pensiun_admin',$data);

	}

  

    public function m_cek_mail() {

     return $this->db->get_where('pensiun_admin',array('email' => $this->input->post('email'),'status' => 1));

    }
     
    public function m_cek_mails() {

     return $this->db->get_where('pensiun_admin',array('email' => $this->input->post('email'),'status' => 0));

    }

    public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        $this->db->update('pensiun_admin',$data);
    }

    function reset($id)
    {
        $pass = get_hash('12345');
        
        $data = array(
            'password' => $pass
            );
        $this->db->where('id', $id);
        $reset = $this->db->update('pensiun_admin', $data);
        if($reset){
            return true;
        }else{
            return false;
        }
    }

   
    

    public function update($data, $id)
    {
         $this->db->form('pensiun_admin');
        $this->db->where('id', $id);
        $this->db->update('pensiun_admin', $data);
        return $this->db->affected_rows();
	}

    
    public function getalls($id = null)
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('pensiun_admin a');
        $this->db->join('pensiun_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('pensiun_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('pensiun_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.id', $id );
             
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

}
