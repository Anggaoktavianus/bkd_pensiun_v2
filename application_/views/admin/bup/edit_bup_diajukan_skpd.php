<style>
  .main-table {
  /* border-collapse: collapse; */
  cursor: pointer;
  /* margin: 20px; */
  }
  tr.ui-sortable-handle > td:first-child{
    /* width: 50px; */
    /* background-color: lightgreen; */
  }

  .multipleRows tr > td {
    border: none;
    padding-left: 0px;
  } 
  td {
	cursor: pointer;
  }

  .editor{
  display: none;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>View Data Calon BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Calon BUP </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');
				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>	<!-- /.content -->
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <?php
                  $nipPns = $row->B_02B;
                  $urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/'.$nipPns;
                ?>
                <!-- <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/img_avatar.png')?>" alt="User profile picture">
                </div> -->
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="<?= $urlFoto?>" alt="User profile picture">
                </div>
                <h3 class="profile-username text-center"><?= $row->nama ?></h3>
                <p class="text-muted text-center"><?= $row->B_02B ?></p>
               <div class="card-body">
                 <strong><i class="fas fa-calendar-alt mr-1"></i> Tanggal Lahir</strong>
                <p class="text-muted"><?= $row->tl ?></p>
                <hr>
                <strong><i class="fas fa-users mr-1"></i> Unit Kerja</strong>
                <p class="text-muted">
                  <?= $row->nmskpd?>
                </p>
                <hr>
                <strong><i class="fas fa-book-reader mr-1"></i> Pangkat</strong>
                <p class="text-muted"><?= $row->pangkat ?>, <?= $row->F_PK ?> </p>
                <hr>
                <strong><i class="fas fa-award mr-1"></i> Jabatan</strong>
                <p class="text-muted"><?= $row->I_JB ?> </p>
                <hr>
              </div>
                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Data BUP</a></li>
                  <!-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="tab-pane" id="settings">
                    <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" >
                      <div class="row">
                        <div class="col-md-3">
                          <input type="hidden" name="B_02B" class="form-control"  value="<?= $row->B_02B ?>" >
                          <input type="hidden" name="skpd" class="form-control"  value="<?= $row->A_01 ?>" >
                          <input type="hidden" name="nip_lama" class="form-control"  value="<?= $row->B_02 ?>" >
                          <input type="hidden" name="gelar_dpn" class="form-control"  value="<?= $row->B_03A ?>" >
                          <input type="hidden" name="nama" class="form-control"  value="<?= $row->nama ?>" >
                          <input type="hidden" name="gelar_blkg" class="form-control"  value="<?= $row->B_03B ?>" >
                          <input type="hidden" name="t_lahir" class="form-control"  value="<?= $row->B_04 ?>" >
                          <input type="hidden" name="tl" class="form-control"  value="<?= $row->tl ?>" >
                          <div class="form-group">
                            <label>Provinsi</label>
                             <select name="provinsi_id" class="form-control not-dark" id="provinsi" required>
                               <option value="">--Select--</option>
                                <?php foreach ($provinsi as $key => $data) { ?>
                                <option value="<?= $data->id ?>" <?= $data->id == $row->provinsi_id ? "selected" : null ?>><?= $data->name ?></option>
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                          <!-- /.col -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kab/Kota</label>
                            <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                              <option value="">--Select--</option>
                              <?php foreach ($kabkota as $key => $data) { ?>
                              <option value="<?= $data->id?>" <?= $data->id == $row->kabkota_id ? "selected" : null ?>><?= $data->name ?></option>
                              <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>        
                        <!-- /.col -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kecamatan</label>
                            <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
                                <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>        
                        <!-- /.col -->
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Kelurahan </label>
                            <select name="kelurahan_id" class="form-control not-dark" id="kelurahan" >
												        <option value="">--Select--</option>
                                <?php foreach ($desa as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											      </select>
                          </div>
                          <!-- /.form-group -->
                        </div>  
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Alamat Setelah Pensiun</label>
                            <textarea name="alamat" class="form-control" rows="3" placeholder="Masukkan Alamat Lengkap ..."><?= $row->alamat ?></textarea>
                          </div>
                            <!-- /.form-group -->
                          <!-- /.form-group -->
                        </div> 
                      </div>
                      <!-- Data Ahli Waris -->
                      <!-- Data Ahli Waris -->
                      <h3 class="card-title"><strong> Data Ahli Waris </strong></h3><br>
                      <h3 class="card-title"><small>A.Data Suami/Isteri</small> </h3><br>
                      <!-- <a data-toggle="modal" data-target="#modal-add-sutri" class="btn btn-primary btn-circle btn-xs" data-popup="tooltip" data-placement="top"><i class="fas fa-plus">&nbsp;</i>Add Data</a> -->
                      <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Lahir</th>
                            <th>Tanggal Menikah</th>
                            <th>Umur</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody class="row_position">
                          	<?php
                            $sl = '1';
                            foreach($sutri as $data): ?>
                          <tr id="<?= $data->ID ?>" >
                            <td><?= $data->KF_03?></td>
                            <td><?= $data->KF_04?></td>
                            <td><?= $data->KF_05?></td>
                            <td><?= $data->KF_06?></td>
                            <td><?= $data->umur?></td>
                            <td><?= $data->keterangan?></td>
                            <td><span style="background-color: #FFFF00"> <?= $data->ket?></span></td>
                            <!-- <td>
                              <div class="tooltip-demo">
                                <a data-toggle="modal" data-target="#modal-edit-sutri<?=$data->ID;?>" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-pencil-alt"></i></a>
                                <a href="<?php echo site_url('Modal/hapus_ahli_waris/'.$data->ID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$data->ID;?> ?');" class="btn btn-danger btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                              </div>
                            </td> -->
                          </tr>
                          <?php
                            $sl++;
                            endforeach; ?>
                        </tbody>
                      </table>
                      <h3 class="card-title"><small>B.Data Anak</small> </h3><br>
                       <!-- <a data-toggle="modal" data-target="#modal-add" class="btn btn-primary btn-circle btn-xs" data-popup="tooltip" data-placement="top"><i class="fas fa-plus">&nbsp;</i>Add Data</a> -->
                      <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Lahir</th>
                            <th>Umur</th>
                            <th>Nama Ayah/Ibu</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody class="row_positions">
                          	<?php
                            $sl = '1';
                            foreach($anak as $data): ?>
                            <tr id="<?= $data->ID ?>" >
                            <td><?= $data->KF_03?></td>
                            <td><?= $data->KF_04?></td>
                            <td><?= $data->KF_05?></td>
                            <td><?= $data->umur?></td>
                            <td><?= $data->nama?></td>
                            <td><?= $data->keterangan?></td>
                            <td><span style="background-color: #FFFF00"> <?= $data->ket?></span></td>
                            <!-- <td>
                              <div class="tooltip-demo">
                                <a data-toggle="modal" data-target="#modal-edit<?=$data->ID;?>" class="btn btn-warning btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fas fa-pencil-alt"></i></a>
                                <a href="<?php echo site_url('Bup/hapus_ahli_waris/'.$data->ID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$data->ID;?> ?');" class="btn btn-danger btn-circle btn-xs" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                              </div>
                            </td> -->
                          </tr>
                          <?php
                            $sl++;
                            endforeach; ?>
                        </tbody>
                      </table>
                      <h3 class="card-title">Kelengkapan Dokumen Persyaratan di E-FILE</h3><br><br>
                      <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Jenis Berkas</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($efilenya as $key => $val) :?> 
                                <tr>
                                    <td><?=$key+1;?></td>
                                    <td><?=$val['nama_jenis']?></td>
                                    <td> 
                                       <?php if($val['efile'] != null) :?>
                                          <span class="badge badge-pill badge-info">Sudah Ada</span>
                                        <?php else :?>
                                          <!-- <button class="btn btn-warning btn-xs">Belum ada</button> -->
                                          <span class="badge badge-pill badge-danger">Belum Ada</span>
                                          </form>
                                        <?php endif ;?></td>
                                    <td>
                                        <?php if($val['efile'] != null) :?>
                                            <a href="<?=$val['efile']?>" target="_blank" rel="noopener noreferrer" class="btn btn-primary btn-xs text-white"> <i class="fas fa-print "></i></a>
                                        <?php else :?>
                                            <span class="label label-warning"></span>
                                            <form action="" method="post">
                                            <a href="<?= site_url('Bup/cetak/'. $row->B_02B)?>" class="btn btn-danger btn-xs text-white">
                                           <?php if ($val['nama_jenis']=='DATA PENERIMA CALON PENSIUN ( DPCP )') {
                                            echo '<i class="fas fa-download "></i>';
                                           }else {
                                            echo'<i class="fas fa-times-circle "></i>';
                                           } ?>
                                            
                                            </a>
                                            <a href="http://efile.bkd.jatengprov.go.id/"  target="_blank" class="btn btn-success btn-xs text-white"><i class="fas fa-upload"></i></a>
                                            <input type="hidden" name="B_02B" value="<?= $row->B_02B ?>">
                                          </form>
                                        <?php endif ;?>
                                         
                                    </td>
                                </tr>
                                <?php endforeach ;?>
                        </tbody>
                      </table>

                     
                            <div class="col-md-4">
                              <div class="form-group">
                               
                                
                                  <a href="<?=base_url('welcome/download_zip/'.$row->B_02B)?>" class="btn btn-success"><i class="fas fa-download"> &nbsp;</i>Download Semua</a>
                                
                              </div>
                              <!-- /.form-group -->
                            </div>
                          </div>
                  

                       <!-- TABEL LAST POSITION -->
                      <h3 class="card-title">Posisi BUP</h3><br><br>
                      <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th style="width:5%">No</th>
                            <th>Posisi BUP</th>
                            <th>Status BUP</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td><?php if ($row->last_position == 0) { echo "PNS";} elseif ($row->last_position == 1) {
                              echo "SKPD 1";
                            }elseif ($row->last_position == 2) {
                              echo "BKD";
                            }elseif ($row->last_position == 3) {
                              echo "BKN";
                            }else { echo"SKPD 2";}?></td>
                            <td><?php if ($row->status_ajuan == 0) { echo '<span class="badge badge-pill badge-info">Dikirim dari BKD ke SKPD</span>';
                            } elseif ($row->status_ajuan == 1) {
                              echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke PNS</span>';
                            }elseif ($row->status_ajuan == 2) {
                              echo '<span class="badge badge-pill badge-info">Dikirim dari PNS ke SKPD</span>';
                            }elseif ($row->status_ajuan == 3) {
                              echo '<span class="badge badge-pill badge-info">Dikirim dari SKPD ke BKD</span>';
                            }elseif ($row->status_ajuan == 4) {
                              echo '<span class="badge badge-pill badge-info">Dikirim Revisi dari SKPD ke PNS</span>';
                            }else { echo'<span class="badge badge-pill badge-info">Dikirim dari BKD ke BKN</span>';}?></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- END -->
                      <!-- TABEL HISTORI -->
                      <h3 class="card-title">Histori BUP</h3><br><br>
                      <table  id="filterTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th style="width:5%">No</th>
                            <th>NIP Acc</th>
                            <th>Jabatan</th>
                            <th>Status Acc</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                              foreach ($status_acc as $data ) {   
                            ?>
                          <tr>
                            <td><?=$no++?></td>
                            <td><?= $data->nip_acc?></td>
                            <td><?= $data->jabatan_acc?></td>
                            <td><?php if ($data->status_acc == 0) { echo "Menunggu";} elseif ($data->status_acc == 1) {
                              echo "ACC";
                            }else { echo"Revisi";}?></td>
                          </tr>
                          <?php } ?> 
                        </tbody>
                      </table>
                      <!-- END -->
                      <div class="card-footer">
                            <!-- /.col -->
                            <div class="col-md-4">
                              <div class="form-group">
                                <form action="<?= site_url('Bup/acc_ajuan_skpd'); ?>" method="post" enctype="multipart/form-data">
                                  <input type="hidden" readonly value="<?=$row->B_02B;?>" name="B_02B" class="form-control" >
                                  <input type="hidden" name="last_position" value="0">
                                  <button type="submit" onclick="myFunction()" class="btn btn-success"><i class="fas fa-arrow-circle-up"> &nbsp;</i>Proses ACC</button>
                                </form>
                              </div>
                              <!-- /.form-group -->
                            </div>
                          </div>
                      </div>
                    </form>
                  </div>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <?php $no=0; foreach($anak as $data): $no++; ?>
    <div class="modal fade" id="modal-add">
    <div class="modal-dialog">
      <form action="<?= site_url('Bup/insert_anak'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Data Anak</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class='col-md-12'><input type="hidden" name="KF_02" autocomplete="off" value="2" required placeholder="Masukkan Modal" class="form-control" ></div>
                <div class='col-md-12'><input type="hidden" name="KF_08" autocomplete="off" value="K" required placeholder="Masukkan Modal" class="form-control" ></div>
                <div class='col-md-12'><input type="hidden" name="KF_01" autocomplete="off" value="<?= $data->KF_01?>" required placeholder="Masukkan Modal" class="form-control" ></div>
            <div class="form-group">
                <label class='col-md-12'>Nama Anak</label>
                <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="" required placeholder="Masukkan Nama" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tempat Lahir</label>
                <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan Tempat Lahir" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tanggal Lahir</label>
                <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="KF_05" required class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Jenis Kelamin </label>
                <select name="KF_10" class="form-control not-dark"  required>
                  <option value="">--Select--</option>
                  <option value="L">Laki-laki</option>
                  <option value="P">Perempuan</option>
								</select>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Anak ke</label>
                <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="KF_03" required placeholder="Masukkan Angka" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Anak ke.. dari istri ke.. </label>
                <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="" required placeholder="Keterangan contoh : Anak ke 1 dari istri ke 1 " class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
              </div>
              
              <div class="form-group">
                <label class='col-md-12'>Status</label>
                <select name="status" class="form-control not-dark"  required>
                  <option value="">--Select--</option>
                  <?php foreach ($status_anak as $key => $data) { ?>
                  <option value="<?= $data->id ?>"<><?= $data->ket ?></option>
                  <?php } ?>
								</select>
              </div>
          </div>
          
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
   <?php endforeach; ?>
  <!-- /.modal -->
    <!-- end -->
    <!-- Modal Add Sutri -->
    <?php $no=0; foreach($sutri as $data): $no++; ?>
    <div class="modal fade" id="modal-add-sutri">
    <div class="modal-dialog">
      <form action="<?= site_url('Bup/insert_sutri'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Data Suami/Istri</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class='col-md-12'><input type="hidden" name="KF_02" autocomplete="off" value="1" required placeholder="Masukkan Modal" class="form-control" ></div>
                <div class='col-md-12'><input type="hidden" name="KF_01" autocomplete="off" value="<?= $data->KF_01?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              <div class="form-group">
                <label class='col-md-12'>Nama Suami/Istri</label>
                <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="" required placeholder="Masukkan Nama" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tempat Lahir</label>
                <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="" required class="form-control" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukkan Tempat Lahir" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tanggal Lahir</label>
                <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="KF_05" required class="form-control" ></div>
              </div>
               <div class="form-group">
                <label class='col-md-12'>Tanggal Menikah</label>
                <div class='col-md-12'><input type="date" name="KF_06" autocomplete="off" value="KF_06" required class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Jenis Kelamin </label>
                <select name="KF_10" class="form-control not-dark"  required>
                  <option value="">--Select--</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
								</select>
              </div>
              
              <div class="form-group">
                <label class='col-md-12'>Suami/Istri ke</label>
                <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="KF_03" required placeholder="Masukkan Angka" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Suami/Istri ke...</label>
                <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="" required placeholder="Keterangan contoh : Istri ke 1 " class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
              </div>
              
              <div class="form-group">
                <label class='col-md-12'>Status</label>
                <select name="status" class="form-control not-dark"  required>
                  <option value="">--Select--</option>
                  <?php foreach ($status_sutri as $key => $data) { ?>
                  <option value="<?= $data->id ?>"><?= $data->ket ?></option>
                  <?php } ?>
								</select>
              </div>
          </div>
          
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
   <?php endforeach; ?>
    <!-- END -->
  <!-- MODAL edit anak-->
  <?php $no=0; foreach($anak as $data): $no++; ?>
  <div class="modal fade" id="modal-edit<?=$data->ID;?>">
    <div class="modal-dialog">
      <form action="<?= site_url('Bup/edit_anak'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Anak</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <input type="hidden" readonly value="<?=$data->ID;?>" name="ID" class="form-control" >
              <div class="form-group">
                <label class='col-md-12'>Nama Anak</label>
                <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="<?=$data->KF_04;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tempat Lahir</label>
                <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="<?=$data->KF_09;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tanggal Lahir</label>
                <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="<?=$data->KF_05;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Jenis Kelamin </label>
                <select name="KF_10" class="form-control not-dark"  required>
                  <option value="<?=$data->KF_10;?>"><?=$data->KF_10;?></option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
								</select>
              </div>
               <div class="form-group">
                <label class='col-md-12'>Anak ke</label>
                <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="<?= $data->KF_03?>" required placeholder="Masukkan Angka" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Anak ke.. dari istri ke..</label>
                <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="<?= $data->keterangan?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Status</label>
                <select name="status" class="form-control not-dark"  required>
                  <?php foreach ($status_anak as $key => $data) { ?>
                  <option value="<?= $data->id ?>" ><?= $data->ket ?></option>
                  <?php } ?>
								</select>
              </div>
          </div>
          
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
    
  <?php endforeach; ?>

  <!-- MODAL edit Suami/istri-->
  <?php $no=0; foreach($sutri as $data): $no++; ?>
  <div class="modal fade" id="modal-edit-sutri<?=$data->ID;?>">
    <div class="modal-dialog">
      <form action="<?php echo site_url('Bup/edit_sutri'); ?>" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Suami/Istri</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <input type="hidden" readonly value="<?=$data->ID;?>" name="ID" class="form-control" >
            <div class="form-group">
                <label class='col-md-12'>Nama Suami/Istri</label>
                <div class='col-md-12'><input type="text" name="KF_04" autocomplete="off" value="<?=$data->KF_04;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tempat Lahir</label>
                <div class='col-md-12'><input type="text" name="KF_09" autocomplete="off" value="<?=$data->KF_09;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tanggal Lahir</label>
                <div class='col-md-12'><input type="date" name="KF_05" autocomplete="off" value="<?=$data->KF_05;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Tanggal Menikah</label>
                <div class='col-md-12'><input type="date" name="KF_06" autocomplete="off" value="<?=$data->KF_06;?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Jenis Kelamin </label>
                <select name="KF_10" class="form-control not-dark"  required>
                  <option value="<?=$data->KF_10;?>"><?=$data->KF_10;?></option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
								</select>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Suami/Istri ke</label>
                <div class='col-md-12'><input type="number" name="KF_03" autocomplete="off" value="<?=$data->KF_03;?>" required placeholder="Masukkan Angka" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Istri/Suami ke</label>
                <div class='col-md-12'><input type="text" name="keterangan" autocomplete="off" value="<?= $data->keterangan?>" required placeholder="Masukkan Modal" class="form-control" ></div>
              </div>
              <div class="form-group">
                <label class='col-md-12'>Status</label>
                <select name="status" class="form-control not-dark"  required>
                  <?php foreach ($status_sutri as $key => $data) { ?>
                  <option value="<?= $data->id ?>"><?= $data->ket ?></option>
                  <?php } ?>
								</select>
              </div>
            </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
    
  <?php endforeach; ?>
</div>

<!-- draggable ortu-->
<script type="text/javascript"> 

    $(document).ready( function () {
      $('#MSG').slideUp(3500);
    });

    $( ".row_position" ).sortable({
      stop: function() {
			var selectedData = new Array();
            $('.row_position>tr').each(function() {

                selectedData.push($(this).attr("ID"));

            });

            updateOrder(selectedData);

        }

    });


    function updateOrder(data) {

        $.ajax({

            url:"<?php echo base_url('Bup/updatesort');?>",

            type:'post',

            data:{sortable:data},

            success:function(result){
            	window.location.reload();
             }

        })

    }

</script>

<!-- draggable anak-->
<script type="text/javascript">

    $(document).ready( function () {
      $('#MSG').slideUp(3500);
    });

    $( ".row_positions" ).sortable({
      stop: function() {
			var selectedData = new Array();
            $('.row_positions>tr').each(function() {

                selectedData.push($(this).attr("ID"));

            });

            updateOrder(selectedData);

        }

    });


    function updateOrder(data) {

        $.ajax({

            url:"<?php echo base_url('Bup/updatesort_anak');?>",

            type:'post',

            data:{anak:data},

            success:function(result){
            	window.location.reload();
             }

        })

    }

</script>

<!-- Load provinsi -->
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#provinsi").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kota").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("Bup/list_kota"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#provinsi").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kota").html(response.list_kota).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>

<!-- load kota -->
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("Bup/list_kecamatan"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kota").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#kecamatan").html(response.list_kecamatan).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>

<!-- load kecamatan -->
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#kelurahan").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("Bup/list_kelurahan"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kecamatan").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#kelurahan").html(response.list_kelurahan).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>

<!-- Edit inline -->
<script>
function myFunction() {
  var txt;
  if (confirm("Kirim data ke BKN? Setelah mengirim admin BKD tidak dapat merubah data lagi ")) {
    
  } 
}
</script>




