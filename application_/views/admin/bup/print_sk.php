<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
  <head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 15">
    <meta name=Originator content="Microsoft Word 15">
    <link rel=File-List href="Btul.fld/filelist.xml">

    <link rel=dataStoreItem href="Btul.fld/item0001.xml"
    target="Btul.fld/props002.xml">
    <link rel=themeData href="Btul.fld/themedata.thmx">
    <link rel=colorSchemeMapping href="Btul.fld/colorschememapping.xml">
    <style>
     /* Font Definitions */
      @font-face
        {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:roman;
        mso-font-pitch:variable;
        mso-font-signature:-536870145 1107305727 0 0 415 0;}
      @font-face
        {font-family:Calibri;
        panose-1:2 15 5 2 2 2 4 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:swiss;
        mso-font-pitch:variable;
        mso-font-signature:-536859905 -1073732485 9 0 511 0;}
      @font-face
        {font-family:"Bookman Old Style";
        panose-1:2 5 6 4 5 5 5 2 2 4;
        mso-font-charset:0;
        mso-generic-font-family:roman;
        mso-font-pitch:variable;
        mso-font-signature:647 0 0 0 159 0;}
      /* Style Definitions */
      p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0cm;
        mso-pagination:widow-orphan;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      h1
        {mso-style-priority:9;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 1 Char";
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:51.35pt;
        mso-pagination:widow-orphan;
        mso-outline-level:1;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";
        font-weight:normal;}
      p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-priority:99;
        mso-style-link:"Header Char";
        margin:0cm;
        mso-pagination:widow-orphan;
        tab-stops:center 234.0pt right 468.0pt;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-priority:99;
        mso-style-link:"Footer Char";
        margin:0cm;
        mso-pagination:widow-orphan;
        tab-stops:center 234.0pt right 468.0pt;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
        {mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-link:"Body Text Char";
        margin:0cm;
        mso-pagination:widow-orphan;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {mso-style-priority:1;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:94.2pt;
        text-indent:-14.2pt;
        mso-pagination:widow-orphan;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      span.BodyTextChar
        {mso-style-name:"Body Text Char";
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Body Text";
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-ascii-font-family:"Bookman Old Style";
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-hansi-font-family:"Bookman Old Style";
        mso-bidi-font-family:"Times New Roman";}
      p.TableParagraph, li.TableParagraph, div.TableParagraph
        {mso-style-name:"Table Paragraph";
        mso-style-priority:1;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin:0cm;
        mso-line-height-alt:7.3pt;
        mso-pagination:widow-orphan;
        text-autospace:none;
        font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-bidi-font-family:"Times New Roman";}
      span.Heading1Char
        {mso-style-name:"Heading 1 Char";
        mso-style-priority:9;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 1";
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-ascii-font-family:"Bookman Old Style";
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-hansi-font-family:"Bookman Old Style";
        mso-bidi-font-family:"Times New Roman";
        mso-font-kerning:18.0pt;}
      span.HeaderChar
        {mso-style-name:"Header Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Header;
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-ascii-font-family:"Bookman Old Style";
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-hansi-font-family:"Bookman Old Style";
        mso-bidi-font-family:"Times New Roman";}
      span.FooterChar
        {mso-style-name:"Footer Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Footer;
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Bookman Old Style",serif;
        mso-ascii-font-family:"Bookman Old Style";
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-theme-font:minor-fareast;
        mso-hansi-font-family:"Bookman Old Style";
        mso-bidi-font-family:"Times New Roman";}
      span.SpellE
        {mso-style-name:"";
        mso-spl-e:yes;}
      span.GramE
        {mso-style-name:"";
        mso-gram-e:yes;}
      .MsoChpDefault
        {mso-style-type:export-only;
        mso-default-props:yes;
        font-family:"Calibri",sans-serif;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
      /* Page Definitions */
      @page
        {mso-footnote-separator:url("Btul.fld/header.html") fs;
        mso-footnote-continuation-separator:url("Btul.fld/header.html") fcs;
        mso-endnote-separator:url("Btul.fld/header.html") es;
        mso-endnote-continuation-separator:url("Btul.fld/header.html") ecs;}
      @page WordSection1
        {size:1008.0pt 612.0pt;
        mso-page-orientation:landscape;
        margin:2.0cm 2.0cm 1.0cm 2.0cm;
        mso-header-margin:35.45pt;
        mso-footer-margin:35.45pt;
        mso-paper-source:0;}
      div.WordSection1
        {page:WordSection1;}

    </style>
  </head>
  <body lang=EN-ID style='tab-interval:36.0pt;word-wrap:break-word'>
    <div class=WordSection1>
      <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 align=left width=1323 
        style='border-collapse:collapse;mso-table-layout-alt:fixed;
        border:none;mso-yfti-tbllook:1184;mso-table-lspace:9.0pt;margin-left:6.75pt;
        mso-table-rspace:9.0pt;margin-right:6.75pt;mso-table-anchor-vertical:margin;
        mso-table-anchor-horizontal:page;mso-table-left:8.4pt;mso-table-top:-64.5pt;
        mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
        none'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:14.1pt'>
          <td width=658 valign=top style='width:493.2pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=32 valign=top style='width:23.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=633 valign=top style='width:474.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.1pt'>
            <p class=MsoListParagraph style='margin-top:.3pt;margin-right:0cm;margin-bottom:
            0cm;margin-left:51.3pt;margin-bottom:.0001pt;text-align:justify;text-indent:
            -21.25pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
            around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
            -64.5pt;mso-height-rule:exactly'><b><span lang=EN-US style='font-size:8.0pt;
            letter-spacing:-.1pt;mso-ansi-language:EN-US'>B</span></b><span lang=EN-US
            style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'>. <b>KELUARGA
            PENERIMA PENSIUN<o:p></o:p></b></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:71.6pt'>
          <td width=658 valign=top style='width:493.2pt;padding:0cm 5.4pt 0cm 5.4pt; height:71.6pt'>
            <p class=MsoNormal style='margin-top:5.0pt;margin-right:132.4pt;margin-bottom:
            0cm;margin-left:0cm;margin-bottom:.0001pt;mso-element:frame;mso-element-frame-hspace:
            9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><b><span lang=IN
            style='font-size:8.0pt;mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></b></p>
            <p class=MsoNormal align=center style='margin-top:5.0pt;margin-right:132.4pt;
            margin-bottom:0cm;margin-left:115.5pt;margin-bottom:.0001pt;text-align:center;
            text-indent:-.15pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><b><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>KEPUTUSAN GUBERNUR JAWA
            TENGAH<span style='mso-spacerun:yes'><br></span></span></b><span
            class=GramE><b><span lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>NOMOR<span
            style='letter-spacing:2.0pt'> </span>:</span></b></span><b><span lang=IN
            style='font-size:8.0pt;letter-spacing:2.0pt;mso-ansi-language:IN'> </span></b><b><span
            lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>882/9325/015/23300/AZ/12/21</span></b><span
            lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
            <p class=MsoNormal align=center style='margin-top:0cm;margin-right:12.8pt;
            margin-bottom:0cm;margin-left:37.5pt;margin-bottom:.0001pt;text-align:center;
            text-indent:19.7pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><b><span lang=IN
            style='font-size:8.0pt;mso-ansi-language:IN'>TENTANG<span style='letter-spacing:
            2.0pt'> </span>PEMBERIAN<span style='letter-spacing:2.0pt'> </span>KENAIKAN<span
            style='letter-spacing:2.0pt'> </span>PANGKAT<span style='letter-spacing:2.0pt'>
            </span>PENGABDIAN,<span style='letter-spacing:2.0pt'> </span>PEMBERHENTIAN<span
            style='letter-spacing:2.0pt'> </span>DAN<span style='letter-spacing:2.0pt'> </span>PEMBERIAN<span
            style='letter-spacing:2.4pt'> </span>PENSIUN<span style='letter-spacing:2.45pt'>
            </span>PEGAWAI<span style='letter-spacing:2.4pt'> </span>NEGERI<span
            style='letter-spacing:2.35pt'> </span>SIPIL<span style='letter-spacing:2.6pt'>
            </span>YANG<span style='letter-spacing:2.6pt'> </span>MENCAPAI<span
            style='letter-spacing:2.6pt'> </span>BATAS<span style='letter-spacing:2.6pt'>
            </span>USIA<span style='letter-spacing:2.55pt'> </span><span
            style='letter-spacing:-.1pt'>PENSIUN</span></span></b><span lang=EN-US
            style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
            <p class=MsoNormal align=center style='margin-top:5.4pt;margin-right:132.2pt;
            margin-bottom:0cm;margin-left:122.6pt;margin-bottom:.0001pt;text-align:center;
            mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
            mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
            -64.5pt;mso-height-rule:exactly'><b><span lang=IN style='font-size:8.0pt;
            mso-ansi-language:IN'>DENGAN RAHMAT TUHAN YANG MAHA ESA GUBERNUR JAWA TENGA</span></b><b><span
            lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'>H</span></b><span
            lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
          </td>
          <td width=32 valign=top style='width:23.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:71.6pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=633 valign=top style='width:474.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:71.6pt'>
            <p class=MsoNormal style='margin-left:44.2pt;mso-element:frame;mso-element-frame-hspace:
            9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'>1.
            ISTERI/SUAMI</span><span lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=496
              style='margin-left:45.45pt;border-collapse:collapse;mso-table-layout-alt:
              fixed;border:none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:
              1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
              <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=33 valign=top style='width:24.9pt;border:solid windowtext 1.0pt; mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                  <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                  mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                  8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                  style='font-size:8.0pt;mso-ansi-language:EN-US'>NO<o:p></o:p></span></p>
                </td>
                <td width=132 valign=top style='width:99.25pt;border:solid windowtext 1.0pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                  <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                  mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                  8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                  style='font-size:8.0pt;mso-ansi-language:EN-US'>NAMA<o:p></o:p></span></p>
                </td>
                <td width=123 valign=top style='width:92.1pt;border:solid windowtext 1.0pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                  <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                  mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                  8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                  style='font-size:8.0pt;mso-ansi-language:EN-US'>TGL LAHIR<o:p></o:p></span></p>
                </td>
                <td width=142 valign=top style='width:106.35pt;border:solid windowtext 1.0pt;
                border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'>TGL PERKAWINAN<o:p></o:p></span></p>
                </td>
                <td width=66 valign=top style='width:49.6pt;border:solid windowtext 1.0pt;
                border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'>KET<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes;height:22.1pt'>
                <td width=33 valign=top style='width:24.9pt;border:solid windowtext 1.0pt;
                border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:22.1pt'>
                <p class=MsoNormal style='margin-left:36.0pt;mso-element:frame;mso-element-frame-hspace:
                9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;
                mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
                lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=132 valign=top style='width:99.25pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                height:22.1pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                height:22.1pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=142 valign=top style='width:106.35pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                height:22.1pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=66 valign=top style='width:49.6pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                height:22.1pt'>
                <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
                mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
                8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
                style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
                </td>
              </tr>
            </table>
          <p class=MsoNormal style='margin-left:44.2pt;mso-element:frame;mso-element-frame-hspace:
          9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
          style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
          <p class=MsoNormal style='margin-left:44.2pt;mso-element:frame;mso-element-frame-hspace:
          9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
          style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'>2. ANAK</span><span
          lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
          <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=496
          style='margin-left:45.45pt;border-collapse:collapse;mso-table-layout-alt:
          fixed;border:none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:
          1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
          <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=33 valign=top style='width:24.9pt;border:solid windowtext 1.0pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>NO<o:p></o:p></span></p>
            </td>
            <td width=132 valign=top style='width:99.25pt;border:solid windowtext 1.0pt;
            border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>NAMA<o:p></o:p></span></p>
            </td>
            <td width=123 valign=top style='width:92.1pt;border:solid windowtext 1.0pt;
            border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>TGL LAHIR<o:p></o:p></span></p>
            </td>
            <td width=142 valign=top style='width:106.35pt;border:solid windowtext 1.0pt;
            border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>NAMA AYAH/IBU<o:p></o:p></span></p>
            </td>
            <td width=66 valign=top style='width:49.6pt;border:solid windowtext 1.0pt;
            border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'>KET<o:p></o:p></span></p>
            </td>
          </tr>
          <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes;height:22.1pt'>
            <td width=33 valign=top style='width:24.9pt;border:solid windowtext 1.0pt;
            border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:22.1pt'>
            <p class=MsoNormal style='margin-left:36.0pt;mso-element:frame;mso-element-frame-hspace:
            9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;
            mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
            lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=132 valign=top style='width:99.25pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:22.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:22.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=142 valign=top style='width:106.35pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:22.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=66 valign=top style='width:49.6pt;border-top:none;border-left:
            none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
            mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
            height:22.1pt'>
            <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
            mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
            8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
            style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
            </td>
          </tr>
          </table>
          <p class=MsoNormal style='margin-left:58.4pt;mso-element:frame;mso-element-frame-hspace:
          9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
          style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
          </td>
        </tr>
      <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:401.45pt'>
        <td width=658 valign=top style='width:493.2pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:401.45pt'>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:2.2pt;margin-bottom:
        0cm;margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -73.1pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='mso-ansi-language:IN'>Menimbang<span
        style='letter-spacing:4.0pt'>&nbsp; </span>:<span style='letter-spacing:2.0pt'>
        <span style='mso-tab-count:1'>  </span></span> &nbsp; bahwa Pegawai Negeri Sipil yang
        namanya tercantum dalam keputusan ini telah mencapai batas usia pensiun dan
        telah memenuhi syarat untuk diberikan kenaikan pangkat pengabdian dan
        diberhentikan dengan hormat sebagai Pegawai Negeri Sipil dengan hak pensiun.</span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.45pt;text-align:justify;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.05pt;margin-right:0cm;margin-bottom:
        0cm;margin-left:5.8pt;margin-bottom:.0001pt;text-align:justify;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
        lang=IN style='letter-spacing:-.1pt;mso-ansi-language:IN'>Mengingat</span><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
        style='letter-spacing:1.45pt'>&nbsp; </span></span><span lang=IN
        style='letter-spacing:1.45pt;mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span></span><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp; 1.</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>  </span></span><span
        lang=IN style='mso-ansi-language:IN'>&nbsp; Pasal<span style='letter-spacing:-.25pt'>
        </span>4<span style='letter-spacing:-.1pt'> </span>ayat<span
        style='letter-spacing:-.25pt'> </span>(1)<span style='letter-spacing:-.2pt'> </span><span
        class=SpellE> Undang-Undang</span><span style='letter-spacing:-.15pt'> </span>Dasar<span
        style='letter-spacing:2.05pt'> </span>Republik Indonesia<span
        style='letter-spacing:-.15pt'> </span>Tahun<span style='letter-spacing:-.1pt'>
        1945;</span></span><span lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-right:3.2pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>2.</span><span lang=IN
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        0.7pt;mso-ansi-language:IN'>&nbsp;&nbsp; </span><span class=SpellE><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'> Undang-Undang</span></span><span
        lang=IN style='font-size:8.0pt;letter-spacing:2.0pt;mso-ansi-language:IN'> </span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Nomor<span
        style='letter-spacing:2.0pt'> </span>11<span style='letter-spacing:2.0pt'> </span>Tahun<span
        style='letter-spacing:2.0pt'> </span>1969<span style='letter-spacing:2.0pt'> </span>tentang<span
        style='letter-spacing:2.0pt'> </span>Pensiun<span style='letter-spacing:2.0pt'>
        </span>Pegawai<span style='letter-spacing:2.0pt'> </span>dan<span
        style='letter-spacing:2.0pt'> </span>Pensiun<span style='letter-spacing:2.0pt'>
        </span>Janda/Duda</span><span lang=IN style='font-size:8.0pt;mso-ansi-language:
        EN-US'> </span><span lang=IN style='font-size:8.0pt;letter-spacing:-.1pt;
        mso-ansi-language:IN'>Pegawai;</span><span lang=EN-US style='font-size:8.0pt;
        mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='text-align:justify;line-height:9.9pt;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>3.</span><span lang=IN
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        0.7pt;mso-ansi-language:IN'>&nbsp;&nbsp; </span><span class=SpellE><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Undang-Undang</span></span><span
        lang=IN style='font-size:8.0pt;letter-spacing:-.3pt;mso-ansi-language:IN'> </span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Nomor<span
        style='letter-spacing:-.3pt'> </span>5<span style='letter-spacing:-.25pt'> </span>Tahun<span
        style='letter-spacing:-.25pt'> </span>2014<span style='letter-spacing:-.3pt'>
        </span>tentang<span style='letter-spacing:-.25pt'> </span>Aparatur<span
        style='letter-spacing:-.2pt'> </span>Sipil<span style='letter-spacing:-.25pt'>
        </span><span style='letter-spacing:-.1pt'>Negara;</span></span><span
        lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-right:2.9pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        font-family:"Times New Roman",serif;letter-spacing:-0.3pt;mso-ansi-language:
        IN'>4.&nbsp;&nbsp;&nbsp;&nbsp;</span><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>Peraturan Pemerintah Nomor<span style='letter-spacing:
        -.05pt'> </span>7 Tahun 1977 tentang Peraturan Gaji PNS <span class=SpellE>jo</span>
        Peraturan Pemerintah Nomor 15 Tahun 2019;</span><span lang=EN-US
        style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.1pt;margin-right:2.95pt;
        margin-bottom:0cm;margin-left:94.2pt;margin-bottom:.0001pt;text-align:justify;
        line-height:96%;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        line-height:96%;font-family:"Times New Roman",serif;letter-spacing:-.1pt;
        mso-ansi-language:IN'>5.&nbsp;&nbsp;&nbsp;&nbsp;</span><span lang=IN
        style='font-size:8.0pt;line-height:96%;mso-ansi-language:IN'>Peraturan
        Pemerintah Nomor 18 Tahun 2019 tentang Penetapan Pensiun Pokok Pensiunan PNS
        dan Janda/Dudanya;</span><span lang=EN-US style='font-size:8.0pt;line-height:
        96%;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.05pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        font-family:"Times New Roman",serif;letter-spacing:-.1pt;mso-ansi-language:
        IN'>6.&nbsp;&nbsp;&nbsp;&nbsp;</span><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>Peraturan<span style='letter-spacing:-.3pt'> </span>Pemerintah<span
        style='letter-spacing:-.2pt'> </span>Nomor<span style='letter-spacing:-.2pt'>
        </span>11<span style='letter-spacing:-.3pt'> </span>Tahun<span
        style='letter-spacing:-.2pt'> </span>2017<span style='letter-spacing:-.25pt'>
        </span>tentang<span style='letter-spacing:-.2pt'> </span>Manajemen<span
        style='letter-spacing:-.2pt'> </span>Pegawai<span style='letter-spacing:-.3pt'>
        </span>Negeri<span style='letter-spacing:-.15pt'> </span><span
        style='letter-spacing:-.1pt'>Sipil;</span></span><span lang=EN-US
        style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.1pt;margin-right:3.1pt;
        margin-bottom:0cm;margin-left:94.2pt;margin-bottom:.0001pt;text-align:justify;
        line-height:96%;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        line-height:96%;font-family:"Times New Roman",serif;letter-spacing:-1.1pt;
        mso-ansi-language:IN'>7.&nbsp;&nbsp;&nbsp;&nbsp;</span><span lang=IN
        style='font-size:8.0pt;line-height:96%;mso-ansi-language:IN'>Peraturan Badan
        Kepegawaian Negara Nomor 2 Tahun 2018 tentang Pedoman Pemberian Pertimbangan
        Teknis Pensiun Pegawai Negeri Sipil dan Pensiun Janda/Duda Pegawai Negeri<span
        style='letter-spacing:2.0pt'> </span><span style='letter-spacing:-.1pt'>Sipil.</span></span><span
        lang=EN-US style='font-size:8.0pt;line-height:96%;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.05pt;text-align:justify;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:3.2pt;margin-bottom:
        0cm;margin-left:79.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -73.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='mso-ansi-language:IN'>Memperhatikan<span
        style='letter-spacing:1.95pt'> </span>:<span style='letter-spacing:13.0pt'> </span>Pertimbangan<span
        style='letter-spacing:-.2pt'> </span>Teknis<span style='letter-spacing:-.15pt'>
        </span>Kepala<span style='letter-spacing:-.2pt'> </span>Badan<span
        style='letter-spacing:-.2pt'> </span>Kepegawaian<span style='letter-spacing:
        -.2pt'> </span>Negara/Kepala<span style='letter-spacing:-.2pt'> </span>Kantor<span
        style='letter-spacing:-.15pt'> </span>Regional<span style='letter-spacing:
        -.25pt'> </span>Badan<span style='letter-spacing:-.2pt'> </span>Kepegawaian
        Negara Nomor <b>PH-23300002684 </b>Tanggal <b>16-11-2021</b>.</span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoNormal align=center style='margin-top:0cm;margin-right:132.2pt;
        margin-bottom:0cm;margin-left:131.55pt;margin-bottom:.0001pt;text-align:center;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>MEMUTUSKAN:</span><span
        lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-left:5.8pt;text-align:justify;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
        lang=IN style='mso-ansi-language:IN'>Menetapkan<span style='letter-spacing:
        3.2pt'>&nbsp;&nbsp; </span><span style='letter-spacing:-.5pt'>:</span></span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:1.95pt;margin-bottom:
        0cm;margin-left:101.35pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -95.55pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='letter-spacing:-.3pt;
        mso-ansi-language:IN'>KESATU &nbsp;</span><span lang=IN style='mso-ansi-language:
        IN'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span><span
        lang=IN style='mso-ansi-language:EN-US'> </span><span lang=IN
        style='mso-ansi-language:IN'><span style='mso-spacerun:yes'> </span></span><span
        lang=EN-US style='mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'>  </span></span><span lang=IN style='mso-ansi-language:
        IN'>&nbsp;&nbsp;&nbsp;&nbsp;(1)<span style='letter-spacing:2.0pt'> </span></span><span lang=IN
        style='letter-spacing:2.0pt;mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span></span><span
        lang=IN style='mso-ansi-language:IN'>Memberikan kenaikan pangkat pengabdian
        kepada Pegawai Negeri Sipil yang namanya tersebut</span><span lang=IN
        style='mso-ansi-language:EN-US'> <span style='mso-spacerun:yes'> </span></span><span
        lang=IN style='mso-ansi-language:IN'>dalam</span><span lang=IN
        style='mso-ansi-language:EN-US'> </span><span lang=IN style='mso-ansi-language:
        IN'>lajur 1 dari dan menjadi sebagaimana tersebut dalam lajur 6 dengan gaji
        pokok dari dan menjadi sebagaimana tersebut dalam lajur 8 Keputusan ini.</span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:0cm;margin-right:1.9pt;
        margin-bottom:0cm;margin-left:101.35pt;margin-bottom:.0001pt;text-align:justify;
        text-indent:-1.0cm;mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'>   </span></span><span lang=IN style='font-size:
        7.5pt;letter-spacing:-.1pt;mso-ansi-language:IN'>(2)</span><span lang=IN
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp; </span><span lang=EN-US
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>  </span><span
        style='mso-spacerun:yes'> </span></span><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>Memberhentikan dengan hormat sebagai Pegawai Negeri
        Sipil yang namanya tersebut dalam lajur 1 pada</span><span lang=IN
        style='font-size:8.0pt;mso-ansi-language:EN-US'> </span><span lang=IN
        style='font-size:8.0pt;mso-ansi-language:IN'>akhir bulan tersebut pada lajur
        10 Keputusan ini, disertai ucapan terima kasih atas jasa-</span><span
        class=SpellE><span lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'>jasa</span></span><span
        lang=EN-US style='font-size:8.0pt;mso-ansi-language:EN-US'> <span
        class=SpellE>selama</span> <span class=SpellE>bekerja</span> pada <span
        class=SpellE>Pemerintahan</span> <span class=SpellE>Republik</span>
        Indonesia.<o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.3pt;margin-right:0cm;margin-bottom:
        0cm;margin-left:101.35pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -26.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=EN-US style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'>  </span></span><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>(3)</span><span lang=IN
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp;</span><span lang=EN-US
        style='font-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>   </span></span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Terhitung<span
        style='letter-spacing:1.1pt'> </span>mulai<span style='letter-spacing:1.1pt'>
        </span>tanggal<span style='letter-spacing:1.15pt'> </span>tersebut<span
        style='letter-spacing:1.1pt'> </span>dalam<span style='letter-spacing:1.1pt'>
        </span>lajur<span style='letter-spacing:1.2pt'> </span>11,<span
        style='letter-spacing:1.15pt'> </span>kepadanya<span style='letter-spacing:
        1.15pt'> </span>diberikan<span style='letter-spacing:1.1pt'> </span>pensiun<span
        style='letter-spacing:1.2pt'> </span>pokok<span style='letter-spacing:1.25pt'>
        </span><span style='letter-spacing:-.1pt'>sebulan</span></span><span
        lang=EN-US style='font-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:
        EN-US'> <span class=SpellE>sebesar</span> <span class=SpellE>tersebut</span> <span
        class=SpellE>dalam</span> <span class=SpellE>lajur</span> 12 <span
        class=SpellE>keputusan</span> <span class=SpellE>ini</span><o:p></o:p></span></p>
        <p class=MsoNormal style='margin-left:92.15pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><b><span lang=IN
        style='font-size:8.0pt;mso-ansi-language:IN'>A.<span style='letter-spacing:
        .45pt'> </span>PENERIMA<span style='letter-spacing:1.85pt'> </span><span
        style='letter-spacing:-.1pt'>PENSIUN<o:p></o:p></span></span></b></p>
        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=right
        style='border-collapse:collapse;mso-table-layout-alt:fixed;mso-table-overlap:
        never;mso-yfti-tbllook:1184;mso-table-lspace:9.0pt;margin-left:6.75pt;
        mso-table-rspace:9.0pt;margin-right:6.75pt;mso-table-anchor-vertical:paragraph;
        mso-table-anchor-horizontal:margin;mso-table-left:right;mso-table-top:3.95pt;
        mso-padding-alt:0cm 0cm 0cm 0cm'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:8.15pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>1.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border:solid black 1.0pt;
          border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          letter-spacing:-.2pt;mso-ansi-language:IN'>NAMA</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border:solid black 1.0pt;
          border-left:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>JOKO<span style='letter-spacing:-.2pt'> </span>KUSMANTO,<span
          style='letter-spacing:-.15pt'> </span><span class=SpellE><span
          style='letter-spacing:-.2pt'>A.Md</span></span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:8.3pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>2.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.4pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:
          IN'>NIP</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.4pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>196404171985031009<span
          style='letter-spacing:-.05pt'> </span>/<span style='letter-spacing:-.05pt'>
          </span><span style='letter-spacing:-.1pt'>500075891</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:2;height:8.15pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>3.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>TANGGAL<span style='letter-spacing:-.2pt'> </span><span
          style='letter-spacing:-.1pt'>LAHIR</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>17 APRIL<span style='letter-spacing:-.1pt'> </span><span
          style='letter-spacing:-.2pt'>1964</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:3;height:8.3pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>4.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.4pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:IN'>JABATAN</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>PARAMEDIK<span
          style='letter-spacing:-.3pt'> </span>VETERINER<span style='letter-spacing:
          -.15pt'> </span><span style='letter-spacing:-.1pt'>PENYELIA</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:4;height:8.15pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>5.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>UNIT<span style='letter-spacing:-.15pt'> </span>KERJA<span
          style='letter-spacing:-.1pt'> TERAKHIR</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.15pt'>
          <p class=TableParagraph style='margin-left:5.4pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>DINAS<span style='letter-spacing:-.35pt'> </span>PETERNAKAN<span
          style='letter-spacing:-.35pt'> </span>DAN<span style='letter-spacing:-.35pt'>
          </span><span style='letter-spacing:-.1pt'>KESWAN</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:5;height:8.3pt'>
          <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-top:4.4pt;margin-right:0cm;
          margin-bottom:0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:
          normal;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>6.</span></p>
          </td>
          <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:8.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          letter-spacing:-.1pt;mso-ansi-language:IN'>PANGKAT/GOL. RUANG</span></p>
          </td>
          <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.2pt;mso-ansi-language:IN'>LAMA</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.3pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>PENATA<span
          style='letter-spacing:-.35pt'> </span>TINGKAT<span style='letter-spacing:
          -.3pt'> </span>I/III/d/01-10-<span style='letter-spacing:-.2pt'>2013</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:6;height:8.2pt'>
          <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.2pt;mso-ansi-language:IN'>BARU</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          letter-spacing:-.1pt;mso-ansi-language:IN'>PEMBINA/IV/a/01-04-</span><span
          lang=IN style='ont-size:8.0pt;letter-spacing:-.2pt;mso-ansi-language:IN'>2022</span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:7;height:8.25pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>7.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>MASA<span
          style='letter-spacing:-.1pt'> </span>KERJA<span style='letter-spacing:-.1pt'>
          GOLONGAN</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>33 TAHUN<span
          style='letter-spacing:-.1pt'> </span>6<span style='letter-spacing:-.05pt'> </span><span
          style='letter-spacing:-.1pt'>BULAN</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:8;height:8.2pt'>
          <td width=28 rowspan=2 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;
          margin-bottom:0cm;margin-left:9.35pt;margin-bottom:.0001pt;line-height:
          normal;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>8.</span></p>
          </td>
          <td width=95 rowspan=2 valign=top style='width:70.9pt;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-top:4.3pt;margin-right:0cm;
          margin-bottom:0cm;margin-left:5.45pt;margin-bottom:.0001pt;line-height:
          normal;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;mso-ansi-language:IN'>GAJI<span style='letter-spacing:
          -.25pt'> </span><span style='letter-spacing:-.1pt'>POKOK</span></span></p>
          </td>
          <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph align=right style='margin-right:10.45pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.2pt;mso-ansi-language:IN'>LAMA</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>Rp<span style='letter-spacing:-.05pt'> </span><span
          style='letter-spacing:-.1pt'>4.797.000</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:9;height:8.25pt'>
          <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph align=right style='margin-right:10.35pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.2pt;mso-ansi-language:IN'>BARU</span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>Rp<span
          style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>5.000.000</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:10;height:8.2pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>9.</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>MASA<span style='letter-spacing:-.1pt'> </span>KERJA<span
          style='letter-spacing:-.1pt'> PENSIUN</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>38 TAHUN<span style='letter-spacing:-.1pt'> </span>7<span
          style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:11;height:8.25pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>10</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>BERHENTI<span
          style='letter-spacing:-.1pt'> </span>AKHIR<span style='letter-spacing:-.15pt'>
          </span><span style='letter-spacing:-.1pt'>BULAN</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>APRIL<span
          style='letter-spacing:-.15pt'> </span><span style='letter-spacing:-.2pt'>2022</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:12;height:8.2pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;line-height:7.2pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
          mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
          8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>11</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>PENSIUN<span style='letter-spacing:-.1pt'> </span><span
          style='letter-spacing:-.25pt'>TMT</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.2pt'>
          <p class=TableParagraph style='margin-left:5.45pt;line-height:7.2pt;
          mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
          mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
          -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
          mso-ansi-language:IN'>1<span style='letter-spacing:-.15pt'> </span>MEI <span
          style='letter-spacing:-.2pt'>2022</span></span></p>
          </td>
        </tr>
        <tr style='mso-yfti-irow:13;mso-yfti-lastrow:yes;height:8.25pt'>
          <td width=28 valign=top style='width:21.2pt;border:solid black 1.0pt;
          border-top:none;padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph align=right style='margin-right:4.7pt;text-align:
          right;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
          around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;
          mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
          style='ont-size:8.0pt;letter-spacing:-.25pt;mso-ansi-language:IN'>12</span></p>
          </td>
          <td width=151 colspan=2 valign=top style='width:4.0cm;border-top:none;
          border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>PENSIUN<span
          style='letter-spacing:-.1pt'> POKOK</span></span></p>
          </td>
          <td width=313 valign=top style='width:234.4pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
          padding:0cm 0cm 0cm 0cm;height:8.25pt'>
          <p class=TableParagraph style='margin-left:5.45pt;mso-element:frame;
          mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
          page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
          lang=IN style='ont-size:8.0pt;mso-ansi-language:IN'>Rp<span
          style='letter-spacing:-.05pt'> </span><span style='letter-spacing:-.1pt'>3.750.000</span></span></p>
          </td>
        </tr>
        </table>
        <p class=MsoNormal style='margin-left:49.65pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.3pt;margin-right:0cm;margin-bottom:
        0cm;margin-left:93.1pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -17.75pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=32 valign=top style='width:23.95pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:401.45pt'>
        <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        </td>
        <td width=633 valign=top style='width:474.85pt;padding:0cm 5.4pt 0cm 5.4pt;
        height:401.45pt'>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:23.6pt;margin-bottom:
        0cm;margin-left:2.0cm;margin-bottom:.0001pt;text-align:justify;text-indent:
        -49.6pt;tab-stops:49.65pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:23.6pt;margin-bottom:
        0cm;margin-left:2.0cm;margin-bottom:.0001pt;text-align:justify;text-indent:
        -49.6pt;tab-stops:49.65pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='mso-ansi-language:IN'>KEDUA<span style='letter-spacing:4.0pt'> <span
        style='mso-tab-count:1'> </span></span>: </span><span lang=IN
        style='mso-ansi-language:EN-US'><span style='mso-spacerun:yes'> </span></span><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp;Apabila penerima pensiun meninggal<span
        style='letter-spacing:-.05pt'> </span>dunia kepada <span class=SpellE>isteri</span>
        (<span class=SpellE>isteri-isteri</span>)/suami, anak (anak-anak)<span
        style='letter-spacing:-.05pt'> </span>yang </span><span lang=EN-US
        style='mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>  </span></span><span
        lang=IN style='mso-ansi-language:IN'>tercantum dalam Keputusan ini diberikan
        pensiun pokok sebesar 36% (tiga puluh enam persen) dari <b>Rp 5.000.000 : 1 </b>=
        <b>Rp 1.800.000 </b>(dibulatkan) = <b>Rp 1.800.000 </b>sebulan, terhitung
        mulai bulan berikutnya penerima pensiun Pegawai Negeri Sipil meninggal dunia
        dengan ketentuan :</span><span lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:0cm;margin-right:23.95pt;
        margin-bottom:0cm;margin-left:69.35pt;margin-bottom:.0001pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>1.</span><span lang=IN style='ont-size:8.0pt;
        font-family:"Times New Roman",serif;mso-ansi-language:IN'>&nbsp;&nbsp; </span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Pemberian dan pembayaran
        pensiun janda/duda dihentikan pada akhir bulan janda/duda yang bersangkutan
        menikah lagi atau berakhir apabila meninggal dunia dan tidak terdapat lagi
        anak yang memenuhi syarat untuk menerima pensiun.</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:.05pt;margin-right:23.65pt;
        margin-bottom:0cm;margin-left:69.35pt;margin-bottom:.0001pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>2.</span><span lang=IN style='font-size:8.0pt;
        font-family:"Times New Roman",serif;mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp; </span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>Jika janda/duda menikah
        lagi atau meninggal dunia, selama masih terdapat anak/anak-anak yang berusia
        di bawah 25 tahun tidak berpenghasilan sendiri belum pernah menikah, pensiun
        janda/duda itu dibayarkan kepada dan atas nama anak pertama tersebut di atas
        untuk kepentingan anak-anak lainnya terhitung mulai bulan berikutnya
        terjadinya <span style='letter-spacing:-.1pt'>pernikahan/kematian.</span></span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-top:0cm;margin-right:23.65pt;
        margin-bottom:0cm;margin-left:69.35pt;margin-bottom:.0001pt;text-align:justify;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>3.</span><span lang=IN style='ont-size:8.0pt;
        font-family:"Times New Roman",serif;mso-ansi-language:IN'></span><span
        lang=IN style='font-size:8.0pt;mso-ansi-language:IN'>&nbsp; Khusus untuk janda
        apabila janda yang bersangkutan kemudian bercerai lagi, maka pensiun janda
        yang pembayarannya telah dihentikan, dibayarkan kembali mulai bulan
        berikutnya perceraian itu berlaku sah.</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.5pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='ont-size:8.0pt;mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.05pt;margin-right:24.05pt;margin-bottom:
        0cm;margin-left:2.0cm;margin-bottom:.0001pt;text-align:justify;text-indent:
        -49.6pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='mso-ansi-language:IN'>KETIGA<span
        style='letter-spacing:4.0pt'> </span></span><span lang=IN style='letter-spacing:
        4.0pt;mso-ansi-language:EN-US'><span style='mso-spacerun:yes'> </span></span><span
        lang=IN style='mso-ansi-language:IN'>:</span><span lang=IN style='letter-spacing:
        2.0pt;mso-ansi-language:EN-US'> </span><span lang=IN style='mso-ansi-language:
        IN'>Di<span style='letter-spacing:-.1pt'> </span>atas<span style='letter-spacing:
        -.15pt'> </span>pensiun<span style='letter-spacing:-.1pt'> </span>pokok<span
        style='letter-spacing:-.1pt'> </span>tersebut<span style='letter-spacing:
        -.2pt'> </span>diberikan<span style='letter-spacing:-.1pt'> </span>tunjangan<span
        style='letter-spacing:-.1pt'> </span>keluarga,<span style='letter-spacing:
        -.15pt'> </span>tunjangan<span style='letter-spacing:-.1pt'> </span>pangan,<span
        style='letter-spacing:-.15pt'> </span>dan<span style='letter-spacing:-.1pt'> </span>tunjangan<span
        style='letter-spacing:-.1pt'> </span>lain</span><span lang=IN
        style='mso-ansi-language:EN-US'> </span><span lang=IN style='mso-ansi-language:
        IN'>sesuai ketentuan perundang-undangan.</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:.55pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='ont-size:8.0pt;mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-top:0cm;margin-right:23.35pt;margin-bottom:
        0cm;margin-left:55.35pt;margin-bottom:.0001pt;text-align:justify;text-indent:
        -49.6pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:
        around;mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='mso-ansi-language:IN'>KEEMPAT
        </span><span lang=IN style='mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'> </span></span><span lang=IN style='mso-ansi-language:
        IN'>: Apabila dikemudian hari terdapat kekeliruan dalam Keputusan ini akan
        diadakan perbaikan dan perhitungan kembali sebagaimana mestinya.</span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US style='mso-ansi-language:
        EN-US'><o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-left:5.8pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='letter-spacing:-.1pt;mso-ansi-language:IN'>KELIMA</span><span lang=IN
        style='mso-ansi-language:IN'>&nbsp;&nbsp;&nbsp;&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>  </span></span><span
        lang=IN style='mso-ansi-language:IN'>&nbsp;:<span style='letter-spacing:1.95pt'>
        </span>Keputusan<span style='letter-spacing:-.15pt'> </span>ini<span
        style='letter-spacing:-.2pt'> </span>mulai<span style='letter-spacing:-.25pt'>
        </span>berlaku<span style='letter-spacing:-.2pt'> </span>pada<span
        style='letter-spacing:-.25pt'> </span>tanggal<span style='letter-spacing:
        -.1pt'> ditetapkan.</span></span><span lang=EN-US style='mso-ansi-language:
        EN-US'><o:p></o:p></span></p>
        <h1 style='margin-top:3.05pt;margin-right:23.25pt;margin-bottom:0cm;
        margin-left:55.05pt;margin-bottom:.0001pt;mso-outline-level:1;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><b><span
        lang=IN style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
        IN'>ASLI </span></b><span lang=IN style='mso-fareast-font-family:"Times New Roman";
        mso-ansi-language:IN'>keputusan<span style='letter-spacing:-.2pt'> </span>ini<span
        style='letter-spacing:-.2pt'> </span>diberikan<span style='letter-spacing:
        -.2pt'> </span>kepada<span style='letter-spacing:-.25pt'> </span>yang<span
        style='letter-spacing:-.2pt'> </span>bersangkutan<span style='letter-spacing:
        -.15pt'> </span>dengan<span style='letter-spacing:-.2pt'> </span>alamat JL.<span
        style='letter-spacing:-.25pt'> </span>GG<span style='letter-spacing:-.25pt'> </span>SLAMET<span
        style='letter-spacing:-.2pt'> </span></span><span lang=IN style='font-family:
        "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";mso-ansi-language:
        IN'>RT.<span style='letter-spacing:2.0pt'> </span>001 / RW. 004 DESA BOBOSAN
        KEC. PURWOKERTO UTARA KAB. BANYUMAS.</span><span lang=EN-US style='mso-fareast-font-family:
        "Times New Roman";mso-ansi-language:EN-US'><o:p></o:p></span></h1>
        <h1 style='margin-top:2.5pt;margin-right:0cm;margin-bottom:0cm;margin-left:
        0cm;margin-bottom:.0001pt;mso-line-height-alt:6.0pt;mso-outline-level:1;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=EN-US style='mso-fareast-font-family:
        "Times New Roman";mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'>           </span></span><span lang=IN
        style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:IN'>Petikan<span
        style='letter-spacing:2.4pt'> </span>yang<span style='letter-spacing:2.35pt'>
        </span>sah<span style='letter-spacing:2.45pt'> </span>sesuai<span
        style='letter-spacing:2.3pt'> </span>dengan<span style='letter-spacing:2.45pt'>
        </span><span style='letter-spacing:-.1pt'>aslinya</span></span><span
        lang=EN-US style='mso-fareast-font-family:"Times New Roman";letter-spacing:
        -.1pt;mso-ansi-language:EN-US'> <span style='mso-tab-count:3'>                              </span><span
        style='mso-spacerun:yes'>          </span><span class=SpellE>&emsp;&emsp;&emsp;&emsp; Ditetakan</span>
        di <b>Semarang</b><o:p></o:p></span></h1>
        <h1 style='margin-top:2.5pt;margin-right:0cm;margin-bottom:0cm;margin-left:
        49.65pt;margin-bottom:.0001pt;mso-line-height-alt:6.0pt;mso-outline-level:
        1;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span class=SpellE><span lang=EN-US
        style='mso-fareast-font-family:"Times New Roman";letter-spacing:-.1pt;
        mso-ansi-language:EN-US'>a.n</span></span><span lang=EN-US style='mso-fareast-font-family:
        "Times New Roman";letter-spacing:-.1pt;mso-ansi-language:EN-US'> <span
        class=SpellE>Gubernur</span> <span class=SpellE>Jawa</span> Tengah <span
        style='mso-tab-count:4'>                                           </span><span
        style='mso-spacerun:yes'>          </span>&emsp;&emsp;&emsp;&emsp;&nbsp; Pada <span class=SpellE><span
        class=GramE>tanggal</span></span><span class=GramE> :</span> <o:p></o:p></span></h1>
        <h1 style='margin-top:2.5pt;margin-right:0cm;margin-bottom:0cm;margin-left:
        35.45pt;margin-bottom:.0001pt;mso-line-height-alt:6.0pt;mso-outline-level:
        1;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span class=SpellE><span lang=EN-US
        style='mso-fareast-font-family:"Times New Roman";letter-spacing:-.1pt;
        mso-ansi-language:EN-US'>Kepala</span></span><span lang=EN-US
        style='mso-fareast-font-family:"Times New Roman";letter-spacing:-.1pt;
        mso-ansi-language:EN-US'> Badan <span class=SpellE>Kepegawaian</span> Daerah<span
        style='mso-tab-count:4'>                                       </span><span
        style='mso-spacerun:yes'>          </span>&emsp;&emsp;&emsp;&nbsp;&nbsp; GUBERNUR JAWA TENGAH<o:p></o:p></span></h1>
        <p class=MsoNormal style='text-autospace:ideograph-numeric ideograph-other;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=EN-US style='mso-fareast-font-family:
        "Times New Roman";letter-spacing:-.1pt;mso-ansi-language:EN-US'><span
        style='mso-tab-count:1'>         </span></span><span style='font-size:8.0pt;
        font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
        <h1 style='margin-top:2.5pt;margin-right:0cm;margin-bottom:0cm;margin-left:
        35.45pt;margin-bottom:.0001pt;mso-line-height-alt:6.0pt;mso-outline-level:
        1;tab-stops:221.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-fareast-font-family:"Times New Roman";letter-spacing:-.1pt;
        mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></h1>
        <p class=MsoNormal style='margin-left:44.2pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoNormal style='margin-left:44.2pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>
        <p class=MsoNormal style='margin-top:5.9pt;margin-right:23.55pt;margin-bottom:
        0cm;margin-left:2.0cm;margin-bottom:.0001pt;mso-element:frame;mso-element-frame-hspace:
        9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=IN
        style='font-size:8.0pt;mso-ansi-language:IN'>Drs. WISNU ZAROH, <span
        class=SpellE>M.Si</span> </span><span lang=EN-US style='font-size:8.0pt;
        mso-ansi-language:EN-US'><span style='mso-spacerun:yes'>   </span><span
        style='mso-tab-count:4'>                                        </span><span
        style='mso-spacerun:yes'>                </span>&emsp;&emsp;GANJAR PRANOWO<o:p></o:p></span></p>
        <p class=MsoNormal style='margin-top:5.9pt;margin-right:113.6pt;margin-bottom:
        0cm;margin-left:49.65pt;margin-bottom:.0001pt;text-indent:.05pt;line-height:
        150%;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='font-size:8.0pt;
        line-height:150%;mso-ansi-language:IN'>&nbsp;&nbsp; NIP.<span style='letter-spacing:-.5pt'>
        </span>19630526<span style='letter-spacing:-.55pt'> </span>199503<span
        style='letter-spacing:-.55pt'> </span>1<span style='letter-spacing:-.55pt'> </span>002<o:p></o:p></span></p>
        <p class=MsoBodyText style='margin-left:14.2pt;line-height:115%;mso-element:
        frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-horizontal:
        page;mso-element-left:8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span
        lang=IN style='mso-ansi-language:IN'>Tembusan<span style='letter-spacing:
        -.35pt'> </span>Keputusan<span style='letter-spacing:-.35pt'> </span>ini<span
        style='letter-spacing:-.35pt'> </span>disampaikan<span style='letter-spacing:
        -.3pt'> </span><span style='letter-spacing:-.1pt'>kepada:</span></span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-left:14.2pt;text-indent:-8.8pt;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
        font-family:"Times New Roman",serif;letter-spacing:-.1pt;mso-ansi-language:
        IN'>1.&nbsp;&nbsp; </span><span lang=IN style='font-size:8.0pt;mso-ansi-language:
        IN'>Kepala<span style='letter-spacing:-.35pt'> </span>BKN/Kantor<span
        style='letter-spacing:-.4pt'> </span>Regional<span style='letter-spacing:
        -.3pt'> </span><span style='letter-spacing:-.2pt'>BKN;</span></span><span
        lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-left:14.2pt;text-indent:-8.8pt;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
        font-family:"Times New Roman",serif;letter-spacing:-.1pt;mso-ansi-language:
        IN'>2.&nbsp;&nbsp; </span><span lang=IN style='font-size:8.0pt;mso-ansi-language:
        IN'>Kepala<span style='letter-spacing:-.3pt'> </span>BPKAD<span
        style='letter-spacing:-.25pt'> </span>Provinsi<span style='letter-spacing:
        -.25pt'> </span>Jawa<span style='letter-spacing:-.25pt'> </span><span
        style='letter-spacing:-.1pt'>Tengah;</span></span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoListParagraph style='margin-left:14.2pt;text-indent:-8.8pt;
        mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
        mso-element-anchor-horizontal:page;mso-element-left:8.45pt;mso-element-top:
        -64.5pt;mso-height-rule:exactly'><span lang=IN style='ont-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>3.</span><span lang=IN
        style='ont-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:IN'> </span><span lang=IN style='font-size:8.0pt;
        mso-ansi-language:IN'>&nbsp; Kepala<span style='letter-spacing:-.35pt'> </span>Kantor<span
        style='letter-spacing:-.3pt'> </span>Cabang<span style='letter-spacing:-.35pt'>
        </span>PT.TASPEN/ASABRI<span style='letter-spacing:-.3pt'> </span>(PERSERO)<span
        style='letter-spacing:-.3pt'> </span>di<span style='letter-spacing:-.35pt'> </span><span
        class=SpellE><span style='letter-spacing:-.1pt'>Purwokerto</span></span><span
        style='letter-spacing:-.1pt'>;</span></span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        <p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;
        mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
        8.45pt;mso-element-top:-64.5pt;mso-height-rule:exactly'><span lang=EN-US
        style='ont-size:8.0pt;letter-spacing:-.1pt;mso-ansi-language:EN-US'><span
        style='mso-spacerun:yes'>  </span></span><span lang=IN style='ont-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>&nbsp;4.</span><span lang=IN
        style='ont-size:8.0pt;font-family:"Times New Roman",serif;letter-spacing:
        -.1pt;mso-ansi-language:IN'> </span><span lang=IN style='font-size:8.0pt;
        letter-spacing:-.1pt;mso-ansi-language:IN'>&nbsp; Pertinggal.</span><span lang=IN
        style='font-size:8.5pt;mso-ansi-language:IN'>&nbsp;</span><span lang=EN-US
        style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
        </td>
      </tr>
      </table>
        <p class=MsoNormal><o:p>&nbsp;</o:p></p>
    </div>
  </body>
</html>
