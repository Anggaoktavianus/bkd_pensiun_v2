<style>
  select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Calon BUP</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Calon BUP </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>
    
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<!-- <?php
							$bulan = $row->id_waktu_skpd;
							$tanggalSekarang = date('d-m-Y');
							$tanggalDuedate = date("d-m-Y", strtotime($tanggalSekarang.' + '.$bulan.' Days'));
							echo "tanggal sekarang :".$tanggalSekarang."<br>";
							echo "tanggal overdue :".$tanggalDuedate;
							?> -->
						<div class="card-body">
							<table  id="example"class="table table-bordered table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
							 			<th>Nama</th>
										<th>Jabatan</th>
										<th>Pangkat.</th>
										<th>TMT Pensiun</th>
										<th>SKPD</th>
										<!-- <th></th> -->
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
										foreach ($row as $key => $data) {
									?>
									<tr>
										<td><?=$no++?></td>
										<td><?= $data->B_02B ?></td>
										<td><?= $data->nama ?></td>
										<td><?=$data->I_JB?></td>
										<td><?=$data->F_PK ?>, <?=$data->pangkat?></td>
										<td></td>
										<td><?= $data->nmskpd?></td>
										<!-- <td></td> -->
										<td>
										<form action="<?= site_url('Bup/del/'.$data->B_02B)?>" method="post">
										<a href="<?= site_url('Bup/edit_bup_bkd/'. $data->B_02B)?>" class="btn btn-success btn-xs text-white">
										<i class="fas fa-eye "></i>
										</a>
										<input type="hidden" name="id" value="<?= $data->B_02B ?>">
										<button onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-xs text-white">
										<i class="fas fa-trash-alt "></i>
										</button>
										</form>
										</td>
									</tr>
									<?php
                    							} ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Pangkat.</th>
										<th>TMT Pensiun</th>
										<th>SKPD</th>
										<!-- <th></th> -->
										<th>Action</th>
									</tr>	
								</tfoot>
							</table>
							
						</div>
						<!-- <div class="card-header">
							<a href="" class="btn btn-success btn-sm"><i class="fas fa-arrow-circle-right">&nbsp;</i>Blast Info ke User</a>
						</div> -->
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
 

<!-- <script>
 $(document).ready(function (){
   var table = $('#example1').DataTable({
//       'ajax': '/lab/jquery-datatables-checkboxes/ids-arrays.txt',
	// 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']]
   });


   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      var rows_selected = table.column(0).checkboxes.selected();

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'id[]')
                .val(rowId)
         );
      });
   });
});

</script> -->
<!-- <script>
	$(document).ready(function() {
    $('#example').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
</script> -->

  <script>
// initialising dt table
$(document).ready(function() {

	$('#example').DataTable({
		
      
		// Definition of filter to display
		
		filterDropDown: {
			columns: [
				{
					idx: 6
				},
				{
					idx: 3
				}
			],
			bootstrap: true
		}
	} );
} );
</script>

