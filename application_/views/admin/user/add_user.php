<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah User </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('user/add')?>" method="POST">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" name="nama" class="form-control" value="<?= set_value('nama') ?>" placeholder="Masukkan Nama" >
                        <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>" id="exampleInputEmail1" placeholder="Enter email" >
                        <?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" >
                        <?= form_error('password', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Konfirmasi Password</label>
                        <input type="password" name="password2" class="form-control" id="exampleInputPassword2" placeholder="Password" >
                        <?= form_error('password2', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" >
                        <label for="role_id">Akses</label>
                        <select name="role_id" class="form-control" id="admin" onchange="handleSelect()">
                            <option value="0">--Select--</option>
                            <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
                            <option value="1" <?= set_value('role_id') == 1 ? "selected" : null ?>>Adminprov</option>
                            <option value="2" <?= set_value('role_id') == 2 ? "selected" : null ?> id="kab">Admin Kab/Kota</option>
                            <option value="3" <?= set_value('role_id') == 3 ? "selected" : null ?> id="gudang">Admin Gudang</option>
                            <?php } ?>
                            <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
                            <option value="3" <?= set_value('role_id') == 3 ? "selected" : null ?>>Admin Gudang</option>
                            <?php } ?>
                        </select>
                        <?= form_error('role_id', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group" id="kot">
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" id="kota" class="form-control">
                              <option value="">--Select--</option>
                              <?php foreach ($kabkota as $key => $data) { ?>
                                  <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                              <?php } ?>
                              <!--  -->
                          </select>
                      </div>
                      <div class="form-group" id="kec">
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												        <option value="">--Select--</option>
											    </select>
                      </div>
                      <div class="form-group" id="des">
                          <label for="desa">Kelurahan</label>
                          <select name="kelurahan_id" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <div class="form-group" >
                          <label for="status">Status</label>
                          <select name="status" class="form-control">
                              <option value="0">--Select--</option>
                              <option value="1" <?= set_value('status') == 1 ? "selected" : null ?>>Aktif</option>
                              <option value="0" <?= set_value('status') == 0 ? "selected" : null ?>>Nonaktif</option>
                          </select>
                          <?= form_error('status', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<script>

  document.getElementById("admin").onchange = function() {
     $("#kot").hide();
            $("#kec").hide();
            $("#des").hide();
    if (this.value == '3') {
        var optionID=document.getElementById('gudang');
        // alert(optionID.value);
        $("#kot").show();
        $("#kec").show();
        $("#des").show();

    }
    if (this.value == '2') {
        var optionID=document.getElementById('kab');
        // alert(optionID.value);
        $("#kot").show();

    }
    
}
</script>

