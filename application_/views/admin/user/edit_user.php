<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit <small>User</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="hidden" name="id" class="form-control" value="<?= $row->id ?>">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?= $row->nama ?>" >
                        <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group ">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?= $row->email ?>" >
                        <?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div> 
                      <div class="form-group">
                        <label for="role_id">Akses</label>
                        <select name="role_id" class="form-control" id="admin">
                          <option value="0">--Select--</option>
                          <?php foreach ($role as $key => $data) { ?>
                          <option value="<?= $data->id?>" <?= $data->id == $row->role_id ? "selected" : null ?>><?= $data->nama ?></option>
                          <?php } ?>
                        </select>
                        <?= form_error('role_id', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div> 
                      <div class="form-group" >
                        <label for="role_id">Status</label>
                        <select name="status" class="form-control" >
                            <?php $status = $this->input->post('status') ? $this->input->post('status') : $row->status ?>
                            <option value="1">Aktif</option>
                            <option value="0" <?= $row->status == 0 ? "selected" : null ?>>Nonaktif</option>
                        </select>
                        <?= form_error('status', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id="kot">
                        <label for="kabkota_id_id">Kab/Kota</label>
                        <select name="kabkota_id" id="kota" class="form-control">
                          <option value="0">--Select--</option>
                          <?php foreach ($kabkota as $key => $data) { ?>
                          <option value="<?= $data->id?>" <?= $data->id == $row->kabkota_id ? "selected" : null ?>><?= $data->name ?></option>
                          <?php } ?>
                        </select>
                        <?= form_error('kabkota_id', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group" id="kec">
                        <label for="kecamatan_id">Kecamatan</label>
                        <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
                              <option value="">--Select--</option>
                              <?php foreach ($keca as $key => $data) { ?>
                          <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group" id="des">
                          <label for="desa">Kelurahan</label>
                          <select name="kelurahan_id" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
                            <?php foreach ($desa as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                    </div>
                  </div> 
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <script>

  document.getElementById("admin").onchange = function() {
     $("#kot").hide();
            $("#kec").hide();
            $("#des").hide();
    if (this.value == '3') {
        var optionID=document.getElementById('gudang');
        // alert(optionID.value);
        $("#kot").show();
        $("#kec").show();
        $("#des").show();

    }
    if (this.value == '2') {
        var optionID=document.getElementById('kab');
        // alert(optionID.value);
        $("#kot").show();

    }
    
}
</script>